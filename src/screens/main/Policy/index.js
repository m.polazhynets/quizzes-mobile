import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import HTML from 'react-native-render-html';
import {getPrivacyPolicy} from '../../../actions/generalActions';
import {Container, Preloader} from '../../../components';
import styles from './styles';

class PolicyScreen extends Component {
  componentDidMount() {
    this.props.getPrivacyPolicy();
  }

  render() {
    const {t, is_request, privacy_policy} = this.props;

    return (
      <Container
        enableBrainBackground
        scrollStyles={styles.content}
        headerProps={{
          title: t('main_flow:privacy_policy'),
        }}>
        {is_request && <Preloader />}
        {privacy_policy.content && (
          <HTML
            html={privacy_policy.content}
            imagesMaxWidth={Dimensions.get('window').width}
            tagsStyles={{p: styles.text, a: [styles.text, styles.link]}}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.generalReducer.is_request,
  privacy_policy: state.generalReducer.privacy_policy || {},
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getPrivacyPolicy}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PolicyScreen),
);
