import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import {COLOR_WHITE_DARKEN} from '../../../../constants/Colors';
import {scale, v_scale} from '../../../../constants/StylesConstants';

export default StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  zIndex: {
    zIndex: Platform.OS === 'android' ? 0 : 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  headerButton: {
    position: 'absolute',
    right: scale(10),
    marginTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    paddingVertical: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
  },
  scrollContentStyles: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    paddingRight: 0,
    zIndex: 2,
  },
});
