import {StyleSheet} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {
  COLOR_DARK,
  COLOR_WHITE_DARKEN,
  COLOR_GRAY_0,
} from '../../../constants/Colors';
import variables, {
  scale,
  FONT_REGULAR,
  v_scale,
} from '../../../constants/StylesConstants';

const {small} = variables.fontSize;

export default StyleSheet.create({
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_DARK,
  },
  content: {
    flexDirection: 'row',
    backgroundColor: COLOR_WHITE_DARKEN,
    width: scale(180),
    height: v_scale(50),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(20),
    borderWidth: scale(2),
    borderColor: COLOR_GRAY_0,
    ...variables.shadow,
  },
  icon: {
    width: scale(27),
    height: v_scale(18),
    marginRight: scale(15),
  },
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: getBottomSpace() + v_scale(50),
    zIndex: 2,
  },
});
