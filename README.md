# Quizzes
Quizzes is a React Native Application using Firebase

For problem: 
- The Swift pod `react-native-track-player` depends upon `React`, which do not define modules. 
    Just to make it clear:

    node_modules/react-native-track-player/react-native-track-player.podspec
    s.exclude_files = ["ios/RNTrackPlayer/Vendor/AudioPlayer/Example"]
