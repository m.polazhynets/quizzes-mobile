import {StyleSheet} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {COLOR_DARK} from '../../../constants/Colors';
import variables, {
  scale,
  FONT_REGULAR,
  FONT_BOLD,
  v_scale,
} from '../../../constants/StylesConstants';

const {mainRegular} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    position: 'absolute',
    width: '100%',
    top: getStatusBarHeight(),
    zIndex: 99,
  },
  container: {
    ...variables.shadow,
    backgroundColor: 'white',
    padding: scale(14),
    borderRadius: scale(10),
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    margin: scale(14),
  },
  content: {
    paddingLeft: scale(10),
    flex: 1,
  },
  title: {
    color: COLOR_DARK,
    fontFamily: FONT_BOLD,
    fontSize: mainRegular,
    marginBottom: v_scale(5),
  },
  text: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
  },
  icon: {
    width: scale(50),
    height: scale(50),
    borderRadius: scale(50),
    overflow: 'hidden',
  },
  actionText: {
    opacity: 0,
  },
});
