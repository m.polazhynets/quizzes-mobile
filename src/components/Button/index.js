import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Text,
  ActivityIndicator,
  View,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  COLOR_ROYAL_BLUE,
  COLOR_WHITE,
  COLOR_ROSE_DARKEN,
  COLOR_ROSE,
  COLOR_YELLOW_1_DARKEN,
  COLOR_YELLOW_1,
  COLOR_GRAY_0,
  COLOR_BLUE_3_DARKEN,
  COLOR_BLUE_3,
  COLOR_GRAY_5,
} from '../../constants/Colors';
import {ARROW_WHITE_ICON} from '../../constants/Images';
import styles from './styles';

const RED = [COLOR_ROSE_DARKEN, COLOR_ROSE];
const YELLOW = [COLOR_YELLOW_1_DARKEN, COLOR_YELLOW_1];
const BLUE = [COLOR_BLUE_3_DARKEN, COLOR_BLUE_3];
const DISABLED = [COLOR_GRAY_0, COLOR_GRAY_0];
const GRAY = [COLOR_GRAY_5, COLOR_GRAY_5];

const Button = ({
  onPress,
  isLoading,
  contentContainerStyle = {},
  style = {},
  disable,
  title,
  type,
  margin,
  isArrow,
}) => {
  const activeOpacity = disable ? 1 : 0.8;

  return (
    <LinearGradient
      start={{x: 0.5, y: 0.5}}
      end={{x: 1, y: 0.5}}
      colors={
        disable
          ? DISABLED
          : type === 'red'
          ? RED
          : type === 'blue'
          ? BLUE
          : type === 'gray'
          ? GRAY
          : YELLOW
      }
      style={[
        styles.button,
        margin && styles.buttomMargin,
        type && styles[`button_${type}`],
        contentContainerStyle,
      ]}>
      <TouchableOpacity
        activeOpacity={activeOpacity}
        style={styles.buttonContent}
        onPress={() => {
          if (disable || isLoading) {
            return;
          }
          onPress();
        }}>
        {isLoading ? (
          <View style={styles.loaderWrap}>
            <ActivityIndicator
              size="small"
              color={type === 'red' ? COLOR_WHITE : COLOR_ROYAL_BLUE}
            />
          </View>
        ) : (
          <View />
        )}
        <View style={[styles.textWrapper, isLoading && styles.hideText]}>
          <Text
            style={[
              styles.buttonText,
              type && styles[`buttonText_${type}`],
              disable && styles.disabledbuttonText,
              style,
            ]}>
            {title}
          </Text>
          {isArrow && (
            <Image
              source={ARROW_WHITE_ICON}
              resizeMode="contain"
              style={styles.icon}
            />
          )}
        </View>
      </TouchableOpacity>
    </LinearGradient>
  );
};

Button.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['red', 'yellow', 'blue', 'gray']),
  disable: PropTypes.bool,
  isLoading: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  margin: PropTypes.bool,
  isArrow: PropTypes.bool,
  contentContainerStyle: PropTypes.any,
  style: PropTypes.object,
};

export default Button;
