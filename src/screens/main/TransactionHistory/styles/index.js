import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getBottomSpace} from 'react-native-iphone-x-helper';
import {
  COLOR_GREEN_3,
  COLOR_GRAY_1,
  COLOR_DARK,
  COLOR_WHITE_DARKEN,
} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_BOLD,
} from '../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  scrollContentStyles: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    zIndex: 2,
  },
  textWrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  text: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
  },
  icon: {
    width: scale(24),
    height: scale(27),
    marginRight: scale(22),
  },
  itemList: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    borderTopWidth: scale(1),
    borderColor: COLOR_GRAY_1,
    paddingVertical: v_scale(24),
  },
  colorGreen: {
    color: COLOR_GREEN_3,
  },
  noBorder: {
    borderTopWidth: 0,
  },
  labelText: {
    color: COLOR_DARK,
    fontFamily: FONT_BOLD,
    fontSize: regular,
    marginVertical: v_scale(5),
  },
  background: {
    position: 'absolute',
    width: scale(220),
    height: scale(220),
    left: 0,
    bottom: isIphoneX() ? getBottomSpace() + v_scale(30) : v_scale(30),
  },
  zIndex: {
    zIndex: Platform.OS === 'android' ? 0 : 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
});
