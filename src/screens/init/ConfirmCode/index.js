import React, {Component} from 'react';
import {View, Text, TextInput, Keyboard, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {Container, Preloader} from '../../../components';
import styles from './styles';

class ConfirmCodeScreen extends Component {
  state = {
    ...this.props.route.params,
    code: '',
  };
  input = React.createRef();

  componentDidUpdate(prevProps) {
    const {code_confirmed, code_confirmed_profile} = this.props;

    if (prevProps.code_confirmed !== code_confirmed && code_confirmed) {
      this.state.callBack();
    }
    if (
      prevProps.code_confirmed_profile !== code_confirmed_profile &&
      code_confirmed_profile
    ) {
      this.state.callBack();
    }
  }

  componentWillUnmount() {
    Keyboard.dismiss();
  }

  onChangeText = val => {
    const {requestAction = () => {}, requestData = []} = this.state;
    this.setState({code: val}, () => {
      if (val.length === 6) {
        Keyboard.dismiss();
        this.setState({code: ''});
        requestAction(val, ...requestData);
      }
    });
  };

  render() {
    const {headerTitle, text, code} = this.state;
    const {is_request, is_request_profile} = this.props;

    return (
      <Container
        headerProps={{
          title: headerTitle,
        }}>
        {(is_request || is_request_profile) && <Preloader />}
        <View style={styles.content}>
          <Text style={styles.text}>{text}</Text>
          <TextInput
            ref={this.input}
            value={code}
            keyboardType={'numeric'}
            maxLength={6}
            autoFocus
            onChangeText={this.onChangeText}
            style={styles.hideInput}
          />
          <View style={styles.codeBlock}>
            <TouchableOpacity
              style={styles.codeBlock}
              onPress={() => this.input.current.focus()}
              activeOpacity={0.9}>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(0)}</Text>
              </View>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(1)}</Text>
              </View>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(2)}</Text>
              </View>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(3)}</Text>
              </View>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(4)}</Text>
              </View>
              <View style={styles.code}>
                <Text style={styles.number}>{code.charAt(5)}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.authReducer.is_request,
  is_request_profile: state.profileReducer.is_request,
  code_confirmed: state.authReducer.code_confirmed,
  code_confirmed_profile: state.profileReducer.code_confirmed,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ConfirmCodeScreen),
);
