import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from 'i18next';
import {getStatistics} from '../../../actions/profileActions';
import {Container, Preloader} from '../../../components';
import {CUP_BACKGROUND_IMAGE} from '../../../constants/Images';
import {COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN} from '../../../constants/Colors';
import styles from './styles';

const VIOLET = [COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN];

class StatisticsScreen extends Component {
  constructor(props) {
    super(props);
    const {isTournament} = this.props.route.params;
    this.state = {
      isTournament,
    };
    this.list = [
      {
        id: '0',
        title: this.props.t(
          `main_flow:${
            isTournament
              ? 'number_of_tournaments_completed'
              : 'number_of_quizzes_completed'
          }`,
        ),
        key: 'tournaments_count',
      },
      {
        id: '1',
        title: this.props.t('main_flow:total_correct_answers'),
        key: 'correct_answers',
      },
      {
        id: '2',
        title: this.props.t('main_flow:total_incorrect_answers'),
        key: 'bad_answers',
      },
      {
        id: '3',
        title: this.props.t('main_flow:average_response_time'),
        key: 'question_average_time',
      },
      {
        id: '4',
        title: this.props.t('main_flow:total_time_used'),
        key: 'total_time',
      },
      {
        id: '5',
        title: this.props.t('main_flow:total_tips_used'),
        key: 'total_tooltips',
      },
      {
        id: '6',
        title: this.props.t('main_flow:question_average_tooltips'),
        key: 'question_average_tooltips',
      },
      {
        id: '7',
        title: this.props.t('main_flow:mistakes'),
        key: 'mistakes',
      },
    ];
  }

  componentDidMount() {
    const {isTournament} = this.state;

    this.props.getStatistics(isTournament ? 'tournaments' : 'quizzes');
  }

  renderItem = (item, index, data) => {
    const {t} = this.props;

    return (
      <View
        style={[styles.item, index === this.list.length - 1 && styles.noBorder]}
        key={item.id}>
        <Text style={styles.label}>{item.title}</Text>
        <Text style={styles.value}>{`${data[item.key] || 0}${
          item.key === 'question_average_time' || item.key === 'total_time'
            ? t('common:texts.sec')
            : ''
        }`}</Text>
      </View>
    );
  };

  render() {
    const {
      t,
      is_request,
      tournaments_statistics,
      quizzes_statistics,
    } = this.props;
    const {isTournament} = this.state;

    const data = isTournament ? tournaments_statistics : quizzes_statistics;
    return (
      <Container
        headerProps={{
          title: t(
            `main_flow:${
              isTournament ? 'tournament_statistics' : 'quiz_statistics'
            }`,
          ),
          whiteBackBtn: true,
          backgroundType: VIOLET,
          backgroundImage: CUP_BACKGROUND_IMAGE,
        }}>
        {is_request && <Preloader />}
        {data && (
          <View style={styles.content}>
            {this.list.map((item, index) => this.renderItem(item, index, data))}
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  tournaments_statistics: state.profileReducer.tournaments_statistics,
  quizzes_statistics: state.profileReducer.quizzes_statistics,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getStatistics}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(StatisticsScreen),
);
