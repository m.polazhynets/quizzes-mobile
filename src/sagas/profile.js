/**
 * @module Sagas/Profile
 * @desc Profile
 */

import i18n from 'i18next';
import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {PROFILE, SETTINGS, ERRORS} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getProfile() {
  try {
    const response = yield call(request, 'profile/', {
      method: 'GET',
    });
    yield put({
      type: PROFILE.GET_PROFILE_SUCCESS,
      payload: response,
    });
    return response;
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_PROFILE,
          payload: {},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_PROFILE_FAILURE,
        payload: err.response,
      });
      return null;
    }
  }
}

export function* updateProfile({payload: {payload}}) {
  try {
    const response = yield call(request, 'profile/', {
      method: 'PUT',
      payload,
    });
    yield put({
      type: PROFILE.UPDATE_PROFILE_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.UPDATE_PROFILE,
          payload,
        },
      });
    } else {
      yield put({
        type: PROFILE.UPDATE_PROFILE_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* changeEmail({payload: {email}}) {
  try {
    const response = yield call(request, 'profile/set_email/', {
      method: 'POST',
      payload: {email},
    });
    yield put({
      type: PROFILE.CHANGE_EMAIL_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.CHANGE_EMAIL,
          payload: {email},
        },
      });
    } else {
      yield put({
        type: PROFILE.CHANGE_EMAIL_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* verifyEmail({payload: {code}}) {
  try {
    const response = yield call(request, 'profile/verify_email/', {
      method: 'POST',
      payload: {code},
    });
    yield put({
      type: PROFILE.VERIFY_EMAIL_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.VERIFY_EMAIL,
          payload: {code},
        },
      });
    } else {
      yield put({
        type: PROFILE.VERIFY_EMAIL_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* changeLanguage({payload: {lang_code}}) {
  try {
    const response = yield call(request, 'profile/language/', {
      method: 'POST',
      payload: {lang_code},
    });
    yield put({
      type: PROFILE.CHANGE_LANGUAGE_SUCCESS,
      payload: response,
    });
    yield put({
      type: SETTINGS.SET_LANGUAGE,
      payload: {lang_code},
    });
    i18n.changeLanguage(lang_code);
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.CHANGE_LANGUAGE,
          payload: {lang_code},
        },
      });
    } else {
      yield put({
        type: PROFILE.CHANGE_LANGUAGE_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getStatistics({payload: {type}}) {
  try {
    const response = yield call(request, `profile/statistics/${type}/`, {
      method: 'GET',
    });
    yield put({
      type: PROFILE.GET_STATISTICS_SUCCESS,
      payload: response,
      additionalPayload: {type},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_STATISTICS,
          payload: {type},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_STATISTICS_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getRatingList({payload: {page}}) {
  try {
    const response = yield call(request, 'profile/rating/', {
      method: 'GET',
      params: {page},
    });
    yield put({
      type: PROFILE.GET_RATING_LIST_SUCCESS,
      payload: response,
      additionalPayload: {page},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_RATING_LIST,
          payload: {page},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_RATING_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getUserProfile({payload: {id}}) {
  try {
    const response = yield call(request, `profile/rating/${id}`, {
      method: 'GET',
    });
    yield put({
      type: PROFILE.GET_USER_PROFILE_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_USER_PROFILE,
          payload: {id},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_USER_PROFILE_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* addQuizTipsLifes({
  payload: {target, method, count, package_id = null, callback},
}) {
  let payload = {
    target,
    method,
  };
  if (package_id) {
    payload.package_id = package_id;
  }
  if (count) {
    payload.count = count;
  }
  try {
    const response = yield call(request, 'tips_and_life/quiz/add/', {
      method: 'POST',
      payload,
    });
    yield put({
      type: PROFILE.ADD_QUIZ_TIPS_LIFES_SUCCESS,
      payload: response,
      additionalPayload: payload,
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.ADD_QUIZ_TIPS_LIFES,
          payload: {target, method, count, package_id, callback},
        },
      });
    } else {
      yield put({
        type: PROFILE.ADD_QUIZ_TIPS_LIFES_FAILURE,
        payload: err.response,
      });
      if (callback) {
        callback(false);
      }
    }
  }
}

export function* removeQuizTipsLifes({payload: {target, callback}}) {
  try {
    const response = yield call(request, 'tips_and_life/quiz/remove/', {
      method: 'POST',
      payload: {target},
    });
    yield put({
      type: PROFILE.REMOVE_QUIZ_TIPS_LIFES_SUCCESS,
      payload: response,
      additionalPayload: {target},
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.REMOVE_QUIZ_TIPS_LIFES,
          payload: {target, callback},
        },
      });
    } else {
      yield put({
        type: PROFILE.REMOVE_QUIZ_TIPS_LIFES_FAILURE,
        payload: err.response,
      });
      if (callback) {
        callback(false);
      }
    }
  }
}

export function* addTournamentTipsLifes({payload: {target, price, callback}}) {
  try {
    const response = yield call(request, 'tips_and_life/tournament/add/', {
      method: 'POST',
      payload: {target},
    });
    yield put({
      type: PROFILE.ADD_TOURNAMENT_TIPS_LIFES_SUCCESS,
      payload: response,
      additionalPayload: {target, price},
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.ADD_TOURNAMENT_TIPS_LIFES,
          payload: {target, callback},
        },
      });
    } else {
      yield put({
        type: PROFILE.ADD_TOURNAMENT_TIPS_LIFES_FAILURE,
        payload: err.response,
      });
      if (callback) {
        callback(false);
      }
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* getPaymentHistory({payload: {page}}) {
  try {
    const response = yield call(request, 'profile/payment_history/', {
      method: 'GET',
      params: {page},
    });
    yield put({
      type: PROFILE.GET_PAYMENT_HISTORY_SUCCESS,
      payload: response,
      additionalPayload: {page},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_PAYMENT_HISTORY,
          payload: {page},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_PAYMENT_HISTORY_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getDisbursement({payload: {sum, text, callback}}) {
  try {
    const response = yield call(request, 'profile/wallet/disbursement/', {
      method: 'POST',
      payload: {sum, text},
    });
    yield put({
      type: PROFILE.GET_DISBURSEMENT_SUCCESS,
      payload: response,
      additionalPayload: {sum, text},
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: PROFILE.GET_DISBURSEMENT,
          payload: {sum, text, callback},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_DISBURSEMENT_FAILURE,
        payload: err.response,
      });
      if (callback) {
        callback(false);
      }
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

/**
 * Profile Sagas
 */
export default function* root() {
  yield all([takeLatest(PROFILE.GET_PROFILE, getProfile)]);
  yield all([takeLatest(PROFILE.UPDATE_PROFILE, updateProfile)]);
  yield all([takeLatest(PROFILE.CHANGE_EMAIL, changeEmail)]);
  yield all([takeLatest(PROFILE.VERIFY_EMAIL, verifyEmail)]);
  yield all([takeLatest(PROFILE.CHANGE_LANGUAGE, changeLanguage)]);
  yield all([takeLatest(PROFILE.GET_STATISTICS, getStatistics)]);
  yield all([takeLatest(PROFILE.GET_RATING_LIST, getRatingList)]);
  yield all([takeLatest(PROFILE.GET_USER_PROFILE, getUserProfile)]);
  yield all([takeLatest(PROFILE.ADD_QUIZ_TIPS_LIFES, addQuizTipsLifes)]);
  yield all([takeLatest(PROFILE.REMOVE_QUIZ_TIPS_LIFES, removeQuizTipsLifes)]);
  yield all([
    takeLatest(PROFILE.ADD_TOURNAMENT_TIPS_LIFES, addTournamentTipsLifes),
  ]);
  yield all([takeLatest(PROFILE.GET_PAYMENT_HISTORY, getPaymentHistory)]);
  yield all([takeLatest(PROFILE.GET_DISBURSEMENT, getDisbursement)]);
}
