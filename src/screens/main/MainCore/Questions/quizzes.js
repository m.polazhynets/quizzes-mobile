import React, {Component} from 'react';
import {
  View,
  StatusBar,
  Text,
  ImageBackground,
  TouchableOpacity,
  Image,
  Vibration,
  Alert,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import _ from 'underscore';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import RadialGradient from 'react-native-radial-gradient';
import {CommonActions} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  changeCurrentQuestionDataQuizzes,
  setAnswersToStoreQuizzes,
  cleareAnswersQuizzes,
  sendResultQuiz,
} from '../../../../actions/quizzesActions';
import {toogleDialogModal} from '../../../../actions/generalActions';
import {
  removeQuizTipsLifes,
  addQuizTipsLifes,
} from '../../../../actions/profileActions';
import {navigate} from '../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {Header, QuestionQuizComponent, DialogModal} from './components';
import {Button, Preloader, Admob} from '../../../../components';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import {BACKGROUND_1_IMAGE, LAMP_ICON} from '../../../../constants/Images';
import {COLOR_BLUE_3_DARKEN, COLOR_BLUE_3} from '../../../../constants/Colors';
import BackgroundTimer from '../../../../utils/Timer';
import styles from './styles';

const BLUE = [COLOR_BLUE_3_DARKEN, COLOR_BLUE_3];

function getCurrentNumber(max, curr) {
  return max > curr ? curr : max - 1;
}

class QuestionQuizzesScreen extends Component {
  state = {
    type_id: this.props.route.params.id,
    answers: this.props.answers[this.props.route.params.id],
    timer: null,
    pause: false,
    watchAdvertsGoal: null,
    admob: null,
  };
  timer = null;
  admob = React.createRef();

  componentDidUpdate(prevProps) {
    const {answers, questions_list} = this.props;

    if (prevProps.answers !== answers) {
      this.setState({answers: answers[this.state.type_id], timer: null});
      if (answers[this.state.type_id].length === questions_list.length) {
        this.saveAnswers(true);
      }
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      BackgroundTimer.clearInterval(this.timer);
    }
  }

  useTooltip = () => {
    const {currentQuestion} = this.props;

    this.props.changeCurrentQuestionDataQuizzes({
      ...currentQuestion,
      tooltips: currentQuestion.tooltips + 1,
    });
    this.setState({showTooltip: true}, () => {
      setTimeout(() => {
        this.setState({showTooltip: false});
      }, 1000);
    });

    this.props.removeQuizTipsLifes('tooltip', res => {
      if (res) {
        this.closeDialogModal();
      }
    });
  };

  closeDialogModal = () => {
    this.tooglePause(false);
    this.props.toogleDialogModal(null);
  };

  onSuccessAnswered = () => {
    this.tooglePause(true);
    setTimeout(() => {
      this.tooglePause(false);
      this.saveAnswer(true);
    }, 50);
  };

  saveAnswer = correct => {
    const {questions_list, currentQuestion} = this.props;
    const {answers, type_id, timer} = this.state;

    BackgroundTimer.clearInterval(this.timer);

    this.props.setAnswersToStoreQuizzes(type_id, {
      id: questions_list[answers.length].id,
      answer_time: timer
        ? questions_list[answers.length].time - timer
        : questions_list[answers.length].time,
      tooltips: currentQuestion.tooltips,
      correct: correct ? 1 : 0,
      mistakes: currentQuestion.mistakes,
    });
  };

  goToNext = () => {
    const {t} = this.props;

    Alert.alert(
      t('quiz_flow:attention'),
      t('quiz_flow:skip_description'),
      [
        {
          text: t('common:texts.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: t('common:buttons.yes'),
          onPress: () => this.saveAnswer(false),
        },
      ],
      {cancelable: false},
    );
  };

  startTimer = async duration => {
    if (this.state.timer) {
      return null;
    }
    this.setState(state => {
      return {timer: state.timer ? state.timer - 1 : duration};
    });
    this.timer = BackgroundTimer.setInterval(() => {
      if (this.state.timer === 0) {
        return this.stopTime();
      }
      if (this.state.pause) {
        return;
      }
      this.setState(state => {
        return {timer: state.timer ? state.timer - 1 : duration};
      });
    }, 1000);
  };

  stopTime = async () => {
    const {currentQuestion, life} = this.props;

    this.props.changeCurrentQuestionDataQuizzes({
      ...currentQuestion,
      mistakes: currentQuestion.mistakes + 1,
    });
    console.log('timer stopped');
    Vibration.vibrate();
    BackgroundTimer.clearInterval(this.timer);
    this.timer = null;
    if (currentQuestion.mistakes >= 1) {
      if (life > 0) {
        this.props.removeQuizTipsLifes('life');
      }
      this.props.toogleDialogModal('main', 'no_time');
    }
  };

  saveAnswers = need_save => {
    const {answers, complete_num, t} = this.props;
    const {type_id} = this.state;
    if (need_save) {
      let total_count = _.reduce(
        answers[this.state.type_id],
        (memo, num) => {
          return memo + num.correct;
        },
        0,
      );
      if (total_count < complete_num) {
        Alert.alert(
          t('quiz_flow:attention'),
          t('quiz_flow:not_enough_correct_answers'),
          [
            {
              text: t('common:buttons.exit'),
              onPress: () => this.goToHome(),
            },
          ],
          {cancelable: false},
        );
      } else {
        this.props.sendResultQuiz(type_id, answers[type_id], () => {
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 2,
              routes: [
                {
                  name: MainFlowKeys.Home,
                  params: {disableStartRequest: true},
                },
                {name: MainFlowKeys.QuizzesList},
                {
                  name: MainFlowKeys.Congratulation,
                  params: {isTournament: false, id: type_id},
                },
              ],
            }),
          );
        });
      }
    } else {
      this.goToHome();
    }
  };

  goToHome = () => {
    this.props.cleareAnswersQuizzes(this.state.type_id);
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: MainFlowKeys.Home,
            params: {disableStartRequest: true},
          },
        ],
      }),
    );
  };

  tooglePause = val => {
    this.setState({pause: val});
  };

  watchVideoFor = type => {
    this.showVideo(res => {
      if (res) {
        this.props.addQuizTipsLifes(type, 'advertising', 1, null, () => {
          this.tooglePause(false);
        });
      }
    });
  };

  onFinishedWatching = () => {
    const {show_dialog_modal} = this.props;
    const {watchAdvertsGoal} = this.state;

    if (!watchAdvertsGoal || show_dialog_modal === 'children') {
      return;
    }

    this.props.addQuizTipsLifes(watchAdvertsGoal, 'advertising', 1, () => {
      this.tooglePause(false);
    });
  };

  // *** DialogModal functions
  onExitPress = () => {
    this.saveAnswers(false);
    this.closeDialogModal();
  };

  onBuyPress = () => {
    const {show_dialog_modal, dialog_type_key} = this.props;

    this.tooglePause(true);
    navigate(MainFlowKeys.PurchaseDonate, {
      dialogParams: {dialog_type_key, show_dialog_modal},
    });
    this.props.toogleDialogModal(null);
  };

  onWatchVideo = key => {
    const {t} = this.props;
    const {admob} = this.state;

    if (!admob) {
      return Toast.show(
        t('common:errors.adverts_initialization_error'),
        Toast.SHORT,
      );
    }

    this.tooglePause(true);
    this.setState({watchAdvertsGoal: key});
    admob.show();
  };
  // ***

  render() {
    const {
      t,
      is_request,
      questions_list,
      tooltips,
      life,
      show_dialog_modal,
      dialog_type_key,
    } = this.props;
    const {answers, showTooltip, timer} = this.state;
    const question_number = questions_list
      ? getCurrentNumber(questions_list.length, answers.length)
      : answers.length;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={BLUE}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.wrapper}>
          <StatusBar
            barStyle="dark-content"
            translucent
            backgroundColor="transparent"
          />
          <SafeAreaView style={styles.content}>
            <Header
              timer={timer}
              defaultTime={
                questions_list ? questions_list[question_number].time : null
              }
            />
            {is_request && <Preloader />}
            <KeyboardAwareScrollView
              contentContainerStyle={styles.scrollContentStyles}>
              {questions_list && (
                <QuestionQuizComponent
                  data={questions_list[question_number]}
                  showTooltip={showTooltip}
                  onSuccessAnswered={this.onSuccessAnswered}
                  startTimer={this.startTimer}
                  onUseTooltips={this.useTooltip}
                  saveAnswers={this.saveAnswers}
                  togglePause={this.tooglePause}
                />
              )}
            </KeyboardAwareScrollView>
            <View style={styles.bottomContent}>
              <View style={styles.leftSide}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.hintButton}
                  onPress={() =>
                    this.props.toogleDialogModal('main', 'tooltip')
                  }>
                  <Text style={styles.lampText}>
                    {t('common:buttons.hint')}
                  </Text>
                  <View style={styles.lampWrapper}>
                    <Image
                      source={LAMP_ICON}
                      resizeMode="contain"
                      style={styles.lampIcon}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <Button
                contentContainerStyle={styles.bottomButton}
                title={t('common:buttons.next')}
                onPress={this.goToNext}
                type="red"
                isArrow
              />
            </View>
            <DialogModal
              isVisible={show_dialog_modal === 'main'}
              type_key="quiz"
              title_type_key={dialog_type_key}
              onCancel={dialog_type_key === 'tooltip' && this.closeDialogModal}
              onBuyPress={this.onBuyPress}
              onUseTooltips={this.useTooltip}
              onWatchVideo={this.onWatchVideo}
              onExitPress={this.onExitPress}
              onTryOnePress={
                dialog_type_key === 'no_time' && this.closeDialogModal
              }
              life={life}
              tooltips={tooltips}
            />
            <Admob
              onFinishedWatching={this.onFinishedWatching}
              onLoaded={admob => this.setState({admob})}
              onCanceled={() => {
                this.tooglePause(false);
                this.setState({admob: null});
              }}
            />
          </SafeAreaView>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.quizzesReducer.is_request,
  questions_list: state.quizzesReducer.questions_list,
  answers: state.quizzesReducer.answers,
  life: state.quizzesReducer.life,
  complete_num: state.quizzesReducer.complete_num,
  tooltips: state.quizzesReducer.tooltips,
  currentQuestion: state.quizzesReducer.currentQuestion,
  show_dialog_modal: state.generalReducer.show_dialog_modal,
  dialog_type_key: state.generalReducer.dialog_type_key,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeCurrentQuestionDataQuizzes,
      setAnswersToStoreQuizzes,
      toogleDialogModal,
      removeQuizTipsLifes,
      addQuizTipsLifes,
      cleareAnswersQuizzes,
      sendResultQuiz,
    },
    dispatch,
  );

export default withTranslation(['common', 'main_flow', 'quiz_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(QuestionQuizzesScreen),
);
