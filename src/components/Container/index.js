import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {StatusBar, View, Image, FlatList} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {BRAIN_IMAGE} from '../../constants/Images';
import {Header} from '../index';

import styles from './styles';

export default class ContainerComponent extends Component {
  header = React.createRef();

  renderKeyboardAwareScrollView = ({children}) => {
    const {
      scrollEnabled = true,
      headerEnabled = true,
      scrollStyles = {},
      childrenComponentProps = {},
    } = this.props;

    return (
      <KeyboardAwareScrollView
        style={styles.zIndex}
        contentContainerStyle={[
          styles.scrollContentStyles,
          scrollStyles,
          headerEnabled && styles.paddingTop,
        ]}
        scrollEnabled={scrollEnabled}
        keyboardShouldPersistTaps="handle"
        onScroll={e => {
          if (!headerEnabled) {
            return;
          }
          this.header.current.onScroll(e);
        }}
        scrollEventThrottle={16}
        {...childrenComponentProps}>
        {children}
      </KeyboardAwareScrollView>
    );
  };

  renderFlatList = () => {
    const {
      scrollEnabled = true,
      headerEnabled = true,
      scrollStyles = {},
      childrenComponentProps = {},
    } = this.props;
    return (
      <Fragment>
        <View style={styles.emptyBlock} />
        <FlatList
          contentContainerStyle={[
            styles.scrollContentStyles,
            scrollStyles,
            headerEnabled && styles.paddingTopFlatList,
          ]}
          scrollEnabled={scrollEnabled}
          keyboardShouldPersistTaps="handle"
          onScroll={e => {
            if (!headerEnabled) {
              return;
            }
            this.header.current.onScroll(e);
          }}
          scrollEventThrottle={16}
          {...childrenComponentProps}
        />
      </Fragment>
    );
  };

  render() {
    const {
      headerEnabled = true,
      headerProps = {},
      contentContainerStyle = {},
      enableStatusBar = true,
      childrenType,
      children,
      enableBrainBackground = false,
    } = this.props;

    const ChildComponent =
      childrenType === 'flatlist'
        ? this.renderFlatList
        : this.renderKeyboardAwareScrollView;

    return (
      <View style={styles.wrapper}>
        {enableStatusBar && (
          <StatusBar
            barStyle="dark-content"
            translucent
            backgroundColor={'transparent'}
          />
        )}
        <SafeAreaView style={[styles.content, contentContainerStyle]}>
          {headerEnabled && <Header ref={this.header} {...headerProps} />}
          <ChildComponent>
            <Fragment>
              {enableBrainBackground && (
                <Image
                  source={BRAIN_IMAGE}
                  resizeMode="cover"
                  style={styles.background}
                />
              )}
              {children}
            </Fragment>
          </ChildComponent>
        </SafeAreaView>
      </View>
    );
  }
}

ContainerComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  headerProps: PropTypes.object,
  scrollEnabled: PropTypes.bool,
  headerEnabled: PropTypes.bool,
  transparentBackground: PropTypes.bool,
  enableStatusBar: PropTypes.bool,
  enableBrainBackground: PropTypes.bool,
  contentContainerStyle: PropTypes.object,
  scrollStyles: PropTypes.object,
};
