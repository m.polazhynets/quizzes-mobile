import {Component} from 'react';
import {Alert} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Toast from 'react-native-simple-toast';
import {deleteErrors} from '../../actions/errorsActions';
import i18n from 'i18next';

class ErrorModal extends Component {
  componentDidUpdate(prevProps) {
    const {modal, validation} = this.props;

    if (prevProps.modal !== modal && modal) {
      Alert.alert(
        modal.label,
        modal.message,
        [
          {
            text: i18n.t('common:texts.understand'),
            onPress: () => this.props.deleteErrors(),
          },
        ],
        {cancelable: false},
      );
    }

    if (prevProps.validation !== validation && validation && validation.error) {
      Toast.show(validation.error[0], Toast.SHORT);
      setTimeout(() => {
        this.props.deleteErrors();
      }, 1000);
    }

    if (
      prevProps.validation !== validation &&
      validation &&
      validation.non_field_errors
    ) {
      Toast.show(validation.non_field_errors[0], Toast.SHORT);
      setTimeout(() => {
        this.props.deleteErrors();
      }, 1000);
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  modal: state.errorsReducer.modal,
  validation: state.errorsReducer.validation,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({deleteErrors}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ErrorModal);
