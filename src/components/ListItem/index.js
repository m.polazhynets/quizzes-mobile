import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, Image, View} from 'react-native';
import {ARROW_GRAY_ICON, CHECK_BLACK_ICON} from '../../constants/Images';
import styles from './styles';

const ListItem = ({
  onPress,
  title,
  isArrow,
  noBorder,
  contentContainerStyle,
  checked,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[
        styles.item,
        noBorder && styles.noBorder,
        !isArrow && styles.center,
        contentContainerStyle,
      ]}>
      <View style={styles.textWrapper}>
        <Text style={styles.itemText}>{title}</Text>
      </View>
      {checked && (
        <Image
          source={CHECK_BLACK_ICON}
          style={styles.checkIcon}
          resizeMode="contain"
        />
      )}
      {isArrow && (
        <View style={styles.rightContent}>
          <Image
            source={ARROW_GRAY_ICON}
            style={styles.icon}
            resizeMode="contain"
          />
        </View>
      )}
    </TouchableOpacity>
  );
};

ListItem.ListTypes = {
  onPress: PropTypes.func,
  isArrow: PropTypes.bool,
  noBorder: PropTypes.bool,
  checked: PropTypes.bool,
  title: PropTypes.string,
  contentContainerStyle: PropTypes.object,
};

export default ListItem;
