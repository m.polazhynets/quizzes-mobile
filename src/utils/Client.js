// @flow
/**
 * Client
 * @module Client
 */
import axios from 'axios';
import api_config from '../config';
import {store} from '../store/configureStore';

/**
 * Axios data
 *
 * @param {string} url
 * @param {Object} options
 * @param {string} [options.method] - Request method ( GET, POST, PUT, ... ).
 * @param {string} [options.payload] - Request body.
 * @param {Object} [options.headers]
 * @param {Object} [options.params] - Request params.
 *
 * @returns {Promise}
 */

export function request(url, options = {}) {
  const {authReducer} = store.getState();
  const {settingsReducer} = store.getState();

  const config = {
    method: 'GET',
    ...options,
  };
  const errors = [];

  if (!url) {
    errors.push('url');
  }

  if (
    !config.payload &&
    (config.method !== 'GET' && config.method !== 'DELETE')
  ) {
    errors.push('payload');
  }

  if (errors.length) {
    throw new Error(`Error! You must pass \`${errors.join('`, `')}\``);
  }

  const headerParams = {};
  if (authReducer.access_token) {
    headerParams.Authorization = `Bearer ${authReducer.access_token}`;
  }

  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Accept-Language': settingsReducer.lang_code,
    'Offset-Minutes': new Date().getTimezoneOffset(),
    ...headerParams,
    ...config.headers,
  };

  const params = {
    headers,
    method: config.method,
    params: config.params,
    data: config.payload,
    withCredentials: true,
  };

  return axios({
    url: /^(https?:\/\/)/.test(url) ? url : `${api_config.domain}${url}`,
    ...params,
  })
    .then(async response => {
      if (response.data.status >= 400) {
        throw response.data;
      }
      return response.data;
    })
    .catch(async err => {
      throw err;
    });
}
