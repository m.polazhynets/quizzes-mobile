import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TrackPlayer from 'react-native-track-player';
import i18n from 'i18next';
import {Progress} from '../index';
import Toast from 'react-native-simple-toast';
import {
  COLOR_ROSE_DARKEN,
  COLOR_ROSE,
} from '../../../../../../constants/Colors';
import {
  PLAY_ICON,
  PAUSE_ICON,
  REPEAT_ICON,
} from '../../../../../../constants/Images';
import styles from './styles';

const RED = [COLOR_ROSE_DARKEN, COLOR_ROSE];

export default class AudioComponent extends Component {
  state = {
    isLoaded: false,
    trackState: null,
  };

  async componentDidMount() {
    const {link} = this.props;

    if (link) {
      try {
        await TrackPlayer.setupPlayer();
        await TrackPlayer.reset();
        await TrackPlayer.add([
          {
            id: 'unique track id',
            url: link,
            title: 'quiz track',
            artist: 'quiz artist',
          },
        ]);
        await TrackPlayer.updateOptions({
          stopWithApp: true,
          capabilities: [],
          compactCapabilities: [],
          alwaysPauseOnInterruption: false,
        });
        this.onPressControl();
      } catch (e) {
        Toast.show(
          i18n.t('common:errors.audio_initialization_error'),
          Toast.LONG,
        );
        console.log(e);
      }
    }
  }

  componentDidUpdate(prevProps) {
    const {play, startTimer} = this.props;

    if (prevProps.play !== play && !play) {
      TrackPlayer.pause().then(() => {
        startTimer();
        this.setState({trackState: 'pause'});
      });
    }
  }

  componentWillUnmount() {
    const {link} = this.props;

    if (link) {
      TrackPlayer.reset();
    }
  }

  onPressControl = () => {
    const {startTimer, tooglePlay} = this.props;
    const {trackState} = this.state;

    if (!trackState || trackState === 'pause') {
      TrackPlayer.play().then(() => {
        tooglePlay(true);
        this.setState({trackState: 'play'});
      });
    } else if (trackState === 'play') {
      startTimer();
      TrackPlayer.pause().then(() => {
        this.setState({trackState: 'pause'});
      });
    } else if (trackState === 'finish') {
      TrackPlayer.reset().then(() => {
        TrackPlayer.add([
          {
            id: 'unique track id',
            title: 'quiz track',
            url: this.props.data.audio,
            artist: 'quiz artist',
          },
        ]);
        TrackPlayer.play();
        this.setState({trackState: 'play'});
      });
    }
  };

  onTrackFinish = () => {
    this.props.startTimer();
    this.setState({trackState: 'finish'});
  };

  render() {
    const {trackState} = this.state;

    return (
      <View style={styles.audioWrapper}>
        <Progress onFinish={this.onTrackFinish} />
        <LinearGradient
          start={{x: 0.5, y: 0.5}}
          end={{x: 1, y: 0.5}}
          colors={RED}
          style={styles.buttonControl}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.buttonControlContent}
            onPress={this.onPressControl}>
            <Image
              source={
                !trackState || trackState === 'pause'
                  ? PLAY_ICON
                  : trackState === 'play'
                  ? PAUSE_ICON
                  : REPEAT_ICON
              }
              style={styles.controlIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </LinearGradient>
      </View>
    );
  }
}
