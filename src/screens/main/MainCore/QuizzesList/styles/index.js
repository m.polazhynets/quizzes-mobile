import {StyleSheet} from 'react-native';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
} from '../../../../../constants/StylesConstants';
import {COLOR_WHITE} from '../../../../../constants/Colors';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  flex: {
    flex: 1,
  },

  transparentStyle: {
    backgroundColor: 'transparent',
  },
  content: {
    backgroundColor: 'transparent',
    paddingHorizontal: scale(24),
    paddingVertical: v_scale(10),
    marginTop: v_scale(6),
  },
  emptyListText: {
    fontSize: regular,
    color: COLOR_WHITE,
    fontFamily: FONT_REGULAR,
  },
});
