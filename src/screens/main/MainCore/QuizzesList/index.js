import React, {Component} from 'react';
import {
  ImageBackground,
  View,
  Text,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import RadialGradient from 'react-native-radial-gradient';
import {CommonActions} from '@react-navigation/native';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {
  getQuizzesList,
  cleareAnswersQuizzes,
  getQuestionsListQuizzes,
} from '../../../../actions/quizzesActions';
import {Container, Preloader} from '../../../../components';
import {Item} from './components';
import {BACKGROUND_1_IMAGE} from '../../../../constants/Images';
import {COLOR_BLUE_3_DARKEN, COLOR_BLUE_3} from '../../../../constants/Colors';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import styles from './styles';

const BLUE = [COLOR_BLUE_3_DARKEN, COLOR_BLUE_3];

class QuizzesListScreen extends Component {
  state = {
    is_refresh: false,
  };

  componentDidMount() {
    this.props.getQuizzesList();
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      if (this.state.is_refresh) {
        this.setState({is_refresh: false});
      }
    }
  }

  onClick = id => {
    const {answers} = this.props;

    if (!answers || !answers[id]) {
      this.props.cleareAnswersQuizzes(id);
    }
    this.props.getQuestionsListQuizzes(id);
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 2,
        routes: [
          {name: MainFlowKeys.Home, params: {disableStartRequest: true}},
          {name: MainFlowKeys.QuizzesList},
          {
            name: MainFlowKeys.QuestionQuizzes,
            params: {id},
          },
        ],
      }),
    );
  };

  render() {
    const {t, is_request, quizzes_list} = this.props;
    const {is_refresh} = this.state;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={BLUE}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.flex}>
          <Container
            enableStatusBar={false}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.content}
            headerProps={{
              title: t('main_flow:all_quizzes'),
              whiteBackBtn: true,
              backgroundStart: 'transparent',
              backgroundFinish: COLOR_BLUE_3_DARKEN,
            }}
            childrenComponentProps={{
              refreshControl: (
                <RefreshControl
                  refreshing={is_refresh}
                  onRefresh={() => {
                    this.setState({is_refresh: true});
                    this.props.getQuizzesList();
                  }}
                />
              ),
            }}>
            {is_request && !is_refresh && <Preloader />}
            {quizzes_list &&
            !(is_request && !is_refresh) &&
            quizzes_list.length ? (
              quizzes_list.map(item => (
                <Item key={item.id} {...item} onPress={this.onClick} />
              ))
            ) : (
              <View>
                <Text style={styles.emptyListText}>
                  {t('main_flow:no_quizzes_available')}
                </Text>
              </View>
            )}
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.quizzesReducer.is_request,
  quizzes_list: state.quizzesReducer.quizzes_list,
  answers: state.quizzesReducer.answers,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {getQuizzesList, cleareAnswersQuizzes, getQuestionsListQuizzes},
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(QuizzesListScreen),
);
