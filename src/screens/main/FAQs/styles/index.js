import {StyleSheet} from 'react-native';
import {COLOR_GRAY_5, COLOR_DARK} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_BOLD,
} from '../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
  },
  text: {
    color: COLOR_GRAY_5,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
    marginBottom: v_scale(10),
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: regular,
    color: COLOR_DARK,
    paddingLeft: scale(40),
  },
  headerList: {
    paddingVertical: v_scale(8),
    marginBottom: 0,
    position: 'relative',
    justifyContent: 'center',
  },
  contentItem: {
    flex: 1,
    paddingVertical: v_scale(8),
    marginBottom: 0,
    paddingLeft: 3,
  },
  icon: {
    width: scale(12),
    height: scale(12),
    position: 'absolute',
    left: 0,
  },
  bottomContent: {
    padding: scale(24),
    alignItems: 'center',
    zIndex: 2,
  },
  button: {
    width: scale(260),
  },
});
