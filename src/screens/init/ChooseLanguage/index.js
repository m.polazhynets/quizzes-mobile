import React, {Component} from 'react';
import {View, Image, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import RadialGradient from 'react-native-radial-gradient';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {ListItem, Container} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {AuthFlowKeys} from '../../../navigator/Keys';
import {CONCEPT_IMAGE} from '../../../constants/Images';
import {setLanguage} from '../../../actions/settingsActions';
import {COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN} from '../../../constants/Colors';
import {BACKGROUND_3_IMAGE} from '../../../constants/Images';
import {deviceWidth, deviceHeight} from '../../../constants/StylesConstants';
import styles from './styles';

const BACKGROUND = [COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN];

const LIST = [
  {
    id: 'ru',
    title: 'Русский',
  },
  {
    id: 'en',
    title: 'English',
  },
  // {
  //   id: 'zh-hans',
  //   title: '中文',
  // },
  // {
  //   id: 'fr',
  //   title: 'Français',
  // },
  // {
  //   id: 'mg',
  //   title: 'Монгол',
  // },
  // {
  //   id: 'ko',
  //   title: '한국인',
  // },
  // {
  //   id: 'az',
  //   title: 'Azərbaycan',
  // },
];

class ChooseLanguageScreen extends Component {
  render() {
    return (
      <RadialGradient
        style={styles.flex}
        colors={BACKGROUND}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_3_IMAGE} style={styles.flex}>
          <Container
            headerEnabled={false}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}>
            <View style={styles.content}>
              <View style={styles.imageWrapper}>
                <Image
                  source={CONCEPT_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.list}>
                {LIST.map((item, index) => (
                  <ListItem
                    key={item.id}
                    {...item}
                    noBorder={LIST.length === index + 1}
                    onPress={() => {
                      this.props.setLanguage(item.id);
                      this.props.i18n.changeLanguage(item.id);
                      navigate(AuthFlowKeys.ChooseAuth);
                    }}
                  />
                ))}
              </View>
            </View>
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators({setLanguage}, dispatch);

export default withTranslation(['common'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChooseLanguageScreen),
);
