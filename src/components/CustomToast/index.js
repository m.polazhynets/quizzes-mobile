import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toggleCustomToast} from '../../actions/settingsActions';
import i18n from 'i18next';
import {CHECK_GREEN_ICON} from '../../constants/Images';
import styles from './styles';

class CustomToast extends Component {
  componentDidUpdate(prevProps) {
    const {show} = this.props;

    if (prevProps.show !== show && show) {
      setTimeout(() => {
        this.props.toggleCustomToast(false);
      }, 1000);
    }
  }

  render() {
    const {show} = this.props;

    if (!show) {
      return null;
    }

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Image
            source={CHECK_GREEN_ICON}
            style={styles.icon}
            resizeMode="contain"
          />
          <Text style={styles.text}>
            {show.label || i18n.t('common:texts.saved')}
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  show: state.settingsReducer.show_custom_toast,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({toggleCustomToast}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomToast);
