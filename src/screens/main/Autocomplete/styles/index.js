import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import {COLOR_DARK} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  deviceHeight,
} from '../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../components/Header/styles';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    height: deviceHeight * 0.4,
  },
  itemContent: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    paddingVertical: scale(19),
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  itemText: {
    color: COLOR_DARK,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
  },
  list: {
    maxHeight: Platform.OS === 'android' ? deviceHeight : deviceHeight * 0.35,
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(27),
  },
  headerButton: {
    position: 'absolute',
    top:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    right: scale(10),
    height: HEADER_MIN_HEIGHT,
    paddingTop: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
    opacity: 0.2,
  },
  activeButton: {
    opacity: 1,
  },
});
