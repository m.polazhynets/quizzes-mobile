import {StyleSheet} from 'react-native';
import variable, {
  scale,
  v_scale,
  FONT_MEDIUM,
} from '../../../../../../../constants/StylesConstants';
import {COLOR_WHITE} from '../../../../../../../constants/Colors';

const {mediumRegular} = variable.fontSize;

export default StyleSheet.create({
  itemWrapper: {
    borderRadius: scale(10),
    overflow: 'hidden',
    borderWidth: 4,
    borderColor: 'rgba(255, 255, 255, 0.3)',
    marginBottom: v_scale(18),
  },
  itemContent: {
    paddingHorizontal: scale(10),
    paddingVertical: v_scale(16),
    position: 'relative',
    justifyContent: 'center',
  },
  itemImage: {
    width: '100%',
    height: '90%',
    position: 'absolute',
    top: -v_scale(10),
    left: 0,
    right: 0,
    zIndex: 2,
  },
  itemText: {
    color: COLOR_WHITE,
    fontFamily: FONT_MEDIUM,
    fontSize: mediumRegular,
  },
  itemIcon: {
    opacity: 0.3,
    position: 'absolute',
    right: 0,
    height: v_scale(50),
  },
  catleIcon: {
    width: scale(41),
    height: scale(41),
    position: 'absolute',
    right: scale(5),
  },
});
