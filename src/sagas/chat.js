/**
 * @module Sagas/Chat
 * @desc Chat
 */

import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {CHAT} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getChatsList({payload: {page}}) {
  try {
    const response = yield call(request, 'chats/', {
      method: 'GET',
      params: {page},
    });
    yield put({
      type: CHAT.GET_CHATS_LIST_SUCCESS,
      payload: response,
      additionalPayload: {page},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: CHAT.GET_CHATS_LIST,
          payload: {page},
        },
      });
    } else {
      yield put({
        type: CHAT.GET_CHATS_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* deleteChat({payload: {id}}) {
  try {
    const response = yield call(request, `chats/${id}/`, {
      method: 'DELETE',
    });
    yield put({
      type: CHAT.DELETE_CHAT_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: CHAT.DELETE_CHAT,
          payload: {id},
        },
      });
    } else {
      yield put({
        type: CHAT.DELETE_CHAT_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getHistoryChat({payload: {id, page}}) {
  try {
    const response = yield call(request, `chats/${id}/`, {
      method: 'GET',
      params: {page},
    });
    yield put({
      type: CHAT.GET_HISTORY_CHAT_SUCCESS,
      payload: response,
      additionalPayload: {id, page},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: CHAT.GET_HISTORY_CHAT,
          payload: {id, page},
        },
      });
    } else {
      yield put({
        type: CHAT.GET_HISTORY_CHAT_FAILURE,
        payload: err.response,
        additionalPayload: {id, page},
      });
    }
  }
}

export function* sendMessage({payload: {user_id, message, chat_id, callback}}) {
  try {
    const response = yield call(request, 'chats/messaging/', {
      method: 'POST',
      payload: {user_id, message},
    });
    yield put({
      type: CHAT.SEND_MESSAGE_SUCCESS,
      payload: response,
      additionalPayload: {user_id, message, chat_id},
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: CHAT.SEND_MESSAGE,
          payload: {user_id, message, chat_id, callback},
        },
      });
    } else {
      if (callback) {
        callback(false);
      }
      yield put({
        type: CHAT.SEND_MESSAGE_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* makeChatRead({payload: {id}}) {
  try {
    const response = yield call(request, `chats/${id}/make_read/`, {
      method: 'POST',
      payload: {},
    });
    yield put({
      type: CHAT.MAKE_CHAT_READ_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: CHAT.MAKE_CHAT_READ,
          payload: {id},
        },
      });
    } else {
      yield put({
        type: CHAT.MAKE_CHAT_READ_FAILURE,
        payload: err.response,
      });
    }
  }
}

/**
 * Chat Sagas
 */
export default function* root() {
  yield all([takeLatest(CHAT.GET_CHATS_LIST, getChatsList)]);
  yield all([takeLatest(CHAT.DELETE_CHAT, deleteChat)]);
  yield all([takeLatest(CHAT.GET_HISTORY_CHAT, getHistoryChat)]);
  yield all([takeLatest(CHAT.SEND_MESSAGE, sendMessage)]);
  yield all([takeLatest(CHAT.MAKE_CHAT_READ, makeChatRead)]);
}
