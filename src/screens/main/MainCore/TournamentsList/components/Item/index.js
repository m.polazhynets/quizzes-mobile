import React, {Component} from 'react';
import {ImageBackground, Text, View, Image} from 'react-native';
import i18n from 'i18next';
import moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {Button, ButtonLink} from '../../../../../../components';
import {navigate} from '../../../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../../../navigator/Keys';
import {
  BACKGROUND_2_IMAGE,
  PRIZE_ICON,
  PEOPLE_ICON,
  PLACE_ICON,
} from '../../../../../../constants/Images';
import styles from './styles';

function getTimeToDate(date) {
  const days = moment(date).diff(moment(), 'days');
  const hours = moment(date).diff(moment(), 'hours') % 24;
  const minutes = moment(date).diff(moment(), 'minutes') % 60;

  return `${days}${i18n.t('common:texts:short_day')} : ${hours}${i18n.t(
    'common:texts:short_hour',
  )} : ${minutes}${i18n.t('common:texts:short_minute')}`;
}

class TournamentCard extends Component {
  state = {
    started: moment(this.props.start_date).isBefore(),
    stringToStart: getTimeToDate(this.props.start_date),
    stringToFinish: getTimeToDate(this.props.end_date),
  };
  timer = null;

  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({
        stringToStart: getTimeToDate(this.props.start_date),
        stringToFinish: getTimeToDate(this.props.end_date),
      });
    }, 30000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    const {
      id,
      initial_bid,
      title,
      players_count,
      questions_count,
      total_sum,
      finished,
      onPress,
      is_joined,
      terms_of_participation,
      t,
    } = this.props;
    const {started, stringToStart, stringToFinish} = this.state;

    return (
      <ImageBackground
        source={BACKGROUND_2_IMAGE}
        key={id}
        style={styles.itemContent}>
        <Text style={styles.title}>{title}</Text>
        {/* <Text style={styles.text}>{`${i18n.t(
          'main_flow:subject',
        )}: ${description}`}</Text> */}
        <View style={styles.row}>
          <View style={styles.item}>
            <Image
              source={PEOPLE_ICON}
              style={styles.icon}
              resizeMode="contain"
            />
            <Text style={styles.text}>{players_count}</Text>
          </View>
          <View style={styles.item}>
            <Image
              source={PLACE_ICON}
              style={styles.icon}
              resizeMode="contain"
            />
            <Text style={styles.text}>{questions_count}</Text>
          </View>
          <View style={styles.item}>
            <Image
              source={PRIZE_ICON}
              style={styles.icon}
              resizeMode="contain"
            />
            <Text style={styles.text}>{total_sum} P</Text>
          </View>
        </View>
        <View style={[styles.row, styles.alignStart]}>
          <ButtonLink
            title={
              finished
                ? i18n.t('main_flow:standings')
                : i18n.t('main_flow:list_of_claimed_participants')
            }
            onPress={() => onPress('participants')}
            contentContainerStyle={styles.link}
            style={styles.linkText}
          />
          <ButtonLink
            title={i18n.t('main_flow:terms_of_participation')}
            onPress={() => {
              navigate(MainFlowKeys.TermsParticipation, {
                content: terms_of_participation,
              });
            }}
            contentContainerStyle={styles.link}
            style={styles.linkText}
          />
        </View>
        <View style={[styles.row, styles.noMargin]}>
          <View>
            <Text style={styles.label}>
              {started ? t('main_flow:ends_in') : t('main_flow:starts_in')}:
            </Text>
            <Text style={[styles.textBold, started && styles.regular]}>
              {started ? stringToFinish : stringToStart}
            </Text>
          </View>
          {started ? (
            <Button
              contentContainerStyle={styles.button}
              title={
                is_joined
                  ? t('common:buttons.to_play')
                  : `${t('common:buttons.participate')}\n${initial_bid} P`
              }
              type="red"
              onPress={() => onPress(is_joined ? 'play' : 'join')}
            />
          ) : (
            <Button
              contentContainerStyle={styles.button}
              title={
                is_joined
                  ? t('common:buttons.leave_tournament')
                  : `${t('common:buttons.participate')}\n${initial_bid} P`
              }
              type="red"
              onPress={() => onPress(is_joined ? 'leave' : 'join')}
            />
            // <View>
            //   <Text style={styles.label}>
            //     {i18n.t('main_flow:cost_of_participation')}:
            //   </Text>
            //   <Text style={styles.textBold}>{initial_bid} P</Text>
            // </View>
          )}
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TournamentCard),
);
