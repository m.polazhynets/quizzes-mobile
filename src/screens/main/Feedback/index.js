import React, {Component} from 'react';
import {Text, View, Keyboard} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {sendFeedback} from '../../../actions/generalActions';
import {toggleCustomToast} from '../../../actions/settingsActions';
import {Container, Input, Button} from '../../../components';
import styles from './styles';

class FeedbackScreen extends Component {
  state = {
    form: {
      name: {
        value: '',
        isValid: null,
        error: '',
      },
      subject: {
        value: '',
        isValid: null,
        error: '',
      },
      text: {
        value: '',
        isValid: null,
        error: '',
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {is_sent_feedback, t} = this.props;

    if (prevProps.is_sent_feedback !== is_sent_feedback && is_sent_feedback) {
      this.props.toggleCustomToast(true, t('common:texts.sent'));
      this.clearForm();
    }
  }

  clearForm = () => {
    this.setState({
      form: {
        name: {
          value: '',
          isValid: null,
          error: '',
        },
        subject: {
          value: '',
          isValid: null,
          error: '',
        },
        text: {
          value: '',
          isValid: null,
          error: '',
        },
      },
    });
  };

  validate = (val, name) => {
    const {form} = this.state;

    this.setState({
      form: {
        ...form,
        [name]: {
          ...form[name],
          value: val,
          isValid: val.length > 3,
        },
      },
    });
  };

  checkFullValidate = async () => {
    const {
      form: {name, subject, text},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            name: {
              ...name,
              isValid: name.isValid === true,
            },
            subject: {
              ...subject,
              isValid: subject.isValid === true,
            },
            text: {
              ...text,
              isValid: text.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.name.isValid && form.subject.isValid && form.text.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {profile} = this.props;
    const {
      form: {name, subject, text},
    } = this.state;

    try {
      await this.checkFullValidate();

      Keyboard.dismiss();
      this.props.sendFeedback(
        name.value,
        subject.value,
        profile.email,
        text.value,
      );
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {name, subject, text},
    } = this.state;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:feedback'),
        }}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Text style={styles.text}>
              {t('main_flow:feedback_description')}
            </Text>
            <Input
              label={t('common:form.your_name')}
              value={name.value}
              required
              isValid={name.isValid}
              onBlur={() => this.validate(name.value, 'name')}
              onChangeText={val => this.validate(val, 'name')}
            />
            <Input
              label={t('common:form.question_subject')}
              value={subject.value}
              required
              isValid={subject.isValid}
              onBlur={() => this.validate(subject.value, 'subject')}
              onChangeText={val => this.validate(val, 'subject')}
            />
            <Input
              label={t('common:form.your_question')}
              value={text.value}
              required
              isValid={text.isValid}
              onBlur={() => this.validate(text.value, 'text')}
              onChangeText={val => this.validate(val, 'text')}
            />
          </View>
          <View style={styles.bottomContent}>
            <Button
              contentContainerStyle={styles.button}
              title={t('common:buttons.send')}
              type="red"
              isLoading={is_request}
              onPress={this.handleSubmit}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.generalReducer.is_request,
  is_sent_feedback: state.generalReducer.is_sent_feedback,
  profile: state.profileReducer.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({sendFeedback, toggleCustomToast}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(FeedbackScreen),
);
