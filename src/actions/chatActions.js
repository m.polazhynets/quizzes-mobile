import {createActions} from 'redux-actions';

import {CHAT} from '../constants/ActionTypes';

export const {
  getChatsList,
  deleteChat,
  getHistoryChat,
  sendMessage,
  getNewMessage,
  removeCurrentChatStore,
  makeChatRead,
} = createActions({
  [CHAT.GET_CHATS_LIST]: page => ({page}),
  [CHAT.DELETE_CHAT]: id => ({id}),
  [CHAT.GET_HISTORY_CHAT]: (id, page) => ({id, page}),
  [CHAT.SEND_MESSAGE]: (user_id, message, chat_id, callback) => ({
    user_id,
    message,
    chat_id,
    callback,
  }),
  [CHAT.GET_NEW_MESSAGE]: data => ({data}),
  [CHAT.REMOVE_CURRENT_CHAT_STORE]: () => ({}),
  [CHAT.MAKE_CHAT_READ]: id => ({id}),
});
