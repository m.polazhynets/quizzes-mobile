import React, {Component} from 'react';
import {
  RefreshControl,
  View,
  Text,
  Image,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import moment from 'moment';
import {SafeAreaView} from 'react-native-safe-area-context';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {getNotificationList} from '../../../actions/notificationActions';
import {Header, Preloader, Container} from '../../../components';
import {BRAIN_IMAGE, ARROW_GRAY_ICON} from '../../../constants/Images';
import {COLOR_ROSE_DARKEN} from '../../../constants/Colors';
import styles from './styles';

class NotificationsListScreen extends Component {
  header = React.createRef();
  state = {
    refreshing: false,
    firstLoading: false,
  };

  componentDidMount() {
    this.getData(false, 1);
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      this.setState({refreshing: false, firstLoading: false});
    }
  }

  getData = (refreshing, page) => {
    const {filter_params, is_request} = this.props;

    if (is_request) {
      return;
    }

    this.setState({refreshing, firstLoading: !refreshing}, () => {
      this.props.getNotificationList(page || filter_params.page + 1);
    });
  };

  renderItem = (item, index) => {
    const {list} = this.props;
    const hideTime =
      list[index - 1] &&
      moment(item.created).isSame(list[index - 1].created, 'day');

    return (
      <React.Fragment>
        {!hideTime && (
          <Text style={styles.labelText}>
            {moment(item.created).format('dd, DD MMMM')}
          </Text>
        )}
        <TouchableOpacity
          activeOpacity={item.notification_type === 'tournament' ? 0.8 : 1}
          onPress={() => {
            if (item.notification_type === 'tournament') {
              navigate(MainFlowKeys.TournamentsList);
            }
          }}
          style={[styles.itemList, !hideTime && styles.noBorder]}>
          <View style={[styles.itemContent]}>
            <Text style={styles.text}>{item.title}</Text>
            <View style={styles.textBlock}>
              <Text style={styles.text}>{item.text}</Text>
            </View>
          </View>
          {item.notification_type === 'tournament' && (
            <View style={styles.rightContent}>
              <Image
                source={ARROW_GRAY_ICON}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
          )}
        </TouchableOpacity>
      </React.Fragment>
    );
  };

  renderFooter = () => {
    if (!this.props.is_request || !this.props.list || !this.props.list.length) {
      return null;
    }

    if (this.state.firstLoading || this.state.refreshing) {
      return null;
    }

    return (
      <View>
        <ActivityIndicator animating size="large" color={COLOR_ROSE_DARKEN} />
      </View>
    );
  };

  renderEmptyComponent = () => {
    const {t} = this.props;

    return <Text style={styles.emptyText}>{t('common:texts.list_empty')}</Text>;
  };

  render() {
    const {t, list, filter_params} = this.props;
    const {refreshing, firstLoading} = this.state;

    return (
      <View style={styles.content}>
        {firstLoading && <Preloader />}
        <Image
          source={BRAIN_IMAGE}
          resizeMode="cover"
          style={styles.background}
        />
        <Container
          headerProps={{
            title: t('main_flow:notification'),
            headerStyle: styles.zIndex,
          }}
          enableBrainBackground
          childrenType="flatlist"
          scrollStyles={styles.transparentStyle}
          contentContainerStyle={styles.transparentStyle}
          childrenComponentProps={{
            keyExtractor: (item, index) => `${item.id}_${index}`,
            data: list,
            renderItem: ({item, index}) => this.renderItem(item, index),
            ListEmptyComponent: this.renderEmptyComponent,
            ListFooterComponent: this.renderFooter,
            refreshing: refreshing,
            onRefresh: () => this.getData(true, 1),
            onEndReached: () => {
              if (!filter_params.last_page) {
                this.getData(false);
              }
            },
            style: styles.scrollContentStyles,
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.notificationReducer.is_request,
  list: state.notificationReducer.list,
  filter_params: state.notificationReducer.list_filter_params,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getNotificationList}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(NotificationsListScreen),
);
