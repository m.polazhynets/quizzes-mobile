/**
 * @module Sagas/Auth
 * @desc Auth
 */

import {all, takeLatest, call, put, select} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {AUTH, ERRORS, GENERAL, SETTINGS} from '../constants/ActionTypes';
import {replace} from '../navigator/RootNavigation';
import {AppFlowKeys, AuthFlowKeys} from '../navigator/Keys';
const getRefreshToken = state => state.authReducer.refresh_token;
const getFCMToken = state => state.authReducer.fcm_token;

export function* getConfirmationCode({payload: {email, reset}}) {
  try {
    const response = yield call(request, 'get_confirmation_code/', {
      method: 'POST',
      payload: {email, reset},
    });
    yield put({
      type: AUTH.GET_CONFIRMATION_CODE_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.GET_CONFIRMATION_CODE_FAILURE,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* verifyConfirmationCode({payload: {code, email, password}}) {
  try {
    const response = yield call(request, 'verify_confirmation_code/', {
      method: 'POST',
      payload: {code},
    });
    if (email && password) {
      yield signUp({payload: {email, password}});
    }
    yield put({
      type: AUTH.VERIFY_CONFIRMATION_CODE_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.VERIFY_CONFIRMATION_CODE_FAILURE,
      payload: err.response,
    });
  }
}

export function* signUp({payload: {email, password}}) {
  try {
    const response = yield call(request, 'registration/', {
      method: 'POST',
      payload: {email, password},
    });
    yield put({
      type: AUTH.SING_UP_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.SING_UP_FAILURE,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* signIn({payload: {email, password}}) {
  try {
    const response = yield call(request, 'login/', {
      method: 'POST',
      payload: {email, password},
    });
    yield put({
      type: AUTH.LOG_IN_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.LOG_IN_FAILURE,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* socialSignIn({payload: {access_token, provider}}) {
  try {
    const response = yield call(request, 'social/login/', {
      method: 'POST',
      payload: {access_token, provider},
    });
    yield put({
      type: AUTH.SOCIAL_LOG_IN_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.SOCIAL_LOG_IN_FAILUREs,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* logOut(payload) {
  try {
    if (!payload) {
      yield removeFCMToken({payload: {enable_push: null}});
    }
    yield put({
      type: AUTH.LOG_OUT_SUCCESS,
    });
    yield put({
      type: GENERAL.SET_APP_LOADED_PARAM,
      payload: false,
    });
  } catch (err) {
    yield put({
      type: AUTH.LOG_OUT_SUCCESS,
    });
    yield put({
      type: GENERAL.SET_APP_LOADED_PARAM,
      payload: false,
    });
  }
}

export function* resetPassword({payload: {email, password}}) {
  try {
    const response = yield call(request, 'reset_password/', {
      method: 'POST',
      payload: {email, password},
    });
    yield put({
      type: AUTH.RESET_PASSWORD_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.RESET_PASSWORD_FAILURE,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* changePassword({payload: {password_old, password_new}}) {
  try {
    const response = yield call(request, 'change_password/', {
      method: 'POST',
      payload: {password_old, password_new},
    });
    yield put({
      type: AUTH.CHANGE_PASSWORD_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: AUTH.CHANGE_PASSWORD_FAILURE,
      payload: err.response,
    });

    if (err.response?.data) {
      yield put({
        type: ERRORS.SET_ERROR,
        payload: err.response.data,
      });
    }
  }
}

export function* refreshToken({payload}) {
  const refresh = yield select(getRefreshToken);

  try {
    const response = yield call(request, 'refresh/', {
      method: 'POST',
      payload: {refresh},
    });
    yield put({
      type: AUTH.REFRESH_TOKEN_SUCCESS,
      payload: response,
    });
    yield put(payload);
  } catch (err) {
    yield put({
      type: AUTH.REFRESH_TOKEN_FAILURE,
      payload: err.response,
    });
    yield logOut({payload: {noRequest: true}});
    replace(AppFlowKeys.Auth, {
      screen: AuthFlowKeys.ChooseAuth,
      params: {
        headerEnabled: true,
      },
    });
  }
}

export function* setFCMToken({payload: {notification_token}}) {
  try {
    const response = yield call(request, 'device_token/add/', {
      method: 'PUT',
      payload: {notification_token},
    });
    yield put({
      type: AUTH.SET_FCM_TOKEN_SUCCESS,
      payload: response,
      additionalPayload: {notification_token},
    });
  } catch (err) {
    yield put({
      type: AUTH.SET_FCM_TOKEN_FAILURE,
      payload: err.response,
    });
  }
}

export function* removeFCMToken({payload: {enable_push}}) {
  const notification_token = yield select(getFCMToken);
  try {
    const response = yield call(request, 'device_token/remove/', {
      method: 'PUT',
      payload: {notification_token},
    });
    yield put({
      type: AUTH.REMOVE_FCM_TOKEN_SUCCESS,
      payload: response,
    });
    if (enable_push !== null) {
      yield put({
        type: SETTINGS.TOGGLE_ENABLE_PUSH_NOTIFICATIONS,
        payload: {enable_push},
      });
    }
  } catch (err) {
    yield put({
      type: AUTH.REMOVE_FCM_TOKEN_FAILURE,
      payload: err.response,
    });
  }
}

/**
 * Auth Sagas
 */
export default function* root() {
  yield all([
    takeLatest(AUTH.GET_CONFIRMATION_CODE, getConfirmationCode),
    takeLatest(AUTH.VERIFY_CONFIRMATION_CODE, verifyConfirmationCode),
    takeLatest(AUTH.LOG_IN, signIn),
    takeLatest(AUTH.SOCIAL_LOG_IN, socialSignIn),
    takeLatest(AUTH.LOG_OUT, logOut),
    takeLatest(AUTH.RESET_PASSWORD, resetPassword),
    takeLatest(AUTH.CHANGE_PASSWORD, changePassword),
    takeLatest(AUTH.REFRESH_TOKEN, refreshToken),
    takeLatest(AUTH.SET_FCM_TOKEN, setFCMToken),
    takeLatest(AUTH.REMOVE_FCM_TOKEN, removeFCMToken),
  ]);
}
