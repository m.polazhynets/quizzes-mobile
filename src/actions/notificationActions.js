import {createActions} from 'redux-actions';

import {NOTICATIONS} from '../constants/ActionTypes';

export const {
  showNotification,
  hideNotification,
  getNotificationList,
} = createActions({
  [NOTICATIONS.SHOW_NOTIFICATION]: message => ({message}),
  [NOTICATIONS.HIDE_NOTIFICATION]: () => ({}),
  [NOTICATIONS.GET_NOTIFICATION_LIST]: page => ({page}),
});
