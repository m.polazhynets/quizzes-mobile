import {StyleSheet} from 'react-native';
import {
  isIphoneX,
  getBottomSpace,
  getStatusBarHeight,
} from 'react-native-iphone-x-helper';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '../../Header/styles';
import {scale, v_scale} from '../../../constants/StylesConstants';
import {COLOR_WHITE_DARKEN} from '../../../constants/Colors';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  scrollContentStyles: {
    flexGrow: 1,
    zIndex: 2,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  emptyBlock: {
    height: isIphoneX()
      ? HEADER_MIN_HEIGHT - getStatusBarHeight() - v_scale(14)
      : HEADER_MIN_HEIGHT - getStatusBarHeight(),
  },
  paddingTopFlatList: {
    paddingTop: HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT,
  },
  paddingTop: {
    paddingTop: isIphoneX()
      ? HEADER_MAX_HEIGHT - getStatusBarHeight() - v_scale(14)
      : HEADER_MAX_HEIGHT - getStatusBarHeight(),
  },
  background: {
    position: 'absolute',
    width: scale(220),
    height: scale(220),
    left: 0,
    bottom: isIphoneX() ? getBottomSpace() + v_scale(30) : v_scale(30),
  },
  zIndex: {
    zIndex: 1,
  },
});
