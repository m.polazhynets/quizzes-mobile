import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getBottomSpace} from 'react-native-iphone-x-helper';
import {
  COLOR_GREEN_3,
  COLOR_GRAY_1,
  COLOR_DARK,
  COLOR_WHITE_DARKEN,
} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_BOLD,
} from '../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  scrollContentStyles: {
    flexGrow: 1,
    zIndex: 2,
    padding: scale(24),
    paddingTop: 0,
    backgroundColor: 'transparent',
  },
  zIndex: {
    zIndex: Platform.OS === 'android' ? 0 : 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  text: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
  },
  itemList: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: scale(1),
    borderColor: COLOR_GRAY_1,
  },
  itemContent: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'baseline',
    paddingVertical: v_scale(24),
  },
  colorGreen: {
    color: COLOR_GREEN_3,
  },
  noBorder: {
    borderTopWidth: 0,
  },
  labelText: {
    color: COLOR_DARK,
    fontFamily: FONT_BOLD,
    fontSize: regular,
    marginVertical: v_scale(5),
  },
  background: {
    position: 'absolute',
    width: scale(220),
    height: scale(220),
    left: 0,
    bottom: isIphoneX() ? getBottomSpace() + v_scale(30) : v_scale(30),
  },
  textBlock: {
    flex: 1,
    paddingTop: v_scale(5),
  },
  icon: {
    width: scale(14),
    height: scale(14),
  },
});
