import {StyleSheet} from 'react-native';
import {COLOR_DARK} from '../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
  },
  title: {
    color: COLOR_DARK,
    fontFamily: FONT_BOLD,
    fontSize: regular,
    lineHeight: v_scale(24),
    marginVertical: v_scale(15),
  },
  label: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
  },
  switchBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: v_scale(15),
  },
  noMargin: {
    marginVertical: v_scale(0),
  },
});
