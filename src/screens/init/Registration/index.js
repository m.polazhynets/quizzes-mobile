import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from 'i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getConfirmationCode,
  verifyConfirmationCode,
} from '../../../actions/authActions';
import {navigate, replace} from '../../../navigator/RootNavigation';
import {AuthFlowKeys} from '../../../navigator/Keys';
import {Button, Container, Input} from '../../../components';
import {validateEmail, validatePassword} from '../../../utils/Helper';
import styles from './styles';

class RegistrationScreen extends Component {
  state = {
    form: {
      email: {
        value: '',
        isValid: null,
        error: '',
      },
      password: {
        value: '',
        isValid: null,
        error: '',
      },
      confirm_password: {
        value: '',
        isValid: null,
        error: i18n.t('common:errors.password_do_not_match'),
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {code_send, access_token, t} = this.props;

    if (prevProps.code_send !== code_send && code_send) {
      navigate(AuthFlowKeys.ConfirmCode, {
        headerTitle: t('auth_flow:registration'),
        text: t('auth_flow:registration_code_text'),
        requestAction: this.props.verifyConfirmationCode,
        requestData: [
          this.state.form.email.value,
          this.state.form.password.value,
        ],
        callBack: () => {
          if (access_token) {
            replace(AuthFlowKeys.PinCode, {
              createPin: true,
              email: this.state.form.email.value,
            });
          } else {
            replace(AuthFlowKeys.Registration);
          }
        },
      });
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'password':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val),
            },
            confirm_password: {
              ...form.confirm_password,
              isValid: form.confirm_password.value
                ? validatePassword(form.confirm_password.value, val)
                : null,
            },
          },
        });
        break;
      case 'confirm_password':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val, form.password.value),
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateEmail(val),
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {email, password, confirm_password},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            email: {
              ...email,
              isValid: email.isValid === true,
            },
            password: {
              ...password,
              isValid: password.isValid === true,
            },
            confirm_password: {
              ...confirm_password,
              isValid: confirm_password.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.email.isValid &&
          form.password.isValid &&
          form.confirm_password.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {email},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.getConfirmationCode(email.value, false);
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {email, password, confirm_password},
    } = this.state;

    return (
      <Container
        headerProps={{
          title: t('auth_flow:registration'),
        }}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Input
              label={t('common:form.email')}
              value={email.value}
              required
              isValid={email.isValid}
              onBlur={() => this.validate(email.value, 'email')}
              onChangeText={val => this.validate(val, 'email')}
            />
            <Input
              label={t('common:form.password')}
              value={password.value}
              required
              secureTextEntry
              isValid={password.isValid}
              onBlur={() => this.validate(password.value, 'password')}
              onChangeText={val => this.validate(val, 'password')}
            />
            <Input
              label={t('common:form.repeat_password')}
              value={confirm_password.value}
              required
              secureTextEntry
              isValid={confirm_password.isValid}
              onBlur={() =>
                this.validate(confirm_password.value, 'confirm_password')
              }
              onChangeText={val => this.validate(val, 'confirm_password')}
              errorMessage={
                confirm_password.value ? confirm_password.error : ''
              }
            />
          </View>
          <View style={styles.bottomContent}>
            <Button
              contentContainerStyle={styles.button}
              title={t('common:buttons.sign_up')}
              type="red"
              isLoading={is_request}
              onPress={this.handleSubmit}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.authReducer.is_request,
  code_send: state.authReducer.code_send,
  access_token: state.authReducer.code_send,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getConfirmationCode, verifyConfirmationCode}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(RegistrationScreen),
);
