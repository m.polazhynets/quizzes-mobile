import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {
  Text,
  ImageBackground,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {navigate} from '../../../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../../../navigator/Keys';
import {getTournamentParticipantsList} from '../../../../../../actions/tournamentsActions';
import {Preloader} from '../../../../../../components';
import {
  BACKGROUND_2_IMAGE,
  PROFILE_ICON,
} from '../../../../../../constants/Images';
import styles from './styles';

class ModalParticipants extends Component {
  componentDidUpdate(prevProps) {
    const {tournament_id, isVisible} = this.props;
    if (prevProps.isVisible !== isVisible && isVisible) {
      this.props.getTournamentParticipantsList(tournament_id);
    }
  }

  renderItem = ({item, index}) => {
    const {participants_list, onCancel, profile} = this.props;
    return (
      <TouchableOpacity
        style={[
          styles.item,
          participants_list.length === index + 1 && styles.noBorder,
        ]}
        key={item.id}
        activeOpacity={0.8}
        onPress={() => {
          onCancel();
          if (profile.id === item.id) {
            navigate(MainFlowKeys.Profile);
          } else {
            navigate(MainFlowKeys.User, {id: item.id});
          }
        }}>
        <Text style={[styles.itemTitle, styles.black]}>{index + 1}</Text>
        <View style={styles.itemAvatarWrapper}>
          {item.photo ? (
            <Image
              style={styles.photo}
              source={{uri: item.photo}}
              resizeMode="cover"
            />
          ) : (
            <Image
              source={PROFILE_ICON}
              resizeMode="contain"
              style={styles.avatarIcon}
            />
          )}
        </View>
        <Text style={styles.itemText}>{item.nickname}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {
      isVisible,
      participants_list,
      onCancel,
      is_modal_request,
      t,
    } = this.props;
    return (
      <Modal isVisible={isVisible} onBackdropPress={onCancel} useNativeDriver>
        {is_modal_request && <Preloader />}
        <ImageBackground
          source={BACKGROUND_2_IMAGE}
          style={styles.modalContent}>
          <Text style={styles.title}>
            {t('main_flow:list_of_claimed_participants')}
          </Text>
          {participants_list && (
            <ScrollView contentContainerStyle={styles.list}>
              {participants_list.length ? (
                participants_list.map((item, index) =>
                  this.renderItem({item, index}),
                )
              ) : (
                <View>
                  <Text style={styles.emptyListText}>
                    {t('common:texts.list_empty')}
                  </Text>
                </View>
              )}
            </ScrollView>
          )}
        </ImageBackground>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profileReducer.profile,
  is_modal_request: state.tournamentsReducer.is_modal_request,
  participants_list: state.tournamentsReducer.participants_list,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getTournamentParticipantsList}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ModalParticipants),
);
