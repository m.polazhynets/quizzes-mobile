import React from 'react';
import {View, Text} from 'react-native';
import Modal from 'react-native-modal';
import {Button, ButtonLink} from '../../../../../components';
import styles from './styles';

export default ({isVisible, data}) => {
  if (!data) {
    return null;
  }
  return (
    <Modal isVisible={isVisible} useNativeDriver>
      <View style={styles.modalContent}>
        {data.title && <Text style={styles.title}>{data.title}</Text>}
        {data.texts &&
          data.texts.map((item, index) => {
            if (!item) {
              return null;
            }
            return (
              <Text style={styles.text} key={index}>
                {item}
              </Text>
            );
          })}
        <View style={styles.buttonsContent}>
          {data.buttons &&
            data.buttons.map((item, index) => {
              if (!item) {
                return null;
              }
              return (
                <Button
                  key={index}
                  contentContainerStyle={styles.button}
                  margin
                  {...item}
                />
              );
            })}
          {data.links &&
            data.links.map((item, index) => {
              if (!item) {
                return null;
              }
              return (
                <ButtonLink
                  key={index}
                  contentContainerStyle={styles.link}
                  {...item}
                />
              );
            })}
        </View>
      </View>
    </Modal>
  );
};
