import React, {Component} from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import moment from 'moment';
import {getPaymentHistory} from '../../../actions/profileActions';
import {Container, Preloader} from '../../../components';
import {PURCHASE_ICON, BRAIN_IMAGE} from '../../../constants/Images';
import {COLOR_ROSE_DARKEN} from '../../../constants/Colors';
import styles from './styles';

class TransactionHistoryScreen extends Component {
  header = React.createRef();
  state = {
    refreshing: false,
    firstLoading: false,
  };

  componentDidMount() {
    this.getData(false, 1);
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      this.setState({refreshing: false, firstLoading: false});
    }
  }

  getData = (refreshing, page) => {
    const {payment_filter_params, is_request} = this.props;

    if (is_request) {
      return;
    }

    this.setState({refreshing, firstLoading: !refreshing}, () => {
      this.props.getPaymentHistory(page || payment_filter_params.page + 1);
    });
  };

  renderItem = (item, index) => {
    const {payment_list} = this.props;
    const hideTime =
      payment_list[index - 1] &&
      moment(item.created).isSame(payment_list[index - 1].created, 'day');

    return (
      <React.Fragment>
        {!hideTime && (
          <Text style={styles.labelText}>
            {moment(item.created).format('dd, DD MMMM')}
          </Text>
        )}
        <View style={styles.itemList}>
          <Image
            source={PURCHASE_ICON}
            resizeMode="contain"
            style={styles.icon}
          />
          <View style={[styles.itemContent, !hideTime && styles.noBorder]}>
            <View style={styles.textWrapper}>
              <Text style={styles.text}>{item.text}</Text>
            </View>
            <Text
              style={[
                styles.text,
                item.operation_type === 'income' && styles.colorGreen,
              ]}>
              {`${item.operation_type === 'income' ? '+' : '-'} ${item.sum}P`}
            </Text>
          </View>
        </View>
      </React.Fragment>
    );
  };

  renderFooter = () => {
    if (
      !this.props.is_request ||
      !this.props.payment_list ||
      !this.props.payment_list.length
    ) {
      return null;
    }

    if (this.state.firstLoading || this.state.refreshing) {
      return null;
    }

    return (
      <View>
        <ActivityIndicator animating size="large" color={COLOR_ROSE_DARKEN} />
      </View>
    );
  };

  renderEmptyComponent = () => {
    const {t} = this.props;

    return <Text style={styles.emptyText}>{t('common:texts.list_empty')}</Text>;
  };

  render() {
    const {t, payment_list, payment_filter_params} = this.props;
    const {refreshing, firstLoading} = this.state;

    return (
      <View style={styles.content}>
        {firstLoading && <Preloader />}
        <Image
          source={BRAIN_IMAGE}
          resizeMode="cover"
          style={styles.background}
        />
        <Container
          headerProps={{
            title: t('main_flow:notification'),
            headerStyle: styles.zIndex,
          }}
          enableBrainBackground
          childrenType="flatlist"
          scrollStyles={styles.transparentStyle}
          contentContainerStyle={styles.transparentStyle}
          childrenComponentProps={{
            keyExtractor: (item, index) => `${item.id}_${index}`,
            data: payment_list,
            renderItem: ({item, index}) => this.renderItem(item, index),
            ListEmptyComponent: this.renderEmptyComponent,
            ListFooterComponent: this.renderFooter,
            refreshing: refreshing,
            onRefresh: () => this.getData(true, 1),
            onEndReached: () => {
              if (!payment_filter_params.last_page) {
                this.getData(false);
              }
            },
            style: styles.scrollContentStyles,
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  payment_list: state.profileReducer.payment_list,
  payment_filter_params: state.profileReducer.payment_filter_params,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getPaymentHistory}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TransactionHistoryScreen),
);
