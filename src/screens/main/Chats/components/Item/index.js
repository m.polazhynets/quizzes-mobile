import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, View, TouchableOpacity, Image, Animated} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import moment from 'moment';
import i18n from 'i18next';
import {RectButton} from 'react-native-gesture-handler';
import {Bange} from '../../../../../components';
import {PROFILE_ICON, REMOVE_ICON} from '../../../../../constants/Images';
import styles from './styles';

function renderDate(date) {
  if (moment().isSame(date, 'day')) {
    return moment(date).format('HH:mm');
  } else if (
    moment()
      .add(-1, 'day')
      .isSame(date, 'day')
  ) {
    return i18n.t('common:texts.yesterday');
  } else {
    return moment(date).format('DD.MM.YYYY');
  }
}

export default class Item extends Component {
  _swipeableRow = React.createRef();

  updateRef = ref => {
    this._swipeableRow = ref;
  };

  close = () => {
    this._swipeableRow.close();
  };

  renderRightAction = (key, color, source, x, progress) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });
    const pressHandler = () => {
      this.props.onRemove();
      this._swipeableRow.close();
    };
    const wrapperStyles = {
      flex: 1,
      transform: [{translateX: trans}],
    };

    return (
      <Animated.View style={wrapperStyles}>
        <RectButton
          style={[styles.swipeButton, styles[`background_${color}`]]}
          onPress={pressHandler}>
          <Image
            source={source}
            style={styles.swipeIcon}
            resizeMode={'contain'}
          />
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActions = progress => {
    return (
      <View style={styles.rightContainer}>
        {this.renderRightAction('delete', 'red', REMOVE_ICON, 60, progress)}
      </View>
    );
  };

  render() {
    const {user, last_message, not_read_count, onPress} = this.props;

    return (
      <Swipeable
        ref={this.updateRef}
        renderRightActions={this.renderRightActions}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={onPress}
          style={styles.item}>
          <View style={styles.leftSide}>
            <View style={styles.imageWrapper}>
              <Image
                source={user.photo ? {uri: user.photo} : PROFILE_ICON}
                resizeMode={user.photo ? 'cover' : 'contain'}
                style={user.photo ? styles.fullAvatar : styles.avatar}
              />
            </View>
            <View>
              <Text style={styles.name}>{user.nickname}</Text>
              <Text style={styles.text} numberOfLines={1}>
                {last_message.text}
              </Text>
            </View>
          </View>
          <View style={styles.rightSide}>
            <Text style={styles.text}>{renderDate(last_message.created)}</Text>
            {not_read_count > 0 ? (
              <Bange
                count={not_read_count}
                contentContainerStyle={styles.badge}
              />
            ) : (
              <View style={styles.emptyBlock} />
            )}
          </View>
        </TouchableOpacity>
      </Swipeable>
    );
  }
}

Item.ListTypes = {
  onPress: PropTypes.func,
  onRemove: PropTypes.func,
  last_message: PropTypes.object,
  user: PropTypes.object,
};
