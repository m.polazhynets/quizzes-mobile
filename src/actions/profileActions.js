import {createActions} from 'redux-actions';

import {PROFILE} from '../constants/ActionTypes';

export const {
  getProfile,
  updateProfile,
  changeEmail,
  verifyEmail,
  changeLanguage,
  getStatistics,
  getRatingList,
  getUserProfile,
  addQuizTipsLifes,
  removeQuizTipsLifes,
  addTournamentTipsLifes,
  getPaymentHistory,
  getDisbursement,
} = createActions({
  [PROFILE.GET_PROFILE]: () => ({}),
  [PROFILE.UPDATE_PROFILE]: payload => ({payload}),
  [PROFILE.CHANGE_EMAIL]: email => ({email}),
  [PROFILE.VERIFY_EMAIL]: code => ({code}),
  [PROFILE.CHANGE_LANGUAGE]: lang_code => ({lang_code}),
  [PROFILE.GET_STATISTICS]: type => ({type}),
  [PROFILE.GET_RATING_LIST]: page => ({page}),
  [PROFILE.GET_USER_PROFILE]: id => ({id}),
  [PROFILE.ADD_QUIZ_TIPS_LIFES]: (
    target,
    method,
    count,
    package_id,
    callback,
  ) => ({
    target,
    method,
    count,
    package_id,
    callback,
  }),
  [PROFILE.REMOVE_QUIZ_TIPS_LIFES]: (target, callback) => ({
    target,
    callback,
  }),
  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES]: (target, price, callback) => ({
    target,
    price,
    callback,
  }),
  [PROFILE.GET_PAYMENT_HISTORY]: page => ({page}),
  [PROFILE.GET_DISBURSEMENT]: (sum, text, callback) => ({
    sum,
    text,
    callback,
  }),
});
