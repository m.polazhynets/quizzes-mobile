import {StyleSheet} from 'react-native';
import {COLOR_GRAY_5, COLOR_BLUE} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
  },
  text: {
    color: COLOR_GRAY_5,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
    marginBottom: v_scale(10),
  },
  link: {
    color: COLOR_BLUE,
  },
});
