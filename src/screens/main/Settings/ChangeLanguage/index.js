import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {changeLanguage} from '../../../../actions/profileActions';
import {Container, ListItem} from '../../../../components';
import styles from './styles';

const LIST = [
  {
    id: 'ru',
    title: 'Русский',
  },
  {
    id: 'en',
    title: 'English',
  },
  // {
  //   id: 'zh-hans',
  //   title: '中文',
  // },
  // {
  //   id: 'fr',
  //   title: 'Français',
  // },
  // {
  //   id: 'mg',
  //   title: 'Монгол',
  // },
  // {
  //   id: 'ko',
  //   title: '한국인',
  // },
  // {
  //   id: 'az',
  //   title: 'Azərbaycan',
  // },
];

class ChangeLanguageScreen extends Component {
  render() {
    const {lang_code, i18n} = this.props;

    return (
      <Container enableBrainBackground>
        <View style={styles.content}>
          {LIST.map((item, index) => (
            <ListItem
              {...item}
              key={item.id}
              checked={item.id === lang_code}
              noBorder={LIST.length === index + 1}
              onPress={() => {
                this.props.changeLanguage(item.id);
                i18n.changeLanguage(item.id);
              }}
            />
          ))}
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  lang_code: state.settingsReducer.lang_code,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({changeLanguage}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChangeLanguageScreen),
);
