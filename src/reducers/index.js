import {combineReducers} from 'redux';
import authReducer from './AuthReducer';
import settingsReducer from './SettingsReducer';
import errorsReducer from './ErrorsReducer';
import profileReducer from './ProfileReducer';
import generalReducer from './GeneralReducer';
import tournamentsReducer from './TournamentsReducer';
import quizzesReducer from './QuizzesReducer';
import notificationReducer from './NotificationReducer';
import chatReducer from './ChatReducer';

const rootReducer = combineReducers({
  authReducer,
  settingsReducer,
  errorsReducer,
  profileReducer,
  generalReducer,
  tournamentsReducer,
  quizzesReducer,
  notificationReducer,
  chatReducer,
});

export default rootReducer;
