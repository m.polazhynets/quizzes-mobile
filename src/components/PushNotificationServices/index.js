import {Component} from 'react';
import {connect} from 'react-redux';
import {Vibration} from 'react-native';
import {bindActionCreators} from 'redux';

import messaging from '@react-native-firebase/messaging';
import {setFcmToken} from '../../actions/authActions';
import {getNewMessage, getChatsList} from '../../actions/chatActions';
import {showNotification} from '../../actions/notificationActions';

const ONE_SECOND_IN_MS = 200;

const PATTERN = [
  1 * ONE_SECOND_IN_MS,
  1 * ONE_SECOND_IN_MS,
  1 * ONE_SECOND_IN_MS,
  1 * ONE_SECOND_IN_MS,
];

class PushNotificationService extends Component {
  componentDidUpdate(prevProps) {
    const {app_is_full_loaded, enable_push} = this.props;

    if (
      prevProps.app_is_full_loaded !== app_is_full_loaded &&
      app_is_full_loaded
    ) {
      this.start();
    }
    if (prevProps.enable_push !== enable_push && enable_push) {
      this.start();
    }
  }

  async start() {
    this.checkPermission();
    this.createNotificationListeners();
  }

  async checkPermission() {
    const enabled = await messaging().requestPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.checkPermission();
    }
  }

  async getToken() {
    const {access_token, fcm_token, enable_push} = this.props;

    if (!fcm_token && access_token && enable_push) {
      const token = await messaging().getToken();
      if (token) {
        this.props.setFcmToken(token);
      }
    }
  }

  async createNotificationListeners() {
    messaging().onMessage(async remoteMessage => {
      if (!remoteMessage.notification && remoteMessage.data) {
        if (
          !this.props.chat_id ||
          this.props.chat_id !== +remoteMessage.data.chat_id
        ) {
          Vibration.vibrate(PATTERN);
          this.props.getChatsList(1);
        }
        this.props.getNewMessage(remoteMessage.data);
      }
      if (remoteMessage.notification) {
        Vibration.vibrate(PATTERN);
        this.props.showNotification(remoteMessage.notification);
      }
    });
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  fcm_token: state.authReducer.fcm_token,
  access_token: state.authReducer.access_token,
  app_is_full_loaded: state.generalReducer.app_is_full_loaded,
  enable_push: state.settingsReducer.enable_push,
  chat_id: state.chatReducer.chat_id,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {setFcmToken, showNotification, getNewMessage, getChatsList},
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PushNotificationService);
