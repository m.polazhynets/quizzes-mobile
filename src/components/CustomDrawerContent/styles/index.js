import {StyleSheet, Platform} from 'react-native';
import {
  getStatusBarHeight,
  getBottomSpace,
  isIphoneX,
} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../constants/StylesConstants';
import {
  COLOR_WHITE,
  COLOR_DARK,
  COLOR_GRAY_1,
  COLOR_GRAY_5,
} from '../../../constants/Colors';

const {large, small, regular} = variables.fontSize;

export const DRAWER_WIDTH = scale(320);

export default StyleSheet.create({
  content: {
    backgroundColor: COLOR_WHITE,
    flex: 1,
    paddingHorizontal: scale(24),
    justifyContent: 'space-between',
    paddingVertical: v_scale(10),
  },
  flex: {
    flex: 1,
  },
  header: {
    paddingTop: getStatusBarHeight(),
    paddingBottom: v_scale(30),
  },
  topHeader: {
    paddingHorizontal: scale(10),
    paddingTop: Platform.OS === 'android' ? v_scale(16) : getStatusBarHeight(),
    justifyContent: 'center',
  },
  closeButton: {
    paddingHorizontal: scale(14),
    paddingBottom: v_scale(10),
  },
  closeIcon: {
    width: scale(18),
    height: scale(18),
  },
  profileIcon: {
    width: scale(26),
    height: v_scale(27),
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  imageWrapper: {
    width: scale(60),
    height: scale(60),
    backgroundColor: 'rgba(234, 234, 234, 0.5)',
    borderRadius: scale(30),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(14),
    overflow: 'hidden',
  },
  profileBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: v_scale(20),
    paddingHorizontal: scale(24),
  },
  leftSide: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_WHITE,
    marginBottom: v_scale(5),
  },
  email: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_WHITE,
    opacity: 0.5,
  },
  arrowIcon: {
    width: scale(12),
    height: scale(12),
    marginLeft: scale(5),
  },
  itemContent: {
    height: v_scale(60),
    borderBottomWidth: scale(1),
    borderColor: COLOR_GRAY_1,
    flex: 1,
    marginLeft: scale(26),
    position: 'relative',
    justifyContent: 'center',
    paddingRight: scale(20),
  },
  itemText: {
    color: COLOR_DARK,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
  },
  iconMenu: {
    width: scale(27),
    height: scale(27),
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuArrow: {
    position: 'absolute',
    right: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  noBorder: {
    borderBottomWidth: 0,
    flex: 0,
  },
  bottomContent: {
    paddingBottom: getBottomSpace(),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  versionText: {
    color: COLOR_GRAY_5,
    fontFamily: FONT_REGULAR,
    fontSize: small,
  },
  background: {
    position: 'absolute',
    width: scale(220),
    height: scale(220),
    left: 0,
    bottom: isIphoneX() ? getBottomSpace() : v_scale(50),
  },
});
