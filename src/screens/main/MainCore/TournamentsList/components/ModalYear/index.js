import React from 'react';
import Modal from 'react-native-modal';
import {View, Text, ImageBackground} from 'react-native';
import i18n from 'i18next';
import {Button} from '../../../../../../components';
import {BACKGROUND_2_IMAGE} from '../../../../../../constants/Images';
import styles from './styles';

export default ({isVisible, onSuccess, onCancel}) => {
  return (
    <Modal isVisible={isVisible} useNativeDriver>
      <ImageBackground source={BACKGROUND_2_IMAGE} style={styles.modalContent}>
        <Text style={styles.title}>{i18n.t('main_flow:your_age')}</Text>
        <View style={styles.textWrapper}>
          <Text style={styles.text}>{i18n.t('main_flow:you_nave')} </Text>
          <Text style={[styles.text, styles.bold]}>
            {i18n.t('main_flow:18_years_old')}
          </Text>
        </View>
        <View style={styles.buttonsWrapper}>
          <Button
            contentContainerStyle={styles.button}
            title={i18n.t('common:buttons.yes')}
            type="yellow"
            onPress={onSuccess}
          />
          <Button
            contentContainerStyle={styles.button}
            title={i18n.t('common:buttons.no')}
            type="gray"
            onPress={onCancel}
          />
        </View>
      </ImageBackground>
    </Modal>
  );
};
