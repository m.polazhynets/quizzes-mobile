import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {AuthStack, MainStack} from './stacks';
import {LoadingScreen} from './Screens';
import {AppFlowKeys} from './Keys';

const Stack = createStackNavigator();

function RootStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <Stack.Screen name={AppFlowKeys.Loading} component={LoadingScreen} />
      <Stack.Screen name={AppFlowKeys.Main} component={MainStack} />
      <Stack.Screen name={AppFlowKeys.Auth} component={AuthStack} />
    </Stack.Navigator>
  );
}

export default RootStack;
