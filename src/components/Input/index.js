import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, TextInput, Text, Animated} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import styles, {
  TOP_START,
  TOP_FINISH,
  COLOR_START,
  COLOR_FINISH,
  BACKGROUND_COLOR_START,
  BACKGROUND_COLOR_FINISH,
  SIZE_START,
  SIZE_FINISH,
} from './styles';

class Input extends Component {
  state = {
    isFocused: this.props.value ? true : false,
  };
  animatedIsFocused = new Animated.Value(this.props.value ? 1 : 0);

  componentDidUpdate() {
    const {isFocused} = this.state;
    const {value} = this.props;

    Animated.timing(this.animatedIsFocused, {
      toValue: isFocused || value ? 1 : 0,
      duration: 100,
    }).start();
  }

  handleFocus = () => {
    const {onFocus} = this.props;

    if (onFocus) {
      onFocus();
    }

    this.setState({isFocused: true});
  };

  handleBlur = () => {
    const {onBlur, value} = this.props;

    if (onBlur) {
      onBlur();
    }
    if (value) {
      return;
    }
    this.setState({isFocused: false});
  };

  render() {
    const {
      mask,
      value,
      containerStyles,
      inputStyles,
      maskProps,
      required,
      isValid,
      errorMessage,
      label,
      noPadding,
      ...props
    } = this.props;

    let _maskProps = {},
      _Component = TextInput;
    if (mask) {
      _maskProps = maskProps || {
        type: 'custom',
        options: {
          withDDD: true,
          mask,
        },
      };
      _Component = TextInputMask;
    }

    const labelStyle = {
      color: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [COLOR_START, COLOR_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      top: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [TOP_START, TOP_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      fontSize: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [SIZE_START, SIZE_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      backgroundColor: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [BACKGROUND_COLOR_START, BACKGROUND_COLOR_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
    };

    return (
      <View style={[styles.container, noPadding && styles.noPadding]}>
        <View
          style={[
            styles.inputContainerStyle,
            containerStyles,
            isValid && styles.successStyle,
            required && isValid === false && styles.errorStyle,
          ]}>
          {label && (
            <Animated.Text
              numberOfLines={1}
              style={[
                styles.label,
                labelStyle,
                isValid && styles.successLabelStyle,
                required && isValid === false && styles.errorLabelStyle,
              ]}>
              {label}
            </Animated.Text>
          )}
          <View style={styles.inputWrapper}>
            <_Component
              {...props}
              {..._maskProps}
              value={value}
              autoCapitalize={'none'}
              style={[
                styles.input,
                required && isValid === false && styles.inputError,
                inputStyles,
              ]}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
            />
          </View>
        </View>
        {required && isValid === false && errorMessage ? (
          <Text numberOfLines={1} style={styles.errorMessageStyles}>
            {errorMessage}
          </Text>
        ) : (
          <View />
        )}
      </View>
    );
  }
}

export default Input;

Input.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  mask: PropTypes.string,
  errorMessage: PropTypes.string,
  noPadding: PropTypes.bool,
  required: PropTypes.bool,
  isValid: PropTypes.bool,
  containerStyles: PropTypes.object,
  onBlur: PropTypes.func,
  inputStyles: PropTypes.object,
};
