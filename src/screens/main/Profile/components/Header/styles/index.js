import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  deviceWidth,
} from '../../../../../../constants/StylesConstants';
import {COLOR_WHITE, COLOR_DARK} from '../../../../../../constants/Colors';

const {large, regular} = variables.fontSize;

export const HEADER_MIN_HEIGHT = isIphoneX()
  ? 110 + getStatusBarHeight()
  : 80 + getStatusBarHeight();
export const HEADER_MAX_HEIGHT = isIphoneX()
  ? 185 + getStatusBarHeight()
  : 145 + getStatusBarHeight();
export const TITLE_BOTTOM_START = isIphoneX() ? 84 : 70;
export const TITLE_BOTTOM_FINISH = isIphoneX() ? 62 : 48;
export const TITLE_LEFT_START = scale(24);
export const TITLE_LEFT_FINISH = scale(70);
export const TITLE_FONT_SIZE_START = large;
export const TITLE_FONT_SIZE_FINISH = regular;
export const TITLE_FONT_WEIGHT_START = '700';
export const TITLE_FONT_WEIGHT_FINISH = '400';
export const IMAGE_WIDTH_START = scale(130);
export const IMAGE_WIDTH_FINISH = scale(80);
export const IMAGE_HEIGHT_START = v_scale(110);
export const IMAGE_HEIGHT_FINISH = v_scale(70);
export const IMAGE_BOTTOM_START = v_scale(10);
export const IMAGE_BOTTOM_FINISH = v_scale(40);
export const IMAGE_LEFT_START = deviceWidth - scale(154);
export const IMAGE_LEFT_FINISH = deviceWidth - scale(100);

export default StyleSheet.create({
  arrowIcon: {
    width: scale(30),
    height: v_scale(20),
  },
  backButton: {
    padding: scale(15),
    width: scale(60),
    zIndex: 2,
  },
  headerWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 3,
  },
  headerContainer: {
    paddingHorizontal: scale(10),
    paddingTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    flex: 1,
  },
  titleStyle: {
    zIndex: 2,
    fontFamily: FONT_REGULAR,
    color: COLOR_DARK,
  },
  whiteTitle: {
    color: COLOR_WHITE,
  },
  backgroundImage: {
    position: 'relative',
  },
});
