import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import {
  COLOR_DARK,
  COLOR_WHITE,
  COLOR_GRAY_0,
  COLOR_GRAY_6,
  COLOR_RED,
} from '../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_MEDIUM,
  FONT_REGULAR,
} from '../../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../../components/Header/styles';

const {mediumRegular, small, regular} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scrollContentStyles: {
    padding: scale(24),
    paddingTop: v_scale(10),
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
  button: {
    width: scale(230),
    marginTop: v_scale(8),
  },
  content: {
    paddingHorizontal: scale(24),
    paddingVertical: v_scale(10),
  },
  itemOffer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: COLOR_WHITE,
    padding: scale(20),
    borderRadius: scale(10),
    borderWidth: scale(2),
    borderColor: COLOR_GRAY_0,
    marginBottom: v_scale(20),
  },
  offerTitle: {
    fontFamily: FONT_MEDIUM,
    fontSize: mediumRegular,
    color: COLOR_DARK,
    marginBottom: v_scale(5),
  },
  oldText: {
    fontFamily: FONT_REGULAR,
    marginTop: v_scale(3),
    fontSize: small,
    color: COLOR_GRAY_6,
    textDecorationLine: 'line-through',
  },
  giftText: {
    fontFamily: FONT_REGULAR,
    marginTop: v_scale(3),
    fontSize: small,
    color: COLOR_RED,
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  headerButton: {
    position: 'absolute',
    top:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    right: scale(10),
    height: HEADER_MIN_HEIGHT,
    paddingTop: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
  },
  emptyListText: {
    fontSize: regular,
    color: COLOR_WHITE,
    fontFamily: FONT_REGULAR,
  },
});
