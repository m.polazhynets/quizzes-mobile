import React from 'react';
import {View, Text} from 'react-native';
import Switch from 'react-native-switch-pro';
import {COLOR_GRAY_1, COLOR_ROSE} from '../../../../../../constants/Colors';
import {scale} from '../../../../../../constants/StylesConstants';
import styles from './styles';

export default ({label, value, onPress}) => {
  return (
    <View style={styles.switchBlock}>
      <Text style={styles.label}>{label}</Text>
      <Switch
        value={value}
        onSyncPress={onPress}
        backgroundInactive={COLOR_GRAY_1}
        backgroundActive={COLOR_ROSE}
        width={scale(38)}
        height={scale(22)}
      />
    </View>
  );
};
