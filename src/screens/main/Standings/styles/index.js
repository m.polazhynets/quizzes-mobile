import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';
import {
  HEADER_MAX_HEIGHT,
  HEADER_MIN_HEIGHT,
} from '../components/Header/styles';
import {
  COLOR_WHITE_DARKEN,
  COLOR_DARK,
  COLOR_GRAY_5,
  COLOR_GRAY_0,
} from '../../../../constants/Colors';

const {large, small, regular} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  scrollContentStyles: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: isIphoneX()
      ? HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT + v_scale(14)
      : HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT,
    position: 'relative',
  },
  zIndex: {
    zIndex: 2,
  },
  emptyBlock: {
    height: isIphoneX()
      ? HEADER_MIN_HEIGHT - getStatusBarHeight() - v_scale(14)
      : HEADER_MIN_HEIGHT - getStatusBarHeight() + v_scale(20),
  },
  paddingTopFlatList: {
    paddingTop: HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT,
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_DARK,
  },
  headerTable: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: v_scale(10),
    paddingTop: v_scale(20),
  },
  headerTitle: {
    fontFamily: FONT_BOLD,
    fontSize: small,
    color: COLOR_GRAY_5,
  },
  leftSide: {
    flex: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightSide: {
    flex: 1,
    justifyContent: 'center',
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: v_scale(10),
    alignItems: 'center',
    borderBottomWidth: scale(1),
    borderColor: COLOR_GRAY_0,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  black: {
    color: COLOR_DARK,
  },
  tableText: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_DARK,
  },
  itemAvatarWrapper: {
    width: scale(36),
    height: scale(36),
    backgroundColor: COLOR_GRAY_0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(18),
    marginLeft: scale(10),
    marginRight: scale(12),
    overflow: 'hidden',
  },
  itemAvatar: {
    width: scale(16),
    height: scale(16),
  },
  itemAvatarLarge: {
    width: scale(36),
    height: scale(36),
  },
  moreButton: {
    position: 'absolute',
    right: -scale(10),
    padding: scale(10),
  },
  moreIcon: {
    width: scale(3),
    height: v_scale(17),
  },
});
