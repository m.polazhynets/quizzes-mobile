import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  deviceWidth,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';
import {HEADER_MAX_HEIGHT} from '../components/Header/styles';
import {
  COLOR_WHITE_DARKEN,
  COLOR_GRAY_2,
  COLOR_GRAY_5,
  COLOR_DARK,
} from '../../../../constants/Colors';

export const LEFT_AVATAR_START = scale(24);
export const LEFT_AVATAR_FINISH = (deviceWidth - v_scale(80)) / 2;
export const TOP_AVATAR_START = isIphoneX() ? v_scale(148) : v_scale(130);
export const TOP_AVATAR_FINISH = isIphoneX() ? v_scale(100) : v_scale(82);
export const AVATAR_SIZE_START = v_scale(134);
export const AVATAR_SIZE_FINISH = v_scale(80);

const {large, regular, small} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  scrollContentStyles: {
    flexGrow: 1,
    paddingTop: isIphoneX()
      ? HEADER_MAX_HEIGHT - getStatusBarHeight() + v_scale(70)
      : HEADER_MAX_HEIGHT - getStatusBarHeight() + v_scale(90),
    position: 'relative',
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  headerButton: {
    position: 'absolute',
    right: scale(10),
    marginTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    paddingVertical: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
  },
  avatarWrapper: {
    position: 'absolute',
    zIndex: 3,
    borderRadius: AVATAR_SIZE_START,
    overflow: 'hidden',
    backgroundColor: COLOR_GRAY_2,
  },
  avatar: {
    width: '100%',
    height: '100%',
    borderWidth: scale(2),
    borderColor: 'rgba(242, 242, 242, 0.3)',
  },
  userInfoBlock: {
    paddingHorizontal: scale(24),
    paddingBottom: v_scale(20),
  },
  row: {
    marginBottom: v_scale(15),
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_DARK,
    lineHeight: v_scale(30),
  },
  smallTitle: {
    fontFamily: FONT_BOLD,
    fontSize: small,
    color: COLOR_DARK,
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_GRAY_5,
    lineHeight: v_scale(22),
    marginTop: v_scale(5),
  },
  textLineHeight: {
    lineHeight: v_scale(22),
  },
  alignStart: {
    alignItems: 'flex-start',
    lineHeight: v_scale(22),
  },
  item: {
    paddingVertical: v_scale(8),
    paddingLeft: scale(50),
  },
  itemIcon: {
    width: scale(22),
    height: scale(22),
    position: 'absolute',
    top: v_scale(5),
    left: 0,
  },
  noMargin: {
    marginBottom: 0,
  },
});
