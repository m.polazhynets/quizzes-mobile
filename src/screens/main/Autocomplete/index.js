import React, {Component} from 'react';
import {ScrollView, Text, TouchableOpacity, View, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {cleareAutocomplete} from '../../../actions/generalActions';
import {Container, Input} from '../../../components';
import {CHECK_BLACK_ICON} from '../../../constants/Images';
import styles from './styles';

class AutocompleteScreen extends Component {
  state = {
    headerTitle: this.props.route.params.headerTitle || '',
    requestParams: this.props.route.params.requestParams || [],
    onSelect: this.props.route.params.onSelect,
    action: this.props.route.params.action,
    isFocus: false,
    text: '',
    selected: null,
    typingTimeout: 0,
  };

  componentDidMount() {
    this.props.cleareAutocomplete();
  }

  renderHeaderButton = () => {
    const {selected, onSelect} = this.state;

    if (!selected) {
      return null;
    }

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {
          if (selected) {
            onSelect(selected);
            this.props.navigation.goBack();
          }
        }}
        style={[styles.headerButton, styles.activeButton]}>
        <Image
          source={CHECK_BLACK_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  onChangeText = val => {
    const {typingTimeout, action, requestParams} = this.state;

    if (typingTimeout) {
      clearTimeout(typingTimeout);
    }
    this.setState({text: val, selected: null});

    this.setState({
      typingTimeout: setTimeout(() => {
        action(val, ...requestParams);
      }, 500),
    });
  };

  renderItem = (item, index) => {
    const {list} = this.props;

    return (
      <TouchableOpacity
        key={item.id}
        activeOpacity={0.8}
        style={[
          styles.itemContent,
          index === list.length - 1 && styles.noBorder,
        ]}
        onPress={() => {
          this.setState({selected: item, text: item.name});
        }}>
        <Text style={styles.itemText}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {list, t} = this.props;
    const {headerTitle, text, isFocus} = this.state;

    return (
      <Container
        scrollEnabled={false}
        enableBrainBackground
        headerProps={{
          title: headerTitle,
          rightBtn: this.renderHeaderButton(),
        }}>
        <View style={styles.content}>
          <Input
            onFocus={() => this.setState({isFocus: true})}
            onBlur={() => this.setState({isFocus: false})}
            value={text}
            onChangeText={this.onChangeText}
            autoFocus
            placeholder={t('common:texts.use_latin_keyboard')}
          />
          {isFocus && (
            <ScrollView style={styles.list} keyboardShouldPersistTaps="always">
              {list.map((item, index) => this.renderItem(item, index))}
            </ScrollView>
          )}
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.generalReducer.is_request,
  list: state.generalReducer.autocomplete_data || [],
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({cleareAutocomplete}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AutocompleteScreen),
);
