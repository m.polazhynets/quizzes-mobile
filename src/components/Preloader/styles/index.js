import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 10,
    position: 'absolute',
    height: '100%',
    width: '100%',
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
});
