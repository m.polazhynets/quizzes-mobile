import React from 'react';
import {TouchableOpacity, Image, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  WAVE_IMAGE,
  PART_LOGO_IMAGE,
  CASTLE_ICON,
} from '../../../../../../constants/Images';
import {
  COLOR_BLUE_3_DARKEN,
  COLOR_BLUE_3,
} from '../../../../../../constants/Colors';
import styles from './styles';

const BLUE = [COLOR_BLUE_3_DARKEN, COLOR_BLUE_3];

export default ({available, id, title, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={available ? 0.8 : 1}
      onPress={() => {
        if (!available) {
          return null;
        }
        onPress(id);
      }}
      style={styles.itemWrapper}>
      <Image source={WAVE_IMAGE} style={styles.itemImage} resizeMode="cover" />
      <LinearGradient
        start={{x: 0.5, y: 0.5}}
        end={{x: 1, y: 0.5}}
        colors={BLUE}
        style={styles.itemContent}>
        <Image
          source={PART_LOGO_IMAGE}
          style={styles.itemIcon}
          resizeMode="contain"
        />
        {!available && (
          <Image
            source={CASTLE_ICON}
            style={styles.catleIcon}
            resizeMode="contain"
          />
        )}
        <Text style={styles.itemText}>{title}</Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};
