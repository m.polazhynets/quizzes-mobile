import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {
  ChooseLanguageScreen,
  ChooseAuthScreen,
  ChooseRegistrationScreen,
  RegistrationScreen,
  LoginScreen,
  ConfirmCodeScreen,
  ResetPasswordScreen,
  PinCodeScreen,
  NewPasswordScreen,
} from '../../Screens';
import {AuthFlowKeys} from '../../Keys';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <Stack.Screen
        name={AuthFlowKeys.ChooseLanguage}
        component={ChooseLanguageScreen}
      />
      <Stack.Screen
        name={AuthFlowKeys.ChooseAuth}
        component={ChooseAuthScreen}
      />
      <Stack.Screen
        name={AuthFlowKeys.ChooseRegistration}
        component={ChooseRegistrationScreen}
      />
      <Stack.Screen
        name={AuthFlowKeys.Registration}
        component={RegistrationScreen}
      />
      <Stack.Screen name={AuthFlowKeys.Login} component={LoginScreen} />
      <Stack.Screen
        name={AuthFlowKeys.ConfirmCode}
        component={ConfirmCodeScreen}
      />
      <Stack.Screen
        name={AuthFlowKeys.ResetPassword}
        component={ResetPasswordScreen}
      />
      <Stack.Screen name={AuthFlowKeys.PinCode} component={PinCodeScreen} />
      <Stack.Screen
        name={AuthFlowKeys.NewPassword}
        component={NewPasswordScreen}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
