import React from 'react';
import {View} from 'react-native';
import TrackPlayer from 'react-native-track-player';
import LinearGradient from 'react-native-linear-gradient';
import {
  COLOR_ROSE_DARKEN,
  COLOR_ROSE,
} from '../../../../../../constants/Colors';
import styles from './styles';

const RED = [COLOR_ROSE_DARKEN, COLOR_ROSE];

export default class Progress extends TrackPlayer.ProgressComponent {
  state = {
    width: 0,
  };
  finish = false;

  find_dimesions = layout => {
    const {width} = layout;
    this.setState({width: width});
  };

  render() {
    const {width} = this.state;

    if (this.getProgress() >= 1 && !this.finish) {
      this.finish = true;
      this.props.onFinish();
    }

    return (
      <View
        style={styles.progress}
        onLayout={event => this.find_dimesions(event.nativeEvent.layout)}>
        <LinearGradient
          start={{x: 0.5, y: 0.5}}
          end={{x: 1, y: 0.5}}
          colors={RED}
          style={[styles.leftSide, {width: width * this.getProgress()}]}
        />
      </View>
    );
  }
}
