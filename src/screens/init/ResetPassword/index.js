import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {navigate, replace} from '../../../navigator/RootNavigation';
import {AuthFlowKeys} from '../../../navigator/Keys';
import {
  getConfirmationCode,
  verifyConfirmationCode,
} from '../../../actions/authActions';
import {Button, Container, Input} from '../../../components';
import {validateEmail} from '../../../utils/Helper';
import styles from './styles';

class ResetScreen extends Component {
  state = {
    form: {
      email: {
        value: '',
        isValid: null,
        error: '',
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {code_send, t} = this.props;

    if (prevProps.code_send !== code_send && code_send) {
      navigate(AuthFlowKeys.ConfirmCode, {
        headerTitle: t('auth_flow:password_recovery'),
        text: t('auth_flow:reset_password_code_text'),
        requestAction: this.props.verifyConfirmationCode,
        callBack: () => {
          replace(AuthFlowKeys.NewPassword, {
            email: this.state.form.email.value,
          });
        },
      });
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    this.setState({
      form: {
        ...form,
        [name]: {
          ...form[name],
          value: val,
          isValid: validateEmail(val),
        },
      },
    });
  };

  checkFullValidate = async () => {
    const {
      form: {email},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            email: {
              ...email,
              isValid: email.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.email.isValid ? resolve(true) : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {email},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.getConfirmationCode(email.value, true);
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {email},
    } = this.state;

    return (
      <Container
        headerProps={{
          title: t('auth_flow:password_recovery'),
        }}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Input
              label={t('common:form.email')}
              value={email.value}
              required
              isValid={email.isValid}
              onBlur={() => this.validate(email.value, 'email')}
              onChangeText={val => this.validate(val, 'email')}
            />
          </View>
          <View style={styles.bottomContent}>
            <Button
              contentContainerStyle={styles.button}
              title={t('common:buttons.send')}
              type="red"
              isLoading={is_request}
              onPress={this.handleSubmit}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.authReducer.is_request,
  code_send: state.authReducer.code_send,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getConfirmationCode, verifyConfirmationCode}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ResetScreen),
);
