import {StyleSheet} from 'react-native';
import {COLOR_YELLOW, COLOR_WHITE} from '../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_MEDIUM,
} from '../../../../../constants/StylesConstants';

const {small} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
  },
  scrollContentStyles: {
    padding: scale(24),
    paddingTop: v_scale(10),
  },
  bottomContent: {
    padding: scale(24),
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomButton: {
    width: '50%',
  },
  leftSide: {
    width: '50%',
  },
  lampIcon: {
    width: scale(20),
    height: v_scale(20),
  },
  lampWrapper: {
    height: scale(40),
    width: scale(40),
    backgroundColor: COLOR_YELLOW,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(20),
    marginLeft: scale(5),
  },
  hintButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  lampText: {
    color: COLOR_WHITE,
    fontSize: small,
    fontFamily: FONT_MEDIUM,
    fontStyle: 'italic',
  },
});
