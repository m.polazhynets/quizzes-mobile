import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {SETTINGS} from '../constants/ActionTypes';

export default injectReducer(initialState.settingsReducer, {
  [SETTINGS.NETWORK_CONNECTION_CHANGE]: (state, {payload}) => ({
    ...state,
    network_connected: payload,
  }),

  [SETTINGS.SET_LANGUAGE]: (state, {payload: {lang_code}}) => ({
    ...state,
    lang_code,
  }),

  [SETTINGS.SET_BIOMETRIC_TYPE]: (state, {payload: {biometryType}}) => ({
    ...state,
    biometry_type: biometryType,
  }),

  [SETTINGS.TOGGLE_CUSTOM_TOAST]: (state, {payload: {param, label}}) => ({
    ...state,
    show_custom_toast: param && label ? {label} : param,
  }),

  [SETTINGS.TOGGLE_QUICK_LOG_IN]: (state, {payload: {param}}) => ({
    ...state,
    quick_log_in: param,
  }),

  [SETTINGS.TOGGLE_ENABLE_PUSH_NOTIFICATIONS]: (
    state,
    {payload: {enable_push}},
  ) => ({
    ...state,
    enable_push,
  }),
});
