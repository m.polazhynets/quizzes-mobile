import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {PROFILE, AUTH} from '../constants/ActionTypes';

export default injectReducer(initialState.profileReducer, {
  [AUTH.LOG_OUT_SUCCESS]: () => ({
    ...initialState.authReducer,
  }),

  [PROFILE.GET_PROFILE]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_PROFILE_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    profile: payload,
  }),
  [PROFILE.GET_PROFILE_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.UPDATE_PROFILE]: state => ({
    ...state,
    is_request: true,
    is_updated: null,
  }),
  [PROFILE.UPDATE_PROFILE_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    profile: payload,
    is_updated: true,
  }),
  [PROFILE.UPDATE_PROFILE_FAILURE]: state => ({
    ...state,
    is_request: false,
    is_updated: false,
  }),

  [PROFILE.CHANGE_EMAIL]: state => ({
    ...state,
    code_send: null,
    is_request: true,
  }),
  [PROFILE.CHANGE_EMAIL_SUCCESS]: state => ({
    ...state,
    code_send: true,
    is_request: false,
  }),
  [PROFILE.CHANGE_EMAIL_FAILURE]: state => ({
    ...state,
    code_send: false,
    is_request: false,
  }),

  [PROFILE.VERIFY_EMAIL]: state => ({
    ...state,
    code_confirmed: null,
    is_request: true,
  }),
  [PROFILE.VERIFY_EMAIL_SUCCESS]: state => ({
    ...state,
    code_confirmed: true,
    is_request: false,
  }),
  [PROFILE.VERIFY_EMAIL_FAILURE]: state => ({
    ...state,
    code_confirmed: false,
    is_request: false,
  }),

  [PROFILE.GET_STATISTICS]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_STATISTICS_SUCCESS]: (
    state,
    {payload, additionalPayload: {type}},
  ) => {
    const name = `${type}_statistics`;

    return {
      ...state,
      [name]: payload,
      is_request: false,
    };
  },
  [PROFILE.GET_STATISTICS_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.GET_STATISTICS]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_STATISTICS_SUCCESS]: (
    state,
    {payload, additionalPayload: {type}},
  ) => {
    const name = `${type}_statistics`;

    return {
      ...state,
      [name]: payload,
      is_request: false,
    };
  },
  [PROFILE.GET_STATISTICS_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.GET_RATING_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_RATING_LIST_SUCCESS]: (
    state,
    {
      payload: {points, position, nickname, photo, id, total_pages, results},
      additionalPayload: {page},
    },
  ) => {
    return {
      ...state,
      rating_my_profile: {
        points,
        position,
        nickname,
        photo,
        id,
      },
      rating_list:
        page === 1
          ? results
          : state.rating_list
          ? [...state.rating_list, ...results]
          : results,
      is_request: false,
      rating_filter_params: {
        ...state.rating_filter_params,
        page,
        total_pages,
        last_page: page === total_pages,
      },
    };
  },
  [PROFILE.GET_RATING_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.GET_USER_PROFILE]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_USER_PROFILE_SUCCESS]: (state, {payload}) => ({
    ...state,
    user_profile: payload,
    is_request: false,
  }),
  [PROFILE.GET_USER_PROFILE_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.ADD_QUIZ_TIPS_LIFES]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.ADD_QUIZ_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {count, target, method}},
  ) => {
    let profile = {...state.profile};

    // if (method === 'advertising') {
    //   if (target === 'tooltip') {
    //     profile.free_tooltips = profile.free_tooltips + count;
    //   } else {
    //     profile.free_life = profile.free_life + count;
    //   }
    // }
    if (target === 'tooltip') {
      profile.free_tooltips = profile.free_tooltips + count;
    } else {
      profile.free_life = profile.free_life + count;
    }

    return {
      ...state,
      profile,
      is_request: false,
    };
  },
  [PROFILE.ADD_QUIZ_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.REMOVE_QUIZ_TIPS_LIFES]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.REMOVE_QUIZ_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {target}},
  ) => {
    let profile = {...state.profile};

    if (target === 'tooltip') {
      profile.free_tooltips = profile.free_tooltips - 1;
    } else {
      profile.free_life = profile.free_life - 1;
    }

    return {
      ...state,
      profile,
      is_request: false,
    };
  },
  [PROFILE.REMOVE_QUIZ_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {target, price}},
  ) => {
    let profile = {...state.profile};

    if (target === 'tooltip') {
      profile.tournament_tooltips = profile.tournament_tooltips + 1;
    } else {
      profile.tournament_life = profile.tournament_life + 1;
    }
    profile.wallet = {
      ...profile.wallet,
      amount: profile.wallet.amount - price,
    };

    return {
      ...state,
      profile,
      is_request: false,
    };
  },
  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.GET_PAYMENT_HISTORY]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_PAYMENT_HISTORY_SUCCESS]: (
    state,
    {payload: {total_pages, results}, additionalPayload: {page}},
  ) => {
    return {
      ...state,
      payment_list:
        page === 1
          ? results
          : state.payment_list
          ? [...state.payment_list, ...results]
          : results,
      is_request: false,
      payment_filter_params: {
        ...state.payment_filter_params,
        page,
        total_pages,
        last_page: page === total_pages,
      },
    };
  },
  [PROFILE.GET_PAYMENT_HISTORY_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [PROFILE.GET_DISBURSEMENT]: state => ({
    ...state,
    is_request: true,
  }),
  [PROFILE.GET_DISBURSEMENT_SUCCESS]: state => ({
    ...state,
    is_request: false,
  }),
  [PROFILE.GET_DISBURSEMENT_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),
});
