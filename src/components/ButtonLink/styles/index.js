import {StyleSheet} from 'react-native';
import {COLOR_BLUE} from '../../../constants/Colors';
import variables, {
  scale,
  FONT_REGULAR,
} from '../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  linkText: {
    textAlign: 'center',
    color: COLOR_BLUE,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
  },
  link: {
    position: 'relative',
    width: '100%',
    borderRadius: scale(10),
  },
});
