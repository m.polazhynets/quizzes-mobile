import {StyleSheet} from 'react-native';
import {
  COLOR_WHITE,
  COLOR_DARK,
  COLOR_GREEN_DARKEN,
  COLOR_GREEN_2,
  COLOR_RED,
  COLOR_RED_LIGHTEN,
} from '../../../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_MEDIUM,
} from '../../../../../../../constants/StylesConstants';

const {mediumRegular} = variables.fontSize;

export const SUCCESS = [COLOR_GREEN_2, COLOR_GREEN_DARKEN];
export const FAILED = [COLOR_RED, COLOR_RED_LIGHTEN];
export const DEFAULT = [COLOR_WHITE, COLOR_WHITE];

export default StyleSheet.create({
  item: {
    borderRadius: 10,
    marginBottom: v_scale(24),
    position: 'relative',
  },
  itemContent: {
    height: v_scale(60),
    paddingHorizontal: scale(20),
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: 'transparent',
    borderRadius: 10,
  },
  greenBorder: {
    borderColor: COLOR_GREEN_DARKEN,
  },
  text: {
    fontFamily: FONT_MEDIUM,
    fontSize: mediumRegular,
    color: COLOR_DARK,
  },
  textWhite: {
    color: COLOR_WHITE,
  },
  closeIcon: {
    position: 'absolute',
    right: scale(17),
    width: scale(22),
    height: scale(22),
  },
  checkIcon: {
    position: 'absolute',
    right: scale(17),
    width: scale(26),
    height: scale(19),
  },
});
