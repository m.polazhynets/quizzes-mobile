import React, {Component} from 'react';
import {
  View,
  StatusBar,
  Text,
  ImageBackground,
  TouchableOpacity,
  Image,
  Vibration,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import RadialGradient from 'react-native-radial-gradient';
import {CommonActions} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  sendAnswer,
  getCurrentTooltip,
  cleareAnswer,
  getTournamentQuestion,
} from '../../../../actions/tournamentsActions';
import {toogleDialogModal} from '../../../../actions/generalActions';
import {addTournamentTipsLifes} from '../../../../actions/profileActions';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {Header, QuestionComponent, DialogModal} from './components';
import {Button, Preloader} from '../../../../components';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import {BACKGROUND_1_IMAGE, LAMP_ICON} from '../../../../constants/Images';
import BackgroundTimer from '../../../../utils/Timer';
import styles from './styles';

const VIOLET = ['#DD01AD', '#2200AB'];

class QuestionTournamentScreen extends Component {
  state = {
    type_id: this.props.route.params.id,
    question: null,
    timer: null,
    pause: false,
  };
  timer = null;

  componentDidUpdate(prevProps) {
    const {question, truest_answer} = this.props;

    if (prevProps.question !== question) {
      if (prevProps.question === null && question) {
        this.setState({question});
      } else if (question) {
        if (truest_answer === false) {
          Vibration.vibrate();
        }
        setTimeout(() => {
          this.setState({question, timer: null});
        }, 1000);
      }
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      BackgroundTimer.clearInterval(this.timer);
    }
  }

  useTooltip = () => {
    const {type_id} = this.state;

    this.tooglePause(true);
    this.props.getCurrentTooltip(type_id, res => {
      if (res) {
        this.closeDialogModal();
        this.setState({showTooltip: true}, () => {
          setTimeout(() => {
            this.setState({showTooltip: false});
          }, 1000);
        });
      } else {
        this.tooglePause(false);
      }
    });
  };

  closeDialogModal = () => {
    this.tooglePause(false);
    this.props.toogleDialogModal(null);
  };

  onAnswered = order_num => {
    const {question} = this.props;
    const {type_id, timer} = this.state;
    let payload = {};

    if (order_num) {
      payload.order_num = order_num;
      payload.answer_time = question.time - timer;
    }

    BackgroundTimer.clearInterval(this.timer);
    this.timer = null;
    this.props.sendAnswer(type_id, payload, res => {
      if (res === 'next') {
        console.log('next question');
      } else if (res === 'finish') {
        if (this.props.truest_answer === false) {
          Vibration.vibrate();
        }
        let routes = [
          {
            name: MainFlowKeys.Home,
            params: {disableStartRequest: true},
          },
          {
            name: MainFlowKeys.Congratulation,
            params: {isTournament: true},
          },
        ];
        setTimeout(() => {
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes,
            }),
          );
        }, 1000);
      } else {
        Vibration.vibrate();
        console.log('some error');
      }
    });
  };

  goToNext = () => {
    const {t} = this.props;

    Alert.alert(
      t('quiz_flow:attention'),
      t('quiz_flow:skip_description'),
      [
        {
          text: t('common:texts.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: t('common:buttons.yes'),
          onPress: () => this.onAnswered(),
        },
      ],
      {cancelable: false},
    );
  };

  onNextPress = () => {
    const {type_id} = this.state;

    this.closeDialogModal();
    this.props.getTournamentQuestion(type_id);
  };

  startTimer = async duration => {
    if (this.state.timer) {
      return null;
    }

    this.setState(state => {
      return {timer: state.timer ? state.timer - 1 : duration};
    });
    this.timer = BackgroundTimer.setInterval(() => {
      if (this.state.timer === 0) {
        return this.onAnswered();
      }
      if (this.state.pause) {
        return;
      }
      this.setState(state => {
        return {timer: state.timer ? state.timer - 1 : duration};
      });
    }, 1000);
  };

  tooglePause = val => {
    this.setState({pause: val});
  };

  showPay = (type, price, callback = () => {}) => {
    Alert.alert(
      'Оплата',
      `Ви уверени что хотите купить ${
        type === 'tooltip' ? '1 подсказку' : '1 жизнь'
      } за ${price} Р`,
      [
        {
          text: 'Нет',
          style: 'cancel',
          onPress: () => callback(false),
        },
        {
          text: 'Да',
          onPress: () =>
            this.props.addTournamentTipsLifes(type, price, callback),
        },
      ],
      {cancelable: false},
    );
  };

  // *** DialogModal functions
  onExitPress = () => {
    BackgroundTimer.clearInterval(this.timer);
    this.timer = null;
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: MainFlowKeys.Home,
            params: {disableStartRequest: true},
          },
        ],
      }),
    );
    this.closeDialogModal();
  };

  onBuyPress = () => {
    const {type_id} = this.state;
    const {dialog_type_key, tournament_data} = this.props;

    if (dialog_type_key === 'tooltip') {
      this.tooglePause(true);
      this.showPay('tooltip', tournament_data.tooltip_cost, () => {
        this.tooglePause(false);
      });
    } else {
      this.showPay('life', tournament_data.tooltip_cost, res => {
        if (res) {
          this.closeDialogModal();
          this.props.getTournamentQuestion(type_id);
        }
      });
    }
  };
  // ***

  render() {
    const {
      t,
      is_request,
      show_dialog_modal,
      dialog_type_key,
      life,
      tooltips,
    } = this.props;
    const {showTooltip, timer, question, type_id} = this.state;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={VIOLET}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.wrapper}>
          <StatusBar
            barStyle="dark-content"
            translucent
            backgroundColor="transparent"
          />
          <SafeAreaView style={styles.content}>
            <Header
              timer={timer}
              defaultTime={question ? question.time : null}
            />
            {is_request && <Preloader />}
            <KeyboardAwareScrollView
              contentContainerStyle={styles.scrollContentStyles}>
              {question && (
                <QuestionComponent
                  data={question}
                  tournament_id={type_id}
                  showTooltip={showTooltip}
                  onAnswered={this.onAnswered}
                  startTimer={this.startTimer}
                  onUseTooltips={this.useTooltip}
                  onExitPress={this.onExitPress}
                  togglePause={this.tooglePause}
                />
              )}
            </KeyboardAwareScrollView>
            <View style={styles.bottomContent}>
              <View style={styles.leftSide}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.hintButton}
                  onPress={() =>
                    this.props.toogleDialogModal('main', 'tooltip')
                  }>
                  <Text style={styles.lampText}>
                    {t('common:buttons.hint')}
                  </Text>
                  <View style={styles.lampWrapper}>
                    <Image
                      source={LAMP_ICON}
                      resizeMode="contain"
                      style={styles.lampIcon}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <Button
                contentContainerStyle={styles.bottomButton}
                title={t('common:buttons.next')}
                onPress={this.goToNext}
                type="red"
                isArrow
              />
            </View>
            <DialogModal
              isVisible={show_dialog_modal === 'main'}
              type_key="tournament"
              title_type_key={dialog_type_key}
              onCancel={dialog_type_key === 'tooltip' && this.closeDialogModal}
              onBuyPress={dialog_type_key === 'tooltip' && this.onBuyPress}
              onUseTooltips={dialog_type_key === 'tooltip' && this.useTooltip}
              onExitPress={dialog_type_key === 'tooltip' && this.onExitPress}
              onNextPress={dialog_type_key === 'time_out' && this.onNextPress}
              life={life}
              tooltips={tooltips}
            />
          </SafeAreaView>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.tournamentsReducer.is_request,
  truest_answer: state.tournamentsReducer.answer,
  tournament_data: state.tournamentsReducer.tournament_data,
  question: state.tournamentsReducer.question,
  life: state.tournamentsReducer.life,
  tooltips: state.tournamentsReducer.tooltips,
  show_dialog_modal: state.generalReducer.show_dialog_modal,
  dialog_type_key: state.generalReducer.dialog_type_key,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      sendAnswer,
      addTournamentTipsLifes,
      toogleDialogModal,
      getCurrentTooltip,
      cleareAnswer,
      getTournamentQuestion,
    },
    dispatch,
  );

export default withTranslation(['common', 'main_flow', 'quiz_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(QuestionTournamentScreen),
);
