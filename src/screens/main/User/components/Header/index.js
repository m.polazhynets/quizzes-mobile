import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image, Animated, View, Text} from 'react-native';
import i18n from 'i18next';
import LinearGradient from 'react-native-linear-gradient';
import {goBack} from '../../../../../navigator/RootNavigation';
import {
  ARROW_BACK_WHITE_ICON,
  CUP_BACKGROUND_IMAGE,
  PROFILE_ICON,
} from '../../../../../constants/Images';
import {COLOR_GRAY_5} from '../../../../../constants/Colors';
import styles, {
  HEADER_MIN_HEIGHT,
  HEADER_MAX_HEIGHT,
  IMAGE_WIDTH_START,
  IMAGE_WIDTH_FINISH,
  IMAGE_HEIGHT_START,
  IMAGE_HEIGHT_FINISH,
  IMAGE_LEFT_START,
  IMAGE_LEFT_FINISH,
  IMAGE_BOTTOM_START,
  IMAGE_BOTTOM_FINISH,
  CONTENT_LEFT_START,
  CONTENT_LEFT_FINISH,
  CONTENT_BOTTOM_START,
  CONTENT_BOTTOM_FINISH,
  CONTENT_WIDTH_START,
  CONTENT_WIDTH_FINISH,
  NICNAME_LEFT_START,
  NICNAME_LEFT_FINISH,
} from './styles';

const toolbarHeight = HEADER_MIN_HEIGHT;
const GRAY = [COLOR_GRAY_5, COLOR_GRAY_5];

export default class Header extends Component {
  state = {
    scrollOffset: new Animated.Value(0),
  };
  headerHeight = HEADER_MAX_HEIGHT;

  onScroll = e => {
    this.state.scrollOffset.setValue(e.nativeEvent.contentOffset.y);
  };

  _getHeight = () => {
    const {scrollOffset} = this.state;
    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [this.headerHeight, toolbarHeight],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getImageWidth = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_WIDTH_START, IMAGE_WIDTH_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageHeight = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_HEIGHT_START, IMAGE_HEIGHT_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_LEFT_START, IMAGE_LEFT_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageBottom = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_BOTTOM_START, IMAGE_BOTTOM_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getContentLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [CONTENT_LEFT_START, CONTENT_LEFT_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getContentBottom = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [CONTENT_BOTTOM_START, CONTENT_BOTTOM_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getContentWidth = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [CONTENT_WIDTH_START, CONTENT_WIDTH_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getAvatarOpacity = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
  };

  _getNicnameLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [NICNAME_LEFT_START, NICNAME_LEFT_FINISH],
      extrapolate: 'clamp',
    });
  };

  render() {
    const {headerStyle = {}, rightBtn, data} = this.props;
    const height = this._getHeight();
    const imageWidth = this._getImageWidth();
    const imageHeight = this._getImageHeight();
    const imageLeft = this._getImageLeft();
    const imageBottom = this._getImageBottom();
    const contentLeft = this._getContentLeft();
    const contentBottom = this._getContentBottom();
    const contentWidth = this._getContentWidth();
    const avatarOpacity = this._getAvatarOpacity();
    const nicnameLeft = this._getNicnameLeft();

    const imageStyle = {
      width: imageWidth,
      height: imageHeight,
      left: imageLeft,
      bottom: imageBottom,
    };

    return (
      <Animated.View
        style={[styles.headerWrapper, headerStyle, {height: height}]}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 1, y: 0}}
          colors={GRAY}
          style={styles.headerContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.backButton}
            onPress={() => goBack()}>
            <Image
              source={ARROW_BACK_WHITE_ICON}
              style={styles.arrowIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
          {data && (
            <Animated.View
              style={[
                styles.headerContent,
                {
                  left: contentLeft,
                  paddingBottom: contentBottom,
                  width: contentWidth,
                },
              ]}>
              <View style={styles.headerProfile}>
                <Animated.View
                  style={[styles.imageWrapper, {opacity: avatarOpacity}]}>
                  <Image
                    source={data.photo ? {uri: data.photo} : PROFILE_ICON}
                    resizeMode="cover"
                    style={styles.avatar}
                  />
                </Animated.View>
                <Animated.View style={[styles.userData, {left: nicnameLeft}]}>
                  <Animated.Text style={styles.nicname}>
                    {data.nickname}
                  </Animated.Text>
                  <Animated.Text style={styles.name}>
                    {`${data.first_name || ''} ${data.middle_name ||
                      ''} ${data.last_name || ''}`}
                  </Animated.Text>
                </Animated.View>
              </View>
            </Animated.View>
          )}
          {rightBtn}
          <Animated.Image
            source={CUP_BACKGROUND_IMAGE}
            resizeMode="contain"
            style={[styles.backgroundImage, imageStyle]}
          />
        </LinearGradient>
      </Animated.View>
    );
  }
}

Header.propTypes = {
  rightBtn: PropTypes.node,
  headerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};
