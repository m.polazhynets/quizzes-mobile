import {createActions} from 'redux-actions';

import {SETTINGS} from '../constants/ActionTypes';

export const {
  setLanguage,
  setBiometricType,
  toggleCustomToast,
  toggleQuickLogIn,
  toggleEnablePushNotifications,
} = createActions({
  [SETTINGS.SET_LANGUAGE]: lang_code => ({lang_code}),
  [SETTINGS.SET_BIOMETRIC_TYPE]: biometryType => ({biometryType}),
  [SETTINGS.TOGGLE_CUSTOM_TOAST]: (param, label) => ({param, label}),
  [SETTINGS.TOGGLE_QUICK_LOG_IN]: param => ({param}),
  [SETTINGS.TOGGLE_ENABLE_PUSH_NOTIFICATIONS]: enable_push => ({enable_push}),
});
