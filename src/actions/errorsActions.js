import {createActions} from 'redux-actions';

import {ERRORS} from '../constants/ActionTypes';

export const {deleteErrors} = createActions({
  [ERRORS.DELETE_ERRORS]: () => ({}),
});
