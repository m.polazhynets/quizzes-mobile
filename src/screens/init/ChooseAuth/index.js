import React, {Component} from 'react';
import {View, Image, ImageBackground, Platform} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {LoginButton, AccessToken, LoginManager} from 'react-native-fbsdk';
import RadialGradient from 'react-native-radial-gradient';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import * as Keychain from 'react-native-keychain';
import {socialLogIn} from '../../../actions/authActions';
import {navigate, replace} from '../../../navigator/RootNavigation';
import {AuthFlowKeys, AppFlowKeys, MainFlowKeys} from '../../../navigator/Keys';
import {Button, Container, ButtonLink, Preloader} from '../../../components';
import {CONCEPT_IMAGE} from '../../../constants/Images';
import {COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN} from '../../../constants/Colors';
import {BACKGROUND_3_IMAGE} from '../../../constants/Images';
import {deviceWidth, deviceHeight} from '../../../constants/StylesConstants';
import styles from './styles';

const BACKGROUND = [COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN];
const options =
  Platform.OS === 'ios'
    ? {}
    : {
        accessControl: Keychain.ACCESS_CONTROL.APPLICATION_PASSWORD,
        rules: Keychain.SECURITY_RULES.NONE,
      };

GoogleSignin.configure({
  scopes: ['email', 'profile'],
  webClientId:
    '87170689926-v104dkar4lgraggcv2mk94622jr76tus.apps.googleusercontent.com',
  offlineAccess: true,
  iosClientId:
    '87170689926-b26shponlbq6vj2g7hrn5cvfejsvlms5.apps.googleusercontent.com',
});

class ChooseAuthScreen extends Component {
  state = {
    user_id: null,
    headerEnabled: this.props.route.params
      ? this.props.route.params.headerEnabled
      : false,
  };

  componentDidMount = () => {
    LoginManager.logOut();
  };

  componentDidUpdate(prevProps) {
    const {access_token} = this.props;
    const {user_id} = this.state;

    if (prevProps.access_token !== access_token && access_token) {
      Keychain.getGenericPassword(options)
        .then(res => {
          if (res) {
            if (res.username === user_id) {
              replace(AppFlowKeys.Main, {
                screen: MainFlowKeys.Home,
              });
            } else {
              replace(AppFlowKeys.Auth, {
                screen: AuthFlowKeys.PinCode,
                params: {email: user_id, createPin: true},
              });
            }
          } else {
            replace(AppFlowKeys.Auth, {
              screen: AuthFlowKeys.PinCode,
              params: {email: user_id, createPin: true},
            });
          }
        })
        .catch(e => {
          replace(AppFlowKeys.Auth, {
            screen: AuthFlowKeys.PinCode,
            params: {email: user_id, createPin: true},
          });
        });
    }
  }

  googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this.setState({user_id: userInfo.user.email});
      this.props.socialLogIn(userInfo.idToken, 'google');
    } catch (error) {
      console.log('error', error);
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {headerEnabled} = this.state;

    return (
      <RadialGradient
        style={styles.flex}
        colors={BACKGROUND}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_3_IMAGE} style={styles.flex}>
          {is_request && <Preloader />}
          <Container
            headerProps={{
              hideContent: headerEnabled,
              backgroundStart: 'transparent',
              backgroundFinish: 'transparent',
            }}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}>
            <View style={styles.content}>
              <View style={styles.imageWrapper}>
                <Image
                  source={CONCEPT_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.bottomContent}>
                <LoginButton
                  onLoginFinished={(error, result) => {
                    if (error) {
                      console.log('login has error: ' + result.error, result);
                    } else if (result.isCancelled) {
                      console.log('login is cancelled.');
                    } else {
                      AccessToken.getCurrentAccessToken().then(data => {
                        console.log(data);
                        this.setState({user_id: data.userID});
                        this.props.socialLogIn(data.accessToken, 'facebook');
                      });
                    }
                  }}
                  style={styles.facebookButton}
                />
                <GoogleSigninButton
                  style={styles.googleButton}
                  size={GoogleSigninButton.Size.Wide}
                  onPress={this.googleSignIn}
                  disabled={is_request}
                />
                <Button
                  contentContainerStyle={styles.button}
                  title={t('common:buttons.registration')}
                  type="red"
                  onPress={() => navigate(AuthFlowKeys.Registration)}
                />
                <ButtonLink
                  title={t('common:buttons.already_have_account')}
                  contentContainerStyle={styles.link}
                  onPress={() => navigate(AuthFlowKeys.Login)}
                />
              </View>
            </View>
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  access_token: state.authReducer.access_token,
  is_request: state.authReducer.is_request,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({socialLogIn}, dispatch);

export default withTranslation(['common'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChooseAuthScreen),
);
