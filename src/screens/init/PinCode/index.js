import React, {Component} from 'react';
import {View, Image, Text, Platform, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import RadialGradient from 'react-native-radial-gradient';
import * as Keychain from 'react-native-keychain';
import Toast from 'react-native-simple-toast';
import ReactNativeBiometrics from 'react-native-biometrics';
import {CommonActions} from '@react-navigation/native';
import {logOut} from '../../../actions/authActions';
import {Container, PinView, ButtonLink} from '../../../components';
import {replace} from '../../../navigator/RootNavigation';
import {AppFlowKeys, MainFlowKeys, AuthFlowKeys} from '../../../navigator/Keys';
import {COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN} from '../../../constants/Colors';
import {
  CONCEPT_IMAGE,
  CLOSE_SMALL_BLACK_ICON,
  FACE_ID_ICON,
  TOUCH_ID_ICON,
  BACKGROUND_3_IMAGE,
} from '../../../constants/Images';
import {
  scale,
  deviceWidth,
  deviceHeight,
} from '../../../constants/StylesConstants';
import styles from './styles';

const BACKGROUND = [COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN];

const options =
  Platform.OS === 'ios'
    ? {}
    : {
        accessControl: Keychain.ACCESS_CONTROL.APPLICATION_PASSWORD,
        rules: Keychain.SECURITY_RULES.NONE,
      };

class PinCodeScreen extends Component {
  constructor(props) {
    super(props);
    const {params} = this.props.route;
    this.state = {
      createPin: params ? params.createPin : false,
      changePin: params ? params.changePin : false,
      email: params ? params.email : null,
      enteredPin: '',
      showBiometricAuth: params
        ? !params.createPin && !params.changePin
        : false,
    };
    this.pinView = React.createRef();
  }

  componentDidMount() {
    this.getStorageData(this.state.email);
  }

  setEnteredPin = val => {
    const {createPin, email, storageData} = this.state;
    const {biometry_type, t} = this.props;

    this.setState({enteredPin: val}, () => {
      if (val.length === 4) {
        if (createPin) {
          const _payload = {
            pin: val,
            enableBiometry: biometry_type ? biometry_type.available : false,
          };
          Keychain.setGenericPassword(email, JSON.stringify(_payload), options)
            .then(() => {
              replace(AppFlowKeys.Main, {screen: MainFlowKeys.Home});
            })
            .catch(e => {
              console.log(e);
            });
        } else {
          if (storageData.pin === val) {
            replace(AppFlowKeys.Main, {screen: MainFlowKeys.Home});
          } else {
            Toast.show(t('common:errors.incorrect_pincode'), Toast.SHORT);
            this.pinView.current.clearAll();
          }
        }
      }
    });
  };

  forgetPinCode = () => {
    const {navigation} = this.props;

    this.props.logOut();
    Keychain.resetGenericPassword(options);
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          {name: AuthFlowKeys.ChooseAuth, params: {headerEnabled: true}},
          {name: AuthFlowKeys.Login},
        ],
      }),
    );
  };

  getStorageData = email => {
    const {showBiometricAuth} = this.state;
    if (!email) {
      this.setState({storageData: null});
    }
    Keychain.getGenericPassword(options)
      .then(res => {
        if (res) {
          const payload = JSON.parse(res.password);
          if (
            payload.enableBiometry &&
            showBiometricAuth &&
            res.username === email
          ) {
            this.enableBiometry();
          }
          this.setState({storageData: res.username === email ? payload : null});
        } else {
          this.setState({createPin: true});
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({storageData: null});
      });
  };

  enableBiometry = () => {
    const {biometry_type, t} = this.props;
    ReactNativeBiometrics.simplePrompt({
      promptMessage:
        biometry_type.biometryType === ReactNativeBiometrics.FaceID
          ? t('common:texts.confirm_face')
          : t('common:texts.confirm_touch'),
    })
      .then(resultObject => {
        const {success} = resultObject;

        if (success) {
          replace(AppFlowKeys.Main, {screen: MainFlowKeys.Home});
        }
      })
      .catch(() => {
        console.log('fingerprint failed or prompt was cancelled');
      });
  };

  render() {
    const {t, biometry_type} = this.props;
    const {showBiometricAuth, enteredPin, createPin, storageData} = this.state;

    return (
      <RadialGradient
        style={styles.flex}
        colors={BACKGROUND}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_3_IMAGE} style={styles.flex}>
          <Container
            headerEnabled={false}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}>
            <View style={styles.content}>
              <View style={styles.imageWrapper}>
                <Image
                  source={CONCEPT_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.bottomContent}>
                <View style={styles.titleBlock}>
                  <Text style={styles.text}>
                    {createPin
                      ? t('auth_flow:create_pin')
                      : t('auth_flow:pin_access')}
                  </Text>
                </View>
                <PinView
                  ref={this.pinView}
                  pinLength={4}
                  buttonSize={scale(75)}
                  onValueChange={this.setEnteredPin}
                  buttonTextStyle={styles.buttonTextStyle}
                  inputViewEmptyStyle={styles.inputViewEmptyStyle}
                  inputViewFilledStyle={styles.inputViewFilledStyle}
                  inputAreaStyle={styles.inputAreaStyle}
                  onButtonPress={key => {
                    if (key === 'custom_right') {
                      this.enableBiometry();
                    }
                    if (key === 'custom_left') {
                      this.pinView.current.clear();
                    }
                  }}
                  customLeftButton={
                    enteredPin.length ? (
                      <Image
                        source={CLOSE_SMALL_BLACK_ICON}
                        style={styles.deleteIcon}
                      />
                    ) : (
                      undefined
                    )
                  }
                  customRightButton={
                    showBiometricAuth &&
                    storageData &&
                    storageData.enableBiometry &&
                    biometry_type &&
                    biometry_type.available ? (
                      <Image
                        source={
                          biometry_type.biometryType ===
                          ReactNativeBiometrics.FaceID
                            ? FACE_ID_ICON
                            : TOUCH_ID_ICON
                        }
                        style={styles.biometricIcon}
                      />
                    ) : (
                      undefined
                    )
                  }
                />
                <ButtonLink
                  title={t('common:buttons.forget_pincode')}
                  onPress={this.forgetPinCode}
                />
              </View>
            </View>
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  biometry_type: state.settingsReducer.biometry_type,
});

const mapDispatchToProps = dispatch => bindActionCreators({logOut}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PinCodeScreen),
);
