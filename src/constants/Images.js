/** IMAGES */
export const CONCEPT_IMAGE = require('../../assets/images/concept.png');
export const CONCEPT_LAMP_IMAGE = require('../../assets/images/concept_lamp.png');
export const BACKGROUND_IMAGE = require('../../assets/images/background.png');
export const BACKGROUND_1_IMAGE = require('../../assets/images/background_1.png');
export const BACKGROUND_2_IMAGE = require('../../assets/images/background_2.png');
export const BACKGROUND_3_IMAGE = require('../../assets/images/background_3.png');
export const BRAIN_IMAGE = require('../../assets/images/brain.png');
export const DOCUMENT_IMAGE = require('../../assets/images/document.png');
export const LAMP_BACKGROUND_IMAGE = require('../../assets/images/lamp_background.png');
export const HEART_BACKGROUND_IMAGE = require('../../assets/images/heart_background.png');
export const CUP_BACKGROUND_IMAGE = require('../../assets/images/cup_background.png');
export const CHAT_BACKGROUND_IMAGE = require('../../assets/images/chat_background.png');
export const WAVE_IMAGE = require('../../assets/images/wave.png');
export const PART_LOGO_IMAGE = require('../../assets/images/part_logo.png');
export const CUP_VIOLET_IMAGE = require('../../assets/images/cup_violet.png');
export const CUP_BLUE_IMAGE = require('../../assets/images/cup_blue.png');

/** ICONS */
export const APP_LOGO_ICON = require('../../assets/images/icons/app_logo.png');
export const ARROW_GRAY_ICON = require('../../assets/images/icons/arrow_gray.png');
export const ARROW_BACK_ICON = require('../../assets/images/icons/arrow_back.png');
export const ARROW_WHITE_ICON = require('../../assets/images/icons/arrow_white.png');
export const ARROW_DARK_RIGHT_ICON = require('../../assets/images/icons/arrow_dark_right.png');
export const ARROW_WHITE_RIGHT_ICON = require('../../assets/images/icons/arrow_white_right.png');
export const ARROW_BACK_WHITE_ICON = require('../../assets/images/icons/arrow_back_white.png');
export const FB_ICON = require('../../assets/images/icons/fb.png');
export const GOOGLE_ICON = require('../../assets/images/icons/google.png');
export const FACE_ID_ICON = require('../../assets/images/icons/face_id.png');
export const TOUCH_ID_ICON = require('../../assets/images/icons/touch_id.png');
export const CLOSE_SMALL_BLACK_ICON = require('../../assets/images/icons/close_small_black.png');
export const LAMP_ICON = require('../../assets/images/icons/lamp.png');
export const LAMP_YELLOW_ICON = require('../../assets/images/icons/lamp_yellow.png');
export const DRAWER_ICON = require('../../assets/images/icons/drawer.png');
export const STAR_ICON = require('../../assets/images/icons/star.png');
export const STAR_VIOLET_ICON = require('../../assets/images/icons/star_violet.png');
export const QUESTION_ICON = require('../../assets/images/icons/question.png');
export const TIMER_ICON = require('../../assets/images/icons/timer.png');
export const CHECK_ICON = require('../../assets/images/icons/check.png');
export const CHECK_BLACK_ICON = require('../../assets/images/icons/check_black.png');
export const CHECK_GREEN_ICON = require('../../assets/images/icons/check_green.png');
export const CLOSE_WHITE_ICON = require('../../assets/images/icons/close_white.png');
export const PLUS_ICON = require('../../assets/images/icons/plus.png');
export const PLUS_SLIM_ICON = require('../../assets/images/icons/plus_slim.png');
export const PROFILE_ICON = require('../../assets/images/icons/profile.png');
export const EDIT_ICON = require('../../assets/images/icons/edit.png');
export const BIRTHDAY_ICON = require('../../assets/images/icons/birthday.png');
export const EMAIL_ICON = require('../../assets/images/icons/email.png');
export const LOCATION_ICON = require('../../assets/images/icons/location.png');
export const HEART_ICON = require('../../assets/images/icons/heart.png');
export const CALENDAR_ICON = require('../../assets/images/icons/calendar.png');
export const MORE_ICON = require('../../assets/images/icons/more.png');
export const MESSAGE_ICON = require('../../assets/images/icons/message.png');
export const SEARCH_ICON = require('../../assets/images/icons/search.png');
export const REMOVE_ICON = require('../../assets/images/icons/remove.png');
export const SEND_ICON = require('../../assets/images/icons/send.png');
export const SEND_ACTIVE_ICON = require('../../assets/images/icons/send_active.png');
export const PRIZE_ICON = require('../../assets/images/icons/prize.png');
export const PEOPLE_ICON = require('../../assets/images/icons/people.png');
export const PLACE_ICON = require('../../assets/images/icons/place.png');
export const PURCHASE_ICON = require('../../assets/images/icons/purchase.png');
export const PLAY_ICON = require('../../assets/images/icons/play.png');
export const PAUSE_ICON = require('../../assets/images/icons/pause.png');
export const REPEAT_ICON = require('../../assets/images/icons/repeat.png');
export const CASTLE_ICON = require('../../assets/images/icons/castle.png');

export const MENU_BRAIN_ICON = require('../../assets/images/icons/menu/brain.png');
export const MENU_PRIZE_ICON = require('../../assets/images/icons/menu/prize.png');
export const MENU_POLICY_ICON = require('../../assets/images/icons/menu/policy.png');
export const MENU_QUESTION_ICON = require('../../assets/images/icons/menu/question.png');
export const MENU_FEEDBACK_ICON = require('../../assets/images/icons/menu/feedback.png');
export const MENU_CHATS_ICON = require('../../assets/images/icons/menu/chats.png');
export const MENU_TOOLS_ICON = require('../../assets/images/icons/menu/tools.png');
export const MENU_RATES_ICON = require('../../assets/images/icons/menu/rates.png');
export const MENU_NOTIFICATION_ICON = require('../../assets/images/icons/menu/notification.png');
export const MENU_LOG_OUT_ICON = require('../../assets/images/icons/menu/logOut.png');
