import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../../../constants/StylesConstants';
import {
  COLOR_DARK,
  COLOR_WHITE,
  COLOR_GRAY_5,
  COLOR_GRAY_0,
  COLOR_YELLOW,
  COLOR_GREEN_1,
} from '../../../../../../constants/Colors';

const {large, regular, small} = variables.fontSize;

export default StyleSheet.create({
  personalAccountBlock: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: scale(20),
    paddingVertical: v_scale(15),
    margin: scale(24),
    marginVertical: v_scale(15),
    height: v_scale(90),
    borderRadius: scale(10),
    backgroundColor: COLOR_WHITE,
    ...variables.shadow,
  },
  accountBackground: {
    position: 'absolute',
    width: scale(120),
    height: v_scale(70),
    bottom: isIphoneX() ? -v_scale(7) : -v_scale(2),
    right: scale(60),
  },
  partAccount: {
    flex: 1,
    justifyContent: 'space-between',
  },
  noMargin: {
    marginBottom: 0,
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_DARK,
    lineHeight: v_scale(30),
    marginBottom: v_scale(5),
  },
  alignEnd: {
    alignItems: 'flex-end',
  },
  smallTitle: {
    fontFamily: FONT_BOLD,
    fontSize: regular,
    color: COLOR_DARK,
  },
  transactionBlock: {
    paddingHorizontal: scale(24),
    marginBottom: v_scale(5),
  },
  alignStart: {
    alignItems: 'flex-start',
    lineHeight: v_scale(22),
  },
  gridBackground: {
    position: 'absolute',
    height: v_scale(45),
    bottom: 0,
    left: -scale(10),
  },
  gridBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(12),
  },
  grid: {
    position: 'relative',
    padding: scale(20),
    paddingVertical: v_scale(15),
    margin: scale(12),
    marginVertical: v_scale(15),
    borderRadius: scale(10),
    backgroundColor: COLOR_WHITE,
    ...variables.shadow,
  },
  gridText: {
    color: COLOR_DARK,
    fontSize: small,
    fontFamily: FONT_REGULAR,
    marginBottom: v_scale(5),
  },
  gridIcon: {
    position: 'absolute',
    width: scale(20),
    height: scale(20),
    right: scale(16),
    top: v_scale(20),
  },
  part: {
    flex: 1,
  },
  paddingLeft: {
    paddingLeft: scale(12),
  },
  list: {
    paddingVertical: v_scale(15),
    paddingHorizontal: scale(24),
    marginBottom: v_scale(0),
  },
  topBlock: {
    paddingHorizontal: scale(24),
  },
  line: {
    backgroundColor: COLOR_GRAY_0,
    width: '100%',
    height: scale(1),
    marginBottom: v_scale(24),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_GRAY_5,
    lineHeight: v_scale(22),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(5),
  },
  colorYellow: {
    color: COLOR_YELLOW,
  },
  colorGreen: {
    color: COLOR_GREEN_1,
  },
  listItem: {
    paddingVertical: v_scale(15),
  },
});
