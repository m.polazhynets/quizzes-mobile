import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getChatsList, deleteChat} from '../../../actions/chatActions';
import {Header, Preloader, Container} from '../../../components';
import Item from './components/Item';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {CHAT_BACKGROUND_IMAGE, SEARCH_ICON} from '../../../constants/Images';
import {
  COLOR_VIOLET_1,
  COLOR_VIOLET_1_DARKEN,
  COLOR_ROSE_DARKEN,
} from '../../../constants/Colors';
import styles from './styles';

const VIOLET = [COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN];

class ChatsScreen extends Component {
  header = React.createRef();
  state = {
    refreshing: false,
    firstLoading: false,
  };

  componentDidMount() {
    this.getData(false, 1);
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      this.setState({refreshing: false, firstLoading: false});
    }
  }

  getData = (refreshing, page) => {
    const {filter_params, is_request} = this.props;

    if (is_request) {
      return;
    }

    this.setState({refreshing, firstLoading: !refreshing}, () => {
      this.props.getChatsList(page || filter_params.page + 1);
    });
  };

  onRemoveChat = data => {
    this.props.deleteChat(data.id);
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {}}
        style={styles.headerButton}>
        <Image
          source={SEARCH_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  renderFooter = () => {
    if (
      !this.props.is_request ||
      !this.props.chats_list ||
      !this.props.chats_list.length
    ) {
      return null;
    }

    if (this.state.firstLoading || this.state.refreshing) {
      return null;
    }

    return (
      <View>
        <ActivityIndicator animating size="large" color={COLOR_ROSE_DARKEN} />
      </View>
    );
  };

  renderEmptyComponent = () => {
    const {t} = this.props;

    return <Text style={styles.emptyText}>{t('common:texts.list_empty')}</Text>;
  };

  render() {
    const {t, chats_list, filter_params} = this.props;
    const {refreshing, firstLoading} = this.state;

    return (
      <View style={styles.content}>
        {firstLoading && <Preloader />}
        <Container
          headerProps={{
            headerStyle: styles.zIndex,
            title: t('main_flow:chats'),
            whiteBackBtn: true,
            backgroundType: VIOLET,
            backgroundImage: CHAT_BACKGROUND_IMAGE,
          }}
          childrenType="flatlist"
          scrollStyles={styles.transparentStyle}
          contentContainerStyle={styles.transparentStyle}
          childrenComponentProps={{
            keyExtractor: (item, index) => `${item.id}_${index}`,
            data: chats_list,
            renderItem: ({item}) => (
              <Item
                key={item.id}
                {...item}
                onRemove={() => this.onRemoveChat(item)}
                onPress={() =>
                  navigate(MainFlowKeys.Chat, {user: item.user, id: item.id})
                }
              />
            ),
            ListEmptyComponent: this.renderEmptyComponent,
            ListFooterComponent: this.renderFooter,
            refreshing: refreshing,
            onRefresh: () => this.getData(true, 1),
            onEndReached: () => {
              if (!filter_params.last_page) {
                this.getData(false);
              }
            },
            style: styles.scrollContentStyles,
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.chatReducer.is_request,
  chats_list: state.chatReducer.chats_list,
  filter_params: state.chatReducer.chats_filter_params,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getChatsList, deleteChat}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChatsScreen),
);
