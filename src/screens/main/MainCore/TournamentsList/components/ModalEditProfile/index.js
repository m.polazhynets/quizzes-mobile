import React from 'react';
import Modal from 'react-native-modal';
import {View, Text, ImageBackground} from 'react-native';
import i18n from 'i18next';
import {Button} from '../../../../../../components';
import {BACKGROUND_2_IMAGE} from '../../../../../../constants/Images';
import styles from './styles';

function RenderFields(data) {
  let res = '';

  if (!data.nickname) {
    res = i18n.t('common:form.short_nicname');
  }
  if (!data.first_name) {
    res = res.length
      ? res + `, ${i18n.t('common:form.first_name')}`
      : i18n.t('common:form.first_name');
  }
  if (!data.last_name) {
    res = res.length
      ? res + `, ${i18n.t('common:form.last_name')}`
      : i18n.t('common:form.last_name');
  }
  if (!data.birthdate) {
    res = res.length
      ? res + `, ${i18n.t('main_flow:birthday')}`
      : i18n.t('main_flow.birthdate');
  }
  return res;
}

export default ({isVisible, onSuccess, onCancel, profile}) => {
  return (
    <Modal isVisible={isVisible} useNativeDriver>
      <ImageBackground source={BACKGROUND_2_IMAGE} style={styles.modalContent}>
        <Text style={styles.title}>
          {i18n.t('main_flow:profile_not_completely_filled')}
        </Text>
        <View style={styles.textWrapper}>
          <Text style={styles.text}>
            {i18n.t('main_flow:profile_fields_are_required')} {':'}
          </Text>
          <Text style={[styles.text, styles.bold]}>
            {RenderFields(profile)}
          </Text>
        </View>
        <View style={styles.buttonsWrapper}>
          <Button
            contentContainerStyle={styles.button}
            title={i18n.t('common:buttons.refill')}
            type="yellow"
            onPress={onSuccess}
          />
          <Button
            contentContainerStyle={styles.button}
            title={i18n.t('common:texts.cancel')}
            type="gray"
            onPress={onCancel}
          />
        </View>
      </ImageBackground>
    </Modal>
  );
};
