import React from 'react';
import PropTypes from 'prop-types';
import {Text, View} from 'react-native';
import styles from './styles';

const Badge = ({count, contentContainerStyle}) => {
  return (
    <View style={[styles.container, contentContainerStyle]}>
      <Text style={styles.text}>{count}</Text>
    </View>
  );
};

Badge.ListTypes = {
  count: PropTypes.string,
  contentContainerStyle: PropTypes.any,
};

export default Badge;
