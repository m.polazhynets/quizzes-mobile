import {StyleSheet} from 'react-native';
import {COLOR_DARK} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  text: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
    marginBottom: v_scale(10),
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(260),
  },
});
