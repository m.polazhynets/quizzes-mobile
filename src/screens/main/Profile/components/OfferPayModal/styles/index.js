import {StyleSheet} from 'react-native';
import variables, {
  v_scale,
  scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../../../constants/StylesConstants';
import {
  COLOR_WHITE,
  COLOR_GRAY_0,
  COLOR_DARK,
} from '../../../../../../constants/Colors';

const {large, regular} = variables.fontSize;

export default StyleSheet.create({
  button: {
    width: scale(300),
    height: v_scale(46),
  },
  form: {
    width: '100%',
    flexGrow: 1,
    marginTop: v_scale(10),
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    textAlign: 'center',
    color: COLOR_DARK,
    marginBottom: v_scale(8),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    textAlign: 'center',
    lineHeight: v_scale(22),
    color: COLOR_DARK,
  },
  modalContent: {
    backgroundColor: COLOR_WHITE,
    borderWidth: scale(2),
    borderColor: COLOR_GRAY_0,
    borderRadius: scale(10),
    justifyContent: 'center',
    padding: scale(25),
    paddingBottom: v_scale(18),
  },
  link: {
    paddingVertical: v_scale(10),
  },
  buttonsContent: {
    paddingTop: v_scale(15),
    alignItems: 'center',
  },
});
