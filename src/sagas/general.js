/**
 * @module Sagas/General
 * @desc General
 */

import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {GENERAL, TOURNAMENT, PROFILE, CHAT} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getPrivacyPolicy() {
  try {
    const response = yield call(request, 'privacy_policy/', {
      method: 'GET',
    });
    yield put({
      type: GENERAL.GET_PRIVACY_POLICY_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_PRIVACY_POLICY,
          payload: {},
        },
      });
    } else {
      yield put({
        type: GENERAL.GET_PRIVACY_POLICY_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* sendFeedback({payload: {name, subject, email, text}}) {
  try {
    const response = yield call(request, 'feedback/', {
      method: 'POST',
      payload: {name, subject, email, text},
    });
    yield put({
      type: GENERAL.SEND_FEEDBACK_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.SEND_FEEDBACK,
          payload: {name, subject, email, text},
        },
      });
    } else {
      yield put({
        type: GENERAL.SEND_FEEDBACK_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getFaqsList() {
  try {
    const response = yield call(request, 'faq/', {
      method: 'GET',
    });
    yield put({
      type: GENERAL.GET_FAQS_LIST_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_FAQS_LIST,
          payload: {},
        },
      });
    } else {
      yield put({
        type: GENERAL.GET_FAQS_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getFaqsListTournament() {
  try {
    const response = yield call(request, 'faq_tournament/', {
      method: 'GET',
    });
    yield put({
      type: GENERAL.GET_FAQS_LIST_TOURNAMENT_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_FAQS_LIST_TOURNAMENT,
          payload: {},
        },
      });
    } else {
      yield put({
        type: GENERAL.GET_FAQS_LIST_TOURNAMENT_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getCountriesList({payload: {search}}) {
  try {
    const response = yield call(request, 'countries/', {
      method: 'GET',
      params: {search},
    });
    yield put({
      type: GENERAL.GET_COUNTRIES_LIST_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_COUNTRIES_LIST,
          payload: {search},
        },
      });
    } else {
      yield put({
        type: GENERAL.GET_COUNTRIES_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getCitiesList({payload: {search, country}}) {
  try {
    const response = yield call(request, 'cities/', {
      method: 'GET',
      params: {search, country},
    });
    yield put({
      type: GENERAL.GET_CITIES_LIST_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_CITIES_LIST,
          payload: {search, country},
        },
      });
    } else {
      yield put({
        type: GENERAL.GET_CITIES_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* getHomeRequsts() {
  try {
    const profile = yield call(request, 'profile/', {
      method: 'GET',
    });
    yield put({
      type: PROFILE.GET_PROFILE_SUCCESS,
      payload: profile,
    });

    const tournaments = yield call(request, 'tournaments/', {
      method: 'GET',
    });
    yield put({
      type: TOURNAMENT.GET_TOURNAMENTS_LIST_SUCCESS,
      payload: tournaments,
    });

    const chats = yield call(request, 'chats/', {
      method: 'GET',
      params: {page: 1},
    });
    yield put({
      type: CHAT.GET_CHATS_LIST_SUCCESS,
      payload: chats,
      additionalPayload: {page: 1},
    });

    yield put({
      type: GENERAL.SET_APP_LOADED_PARAM,
      payload: true,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: GENERAL.GET_HOME_REQUESTS,
          payload: {},
        },
      });
    } else {
      yield put({
        type: PROFILE.GET_PROFILE_FAILURE,
        payload: err.response,
      });
      yield put({
        type: TOURNAMENT.GET_TOURNAMENTS_LIST_FAILURE,
        payload: err.response,
      });
      yield put({
        type: CHAT.GET_CHATS_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

/**
 * General Sagas
 */
export default function* root() {
  yield all([takeLatest(GENERAL.GET_PRIVACY_POLICY, getPrivacyPolicy)]);
  yield all([takeLatest(GENERAL.SEND_FEEDBACK, sendFeedback)]);
  yield all([takeLatest(GENERAL.GET_FAQS_LIST, getFaqsList)]);
  yield all([
    takeLatest(GENERAL.GET_FAQS_LIST_TOURNAMENT, getFaqsListTournament),
  ]);
  yield all([takeLatest(GENERAL.GET_COUNTRIES_LIST, getCountriesList)]);
  yield all([takeLatest(GENERAL.GET_CITIES_LIST, getCitiesList)]);
  yield all([takeLatest(GENERAL.GET_HOME_REQUESTS, getHomeRequsts)]);
}
