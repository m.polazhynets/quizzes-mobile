import {StyleSheet} from 'react-native';
import {scale} from '../../../../constants/StylesConstants';

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(280),
  },
});
