import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {NOTICATIONS} from '../constants/ActionTypes';

export default injectReducer(initialState.notificationReducer, {
  [NOTICATIONS.SHOW_NOTIFICATION]: (state, {payload: {message}}) => ({
    ...state,
    message,
  }),

  [NOTICATIONS.HIDE_NOTIFICATION]: state => ({
    ...state,
    message: null,
  }),

  [NOTICATIONS.GET_NOTIFICATION_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [NOTICATIONS.GET_NOTIFICATION_LIST_SUCCESS]: (
    state,
    {payload: {total_pages, results}, additionalPayload: {page}},
  ) => {
    return {
      ...state,
      list:
        page === 1
          ? results
          : state.list
          ? [...state.list, ...results]
          : results,
      is_request: false,
      list_filter_params: {
        ...state.list_filter_params,
        page,
        total_pages,
        last_page: page === total_pages,
      },
    };
  },
  [NOTICATIONS.GET_NOTIFICATION_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),
});
