import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';
import {HEADER_MAX_HEIGHT} from '../components/Header/styles';
import {
  COLOR_WHITE_DARKEN,
  COLOR_GRAY_5,
  COLOR_DARK,
  COLOR_YELLOW,
} from '../../../../constants/Colors';

const {small, regular, large} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE_DARKEN,
  },
  scrollContentStyles: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: isIphoneX()
      ? HEADER_MAX_HEIGHT - getStatusBarHeight()
      : HEADER_MAX_HEIGHT - getStatusBarHeight() + v_scale(20),
    position: 'relative',
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  headerButton: {
    position: 'absolute',
    right: scale(10),
    marginTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    paddingVertical: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
  },
  item: {
    paddingVertical: v_scale(8),
    paddingLeft: scale(50),
  },
  itemIcon: {
    width: scale(22),
    height: scale(22),
    position: 'absolute',
    top: v_scale(5),
    left: 0,
  },
  smallTitle: {
    fontFamily: FONT_BOLD,
    fontSize: small,
    color: COLOR_DARK,
    marginBottom: v_scale(5),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_GRAY_5,
    lineHeight: v_scale(22),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  yellowText: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_YELLOW,
    marginVertical: v_scale(5),
  },
  part: {
    flex: 1,
  },
  button: {
    alignItems: 'flex-start',
    paddingTop: v_scale(15),
    marginBottom: v_scale(15),
  },
});
