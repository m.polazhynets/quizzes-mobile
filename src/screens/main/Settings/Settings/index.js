import React, {Component} from 'react';
import {Text, View, Platform} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import Switch from 'react-native-switch-pro';
import * as Keychain from 'react-native-keychain';
import {Container, ListItem} from '../../../../components';
import {navigate} from '../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {
  toggleQuickLogIn,
  toggleEnablePushNotifications,
} from '../../../../actions/settingsActions';
import {removeFcmToken} from '../../../../actions/authActions';
import {COLOR_GRAY_1, COLOR_ROSE} from '../../../../constants/Colors';
import {scale} from '../../../../constants/StylesConstants';
import styles from './styles';

const options =
  Platform.OS === 'ios'
    ? {}
    : {
        accessControl: Keychain.ACCESS_CONTROL.APPLICATION_PASSWORD,
        rules: Keychain.SECURITY_RULES.NONE,
      };

class SettingsScreen extends Component {
  state = {
    enableBiometric: false,
  };

  componentDidMount() {
    this.getStorageData();
  }

  getStorageData = () => {
    Keychain.getGenericPassword(options)
      .then(res => {
        if (res) {
          const payload = JSON.parse(res.password);
          this.setState({enableBiometric: payload.enableBiometry});
        } else {
          this.setState({enableBiometric: false});
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({enableBiometric: false});
      });
  };

  toggleBiometricParam = val => {
    const {profile} = this.props;

    Keychain.getGenericPassword(options)
      .then(res => {
        if (res) {
          let payload = JSON.parse(res.password);
          payload.enableBiometry = val;
          Keychain.setGenericPassword(
            profile.email,
            JSON.stringify(payload),
            options,
          )
            .then(() => {
              this.setState({enableBiometric: payload.enableBiometry});
            })
            .catch(e => {
              console.log(e);
            });
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({enableBiometric: false});
      });
  };

  onChangePushSettings = val => {
    if (val) {
      this.props.toggleEnablePushNotifications(true);
    } else {
      this.props.removeFcmToken(false);
    }
  };

  render() {
    const {t, quick_log_in, biometry_type, enable_push} = this.props;
    const {enableBiometric} = this.state;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:settings'),
        }}>
        <View style={styles.content}>
          <View style={styles.switchBlock}>
            <Text style={[styles.title, styles.noMargin]}>
              {t('main_flow:quick_login')}
            </Text>
            <Switch
              value={quick_log_in}
              onSyncPress={value => this.props.toggleQuickLogIn(value)}
              backgroundInactive={COLOR_GRAY_1}
              backgroundActive={COLOR_ROSE}
              width={scale(38)}
              height={scale(22)}
            />
          </View>
          {quick_log_in && biometry_type && biometry_type.available && (
            <View style={styles.switchBlock}>
              <Text style={styles.label}>{t('main_flow:via_biometric')}</Text>
              <Switch
                value={enableBiometric}
                onSyncPress={value => this.toggleBiometricParam(value)}
                backgroundInactive={COLOR_GRAY_1}
                backgroundActive={COLOR_ROSE}
                width={scale(38)}
                height={scale(22)}
              />
            </View>
          )}
          <Text style={styles.title}>{t('main_flow:notification')}</Text>
          <View style={styles.switchBlock}>
            <Text style={styles.label}>{t('main_flow:push_enable')}</Text>
            <Switch
              value={enable_push}
              onSyncPress={this.onChangePushSettings}
              backgroundInactive={COLOR_GRAY_1}
              backgroundActive={COLOR_ROSE}
              width={scale(38)}
              height={scale(22)}
            />
          </View>
          <Text style={styles.title}>{t('main_flow:other')}</Text>
          <ListItem
            key="email"
            title={t('main_flow:change_email')}
            onPress={() => navigate(MainFlowKeys.ChangeEmail)}
            isArrow
          />
          <ListItem
            key="password"
            title={t('main_flow:change_password')}
            onPress={() => navigate(MainFlowKeys.ChangePassword)}
            isArrow
          />
          <ListItem
            key="language"
            title={t('main_flow:change_language')}
            noBorder
            onPress={() => navigate(MainFlowKeys.ChangeLanguage)}
            isArrow
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profileReducer.profile,
  biometry_type: state.settingsReducer.biometry_type,
  quick_log_in: state.settingsReducer.quick_log_in,
  enable_push: state.settingsReducer.enable_push,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {toggleQuickLogIn, toggleEnablePushNotifications, removeFcmToken},
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SettingsScreen),
);
