import {StyleSheet} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  FONT_REGULAR,
  FONT_MEDIUM,
  v_scale,
} from '../../../../constants/StylesConstants';
import {HEADER_HEIGHT} from '../components/Header/styles';
import {
  COLOR_WHITE,
  COLOR_GRAY_7,
  COLOR_GRAY_5,
  COLOR_DARK,
} from '../../../../constants/Colors';

const {regular, small} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_WHITE,
  },
  scrollContentStyles: {
    flexGrow: 1,
    backgroundColor: COLOR_GRAY_7,
    paddingHorizontal: scale(10),
    marginTop: isIphoneX()
      ? HEADER_HEIGHT - getStatusBarHeight() - 14
      : HEADER_HEIGHT - getStatusBarHeight(),
  },
  sendIcon: {
    width: scale(25),
    height: scale(25),
  },
  sendButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: v_scale(50),
    paddingHorizontal: scale(20),
  },
  textInputStyle: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
  },
  flexGrow: {
    flexGrow: 1,
  },
  leftBubbles: {
    backgroundColor: COLOR_WHITE,
    borderRadius: scale(10),
    borderTopStartRadius: 0,
    marginBottom: v_scale(10),
    ...variables.shadow,
  },
  rightBubbles: {
    backgroundColor: '#D6DEEB',
    borderRadius: scale(10),
    borderTopEndRadius: 0,
    marginBottom: v_scale(10),
    ...variables.shadow,
  },
  rightBubblesText: {
    color: COLOR_DARK,
    fontFamily: FONT_MEDIUM,
    fontSize: regular,
  },
  leftBubblesText: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
  },
  timeText: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_GRAY_5,
  },
});
