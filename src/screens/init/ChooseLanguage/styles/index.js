import {StyleSheet} from 'react-native';
import {scale, deviceHeight} from '../../../../constants/StylesConstants';

export default StyleSheet.create({
  content: {
    flex: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    height: '100%',
    maxHeight: deviceHeight * 0.5,
  },
  imageWrapper: {
    flex: 1,
  },
  list: {
    justifyContent: 'flex-end',
  },
  flex: {
    flex: 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
});
