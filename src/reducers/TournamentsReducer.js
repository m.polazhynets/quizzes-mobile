import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';
import _ from 'underscore';
import {TOURNAMENT, PROFILE} from '../constants/ActionTypes';

export default injectReducer(initialState.tournamentsReducer, {
  [TOURNAMENT.GET_TOURNAMENTS_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [TOURNAMENT.GET_TOURNAMENTS_LIST_SUCCESS]: (
    state,
    {payload: {results, tournament_data}},
  ) => ({
    ...state,
    is_request: false,
    tournaments_list: results,
    tournament_data,
  }),
  [TOURNAMENT.GET_TOURNAMENTS_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST]: state => ({
    ...state,
    is_modal_request: true,
    participants_list: null,
  }),
  [TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST_SUCCESS]: (
    state,
    {payload, additionalPayload: {id}},
  ) => {
    let tournaments_list = [];
    _.each(state.tournaments_list, item => {
      tournaments_list.push({
        ...item,
        players_count: item.id === id ? payload.length : item.players_count,
      });
    });
    return {
      ...state,
      is_modal_request: false,
      participants_list: payload,
      tournaments_list,
    };
  },
  [TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST_FAILURE]: state => ({
    ...state,
    is_modal_request: false,
  }),

  [TOURNAMENT.JOIN_TO_TOURNAMENT]: state => ({
    ...state,
    is_request: true,
    join_to_tournament: null,
  }),
  [TOURNAMENT.JOIN_TO_TOURNAMENT_SUCCESS]: (
    state,
    {additionalPayload: {id}},
  ) => {
    let tournaments_list = [];
    if (state.tournaments_list) {
      _.each(state.tournaments_list, item =>
        tournaments_list.push({
          ...item,
          is_joined: item.id === id ? true : item.is_joined,
        }),
      );
    }

    return {
      ...state,
      is_request: false,
      join_to_tournament: true,
      tournaments_list,
    };
  },
  [TOURNAMENT.JOIN_TO_TOURNAMENT_FAILURE]: state => ({
    ...state,
    is_request: false,
    join_to_tournament: false,
  }),

  [TOURNAMENT.LEAVE_TOURNAMENT]: state => ({
    ...state,
    is_request: true,
  }),
  [TOURNAMENT.LEAVE_TOURNAMENT_SUCCESS]: (state, {additionalPayload: {id}}) => {
    let tournaments_list = [];
    if (state.tournaments_list) {
      _.each(state.tournaments_list, item =>
        tournaments_list.push({
          ...item,
          is_joined: item.id === id ? false : item.is_joined,
        }),
      );
    }

    return {
      ...state,
      is_request: false,
      tournaments_list,
    };
  },
  [TOURNAMENT.LEAVE_TOURNAMENT_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [TOURNAMENT.GET_TOURNAMENT_QUESTION]: state => ({
    ...state,
    is_request: true,
    answer: null,
  }),
  [TOURNAMENT.GET_TOURNAMENT_QUESTION_SUCCESS]: (
    state,
    {payload: {question, result, tournament_tooltips, tournament_life}},
  ) => {
    const _result = result || null;

    return {
      ...state,
      is_request: false,
      tooltips: tournament_tooltips,
      life: tournament_life,
      result: _result,
      question,
    };
  },
  [TOURNAMENT.GET_TOURNAMENT_QUESTION_FAILURE]: (
    state,
    {
      payload: {
        data: {errors},
      },
    },
  ) => {
    let modalData = null;

    if (errors && errors.life) {
      modalData = {
        life: errors.life,
      };
    }
    return {
      ...state,
      is_request: false,
      answer: false,
      modalData,
    };
  },

  [TOURNAMENT.SEND_ANSWER]: state => ({
    ...state,
    is_request: true,
    modalData: null,
    answer: null,
  }),
  [TOURNAMENT.SEND_ANSWER_SUCCESS]: (
    state,
    {
      payload: {
        question,
        result,
        was_correct,
        tournament_tooltips,
        tournament_life,
      },
    },
  ) => {
    const _question = question || null;
    const _result = result || null;

    return {
      ...state,
      question: _question,
      result: _result,
      answer: was_correct,
      tooltips: tournament_tooltips || state.tooltips,
      life: tournament_life || state.life,
      is_request: false,
    };
  },
  [TOURNAMENT.SEND_ANSWER_FAILURE]: (
    state,
    {
      payload: {
        data: {errors},
      },
    },
  ) => {
    let modalData = null;

    if (errors && errors.life) {
      modalData = {
        life: errors.life,
      };
    }
    return {
      ...state,
      is_request: false,
      answer: false,
      modalData,
    };
  },

  [TOURNAMENT.CLEARE_OLD_DATA]: state => {
    return {
      ...state,
      tooltips: 0,
      life: 0,
      question: null,
      result: null,
      answer: null,
      current_tooltip: null,
    };
  },

  [TOURNAMENT.CLEARE_ANSWER]: state => {
    return {
      ...state,
      answer: null,
    };
  },

  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES]: state => ({
    ...state,
    is_modal_request: true,
  }),
  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {target}},
  ) => {
    return {
      ...state,
      is_modal_request: false,
      life: target === 'tooltip' ? state.life : state.life + 1,
      tooltips: target === 'tooltip' ? state.tooltips + 1 : state.tooltips,
    };
  },
  [PROFILE.ADD_TOURNAMENT_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_modal_request: false,
  }),

  [TOURNAMENT.GET_CURRENT_TOOLTIP]: state => ({
    ...state,
    is_modal_request: true,
  }),
  [TOURNAMENT.GET_CURRENT_TOOLTIP_SUCCESS]: (
    state,
    {payload: {right_answer_num, tournament_tooltips, tournament_life}},
  ) => ({
    ...state,
    current_tooltip: right_answer_num,
    life: tournament_life,
    tooltips: tournament_tooltips,
    is_modal_request: false,
  }),
  [TOURNAMENT.GET_CURRENT_TOOLTIP_FAILURE]: (
    state,
    {
      payload: {
        data: {errors},
      },
    },
  ) => {
    let modalData = null;

    if (errors && errors.time_out) {
      modalData = {
        time_out: errors.time_out,
      };
    }
    return {
      ...state,
      is_modal_request: false,
      modalData,
    };
  },
});
