import {I18nManager} from 'react-native';
import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import ru from './translations/ru.json';
import en from './translations/en.json';

i18n.use(initReactI18next).init({
  fallbackLng: 'ru',
  lng: 'ru',
  debug: false,
  resources: {ru, en},
});

I18nManager.forceRTL(false);
export default i18n;
