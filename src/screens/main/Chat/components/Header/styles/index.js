import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
} from '../../../../../../constants/StylesConstants';
import {COLOR_WHITE} from '../../../../../../constants/Colors';

export const HEADER_HEIGHT = isIphoneX()
  ? 82 + getStatusBarHeight()
  : 68 + getStatusBarHeight();
const {regular} = variables.fontSize;

export default StyleSheet.create({
  arrowIcon: {
    width: scale(30),
    height: v_scale(20),
  },
  backButton: {
    padding: scale(15),
    width: scale(60),
    zIndex: 2,
  },
  headerWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: HEADER_HEIGHT,
    zIndex: 1,
  },
  headerContainer: {
    paddingHorizontal: scale(10),
    paddingTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerContent: {
    flexGrow: 1,
    flexDirection: 'row',
    paddingLeft: scale(0),
    paddingRight: scale(14),
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageWrapper: {
    width: scale(52),
    height: scale(52),
    marginRight: scale(14),
    backgroundColor: 'rgba(234, 234, 234, 0.5)',
    borderRadius: scale(30),
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fullAvatar: {
    width: '100%',
    height: '100%',
  },
  avatar: {
    width: scale(24),
    height: scale(24),
  },
  headerProfile: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 3,
  },
  headerTextBlock: {
    paddingLeft: scale(20),
  },
  nicname: {
    fontFamily: FONT_BOLD,
    fontSize: regular,
    color: COLOR_WHITE,
  },
});
