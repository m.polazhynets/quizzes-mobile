import React, {Component} from 'react';
import {View, StatusBar, ImageBackground, Image, Text} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import RadialGradient from 'react-native-radial-gradient';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {getHomeRequests} from '../../../actions/generalActions';
import Header from './components/Header';
import {Button, PushNotificationServices} from '../../../components';
import {deviceWidth, deviceHeight} from '../../../constants/StylesConstants';
import {
  BACKGROUND_1_IMAGE,
  CONCEPT_LAMP_IMAGE,
} from '../../../constants/Images';
import styles from './styles';

const VIOLET = ['#DD01AD', '#2200AB'];

class HomeScreen extends Component {
  state = {
    disableStartRequest: this.props.route.params
      ? this.props.route.params.disableStartRequest
      : false,
  };

  componentDidMount() {
    const {disableStartRequest} = this.state;

    if (!disableStartRequest) {
      this.props.getHomeRequests();
    }
  }

  render() {
    const {t, tournaments_list} = this.props;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={VIOLET}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <PushNotificationServices />
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.wrapper}>
          <StatusBar
            barStyle="dark-content"
            translucent
            backgroundColor="transparent"
          />
          <SafeAreaView style={styles.content}>
            <Header />
            <View style={styles.content}>
              <View style={styles.imageWrapper}>
                <Text style={styles.title}>{t('main_flow:lets_start')}</Text>
                <Image
                  source={CONCEPT_LAMP_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.bottomContent}>
                {tournaments_list && tournaments_list.length ? (
                  <Button
                    contentContainerStyle={styles.button}
                    title={t('common:buttons.tournament')}
                    type="red"
                    onPress={() => navigate(MainFlowKeys.TournamentsList)}
                    margin
                  />
                ) : (
                  <View />
                )}
                <Button
                  contentContainerStyle={styles.button}
                  title={t('common:buttons.quiz')}
                  type="blue"
                  onPress={() => navigate(MainFlowKeys.QuizzesList)}
                />
              </View>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  tournaments_list: state.tournamentsReducer.tournaments_list,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getHomeRequests}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(HomeScreen),
);
