import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import {scale, v_scale} from '../../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../../components/Header/styles';

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(260),
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(27),
  },
  headerButton: {
    position: 'absolute',
    top:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    right: scale(10),
    height: HEADER_MIN_HEIGHT,
    paddingTop: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
    opacity: 0.2,
  },
  activeButton: {
    opacity: 1,
  },
});
