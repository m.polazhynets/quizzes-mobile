import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import variables, {
  scale,
  v_scale,
  deviceWidth,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../../../constants/StylesConstants';
import {COLOR_WHITE, COLOR_YELLOW} from '../../../../../../constants/Colors';

const {small, regular, large} = variables.fontSize;

export const HEADER_MIN_HEIGHT = isIphoneX()
  ? 68 + getStatusBarHeight()
  : 54 + getStatusBarHeight();
export const HEADER_MAX_HEIGHT = isIphoneX()
  ? 150 + getStatusBarHeight()
  : 125 + getStatusBarHeight();
export const IMAGE_WIDTH_START = scale(110);
export const IMAGE_WIDTH_FINISH = isIphoneX() ? v_scale(46) : v_scale(50);
export const IMAGE_HEIGHT_START = v_scale(90);
export const IMAGE_HEIGHT_FINISH = isIphoneX() ? v_scale(46) : v_scale(50);
export const IMAGE_BOTTOM_START = isIphoneX() ? v_scale(20) : v_scale(10);
export const IMAGE_BOTTOM_FINISH = v_scale(50);
export const IMAGE_LEFT_START = deviceWidth - scale(134);
export const IMAGE_LEFT_FINISH = deviceWidth - scale(70);
export const CONTENT_LEFT_START = scale(10);
export const CONTENT_LEFT_FINISH = scale(60);
export const CONTENT_BOTTOM_START = scale(20);
export const CONTENT_BOTTOM_FINISH = isIphoneX() ? scale(3) : scale(5);
export const CONTENT_WIDTH_START = deviceWidth - scale(20);
export const CONTENT_WIDTH_FINISH = deviceWidth - scale(70);
export const NICNAME_LEFT_START = scale(68);
export const NICNAME_LEFT_FINISH = scale(0);

export default StyleSheet.create({
  arrowIcon: {
    width: scale(30),
    height: v_scale(20),
  },
  backButton: {
    padding: scale(15),
    width: scale(60),
    zIndex: 2,
  },
  headerWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: Platform.OS === 'android' ? 0 : 1,
  },
  headerContainer: {
    paddingHorizontal: scale(10),
    paddingTop:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    flex: 1,
  },
  backgroundImage: {
    position: 'relative',
    zIndex: 1,
  },
  headerContent: {
    flexDirection: 'row',
    paddingHorizontal: scale(14),
    paddingBottom: v_scale(20),
    alignItems: 'center',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    zIndex: 2,
  },
  imageWrapper: {
    width: scale(52),
    height: scale(52),
    backgroundColor: 'rgba(234, 234, 234, 0.5)',
    borderRadius: scale(30),
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  headerProfile: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 3,
  },
  headerTextBlock: {
    paddingLeft: scale(20),
  },
  nicname: {
    fontFamily: FONT_BOLD,
    fontSize: small,
    color: COLOR_WHITE,
    position: 'absolute',
  },
  headerLabel: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_WHITE,
  },
  headerValue: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    color: COLOR_YELLOW,
  },
});
