import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {ERRORS} from '../constants/ActionTypes';

export default injectReducer(initialState.errorsReducer, {
  [ERRORS.SET_ERROR]: (state, {payload: {errors, label, message}}) => ({
    ...state,
    validation: {...errors},
    modal: label && message ? {label, message} : null,
  }),

  [ERRORS.DELETE_ERRORS]: () => ({
    ...initialState.errorsReducer,
  }),
});
