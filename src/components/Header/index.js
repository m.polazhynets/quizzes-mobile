import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image, Animated} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {goBack} from '../../navigator/RootNavigation';
import {ARROW_BACK_ICON, ARROW_BACK_WHITE_ICON} from '../../constants/Images';
import styles, {
  HEADER_MIN_HEIGHT,
  HEADER_MAX_HEIGHT,
  TITLE_BOTTOM_START,
  TITLE_BOTTOM_FINISH,
  TITLE_LEFT_START,
  TITLE_LEFT_FINISH,
  TITLE_FONT_SIZE_START,
  TITLE_FONT_SIZE_FINISH,
  TITLE_FONT_WEIGHT_START,
  TITLE_FONT_WEIGHT_FINISH,
  HEADER_BACKGROUND_START,
  HEADER_BACKGROUND_FINISH,
  IMAGE_WIDTH_START,
  IMAGE_WIDTH_FINISH,
  IMAGE_HEIGHT_START,
  IMAGE_HEIGHT_FINISH,
  IMAGE_LEFT_START,
  IMAGE_LEFT_FINISH,
  IMAGE_BOTTOM_START,
  IMAGE_BOTTOM_FINISH,
} from './styles';

const toolbarHeight = HEADER_MIN_HEIGHT;
const TRANSPARENT = ['rgba(0,0,0,0)', 'rgba(0,0,0,0)'];

export default class Header extends Component {
  state = {
    scrollOffset: new Animated.Value(0),
  };
  headerHeight = HEADER_MAX_HEIGHT;

  onScroll = e => {
    this.state.scrollOffset.setValue(e.nativeEvent.contentOffset.y);
  };

  _getHeight = () => {
    const {scrollOffset} = this.state;
    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [this.headerHeight, toolbarHeight],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getBackgroundColor = () => {
    const {scrollOffset} = this.state;
    const {backgroundStart, backgroundFinish} = this.props;
    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [
        backgroundStart || HEADER_BACKGROUND_START,
        backgroundFinish || HEADER_BACKGROUND_FINISH,
      ],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [TITLE_LEFT_START, TITLE_LEFT_FINISH],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getBottom = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [TITLE_BOTTOM_START, TITLE_BOTTOM_FINISH],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getFontSize = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [TITLE_FONT_SIZE_START, TITLE_FONT_SIZE_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getFontWeight = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [TITLE_FONT_WEIGHT_START, TITLE_FONT_WEIGHT_FINISH],
      extrapolate: 'clamp',
      easing: value => {
        const thousandRounded = value * 1000;
        return thousandRounded < TITLE_FONT_WEIGHT_FINISH ? 0 : 1;
      },
    });
  };

  _getImageWidth = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_WIDTH_START, IMAGE_WIDTH_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageHeight = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_HEIGHT_START, IMAGE_HEIGHT_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_LEFT_START, IMAGE_LEFT_FINISH],
      extrapolate: 'clamp',
    });
  };

  _getImageBottom = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [IMAGE_BOTTOM_START, IMAGE_BOTTOM_FINISH],
      extrapolate: 'clamp',
    });
  };

  render() {
    const {
      rightBtn,
      headerStyle = {},
      whiteBackBtn,
      title,
      backgroundType,
      backgroundImage,
      onPressGoBack,
      hideContent,
    } = this.props;
    const height = this._getHeight();
    const left = this._getLeft();
    const bottom = this._getBottom();
    const fontSize = this._getFontSize();
    const fontWeight = this._getFontWeight();
    const backgroundColor = this._getBackgroundColor();
    const style = {
      position: 'absolute',
      left: left,
      bottom: bottom,
      fontSize,
      fontWeight,
    };
    let imageStyle = {};
    if (backgroundImage) {
      const imageWidth = this._getImageWidth();
      const imageHeight = this._getImageHeight();
      const imageLeft = this._getImageLeft();
      const imageBottom = this._getImageBottom();
      imageStyle = {
        width: imageWidth,
        height: imageHeight,
        left: imageLeft,
        bottom: imageBottom,
      };
    }

    return (
      <Animated.View
        style={[
          styles.headerWrapper,
          headerStyle,
          {height: height, backgroundColor},
        ]}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 1, y: 0}}
          colors={backgroundType || TRANSPARENT}
          style={styles.headerContainer}>
          {!hideContent && (
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.backButton}
              onPress={() => {
                goBack();
                if (onPressGoBack) {
                  onPressGoBack();
                }
              }}>
              <Image
                source={whiteBackBtn ? ARROW_BACK_WHITE_ICON : ARROW_BACK_ICON}
                style={styles.arrowIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          )}
          {title && (
            <Animated.Text
              style={[
                styles.titleStyle,
                whiteBackBtn && styles.whiteTitle,
                style,
              ]}>
              {title}
            </Animated.Text>
          )}
          {rightBtn}
          {backgroundImage && (
            <Animated.Image
              source={backgroundImage}
              resizeMode="contain"
              style={[styles.backgroundImage, imageStyle]}
            />
          )}
        </LinearGradient>
      </Animated.View>
    );
  }
}

Header.propTypes = {
  rightBtn: PropTypes.node,
  headerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  whiteBackBtn: PropTypes.bool,
  title: PropTypes.string,
  backgroundImage: PropTypes.number,
  backgroundType: PropTypes.array,
  backgroundStart: PropTypes.string,
  backgroundFinish: PropTypes.string,
  onPressGoBack: PropTypes.func,
  hideContent: PropTypes.bool,
};
