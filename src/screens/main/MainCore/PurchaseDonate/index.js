import React, {Component} from 'react';
import {
  ImageBackground,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import Toast from 'react-native-simple-toast';
import RadialGradient from 'react-native-radial-gradient';
import {getDonatesList} from '../../../../actions/quizzesActions';
import {addQuizTipsLifes} from '../../../../actions/profileActions';
import {toogleDialogModal} from '../../../../actions/generalActions';
// import {navigate} from '../../../navigator/RootNavigation';
// import {AuthFlowKeys} from '../../../navigator/Keys';
import {Container, Button, Preloader} from '../../../../components';
import {
  BACKGROUND_1_IMAGE,
  BACKGROUND_2_IMAGE,
  QUESTION_ICON,
} from '../../../../constants/Images';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import styles from './styles';

class PurchaseDonateScreen extends Component {
  state = {
    is_refresh: false,
    dialogParams:
      this.props.route.params && this.props.route.params.dialogParams
        ? this.props.route.params.dialogParams
        : {},
  };

  componentDidMount() {
    this.props.getDonatesList();
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressGoBack,
    );
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      if (this.state.is_refresh) {
        this.setState({is_refresh: false});
      }
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onPressGoBack = () => {
    const {dialogParams} = this.state;
    this.props.toogleDialogModal(
      dialogParams.show_dialog_modal,
      dialogParams.dialog_type_key,
    );
  };

  showPay = ({title, price, package_type, count, id}) => {
    const {t} = this.props;

    Alert.alert(
      'Оплата',
      `Ви уверени что хотите купить пакет ${title} за ${price} Р`,
      [
        {
          text: 'Нет',
          style: 'cancel',
        },
        {
          text: 'Да',
          onPress: () =>
            this.props.addQuizTipsLifes(
              package_type,
              'money',
              count,
              id,
              () => {
                Toast.show(
                  `${t('quiz_flow:successfully_bought_package')}: ${title}`,
                  Toast.LONG,
                );
              },
            ),
        },
      ],
      {cancelable: false},
    );
  };

  renderOfferItem = ({id, title, description, price, package_type, count}) => {
    const {t} = this.props;

    return (
      <ImageBackground
        source={BACKGROUND_2_IMAGE}
        key={id}
        style={styles.itemOffer}>
        <Text style={styles.offerTitle}>{title}</Text>
        {/* {old_price && <Text style={styles.oldText}>{old_price}</Text>} */}
        {description ? (
          <Text style={styles.giftText}>{description}</Text>
        ) : (
          <View />
        )}
        <Button
          contentContainerStyle={styles.button}
          title={`${t('main_flow:buy_for')} ${price} Р`}
          type="yellow"
          onPress={() => this.showPay({package_type, id, price, count, title})}
        />
      </ImageBackground>
    );
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {}}
        style={styles.headerButton}>
        <Image
          source={QUESTION_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, donates_list, is_request, is_modal_request} = this.props;
    const {is_refresh} = this.state;
    const request = is_request || is_modal_request;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={['#DD01AD', '#2200AB']}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.flex}>
          <Container
            enableStatusBar={false}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}
            headerProps={{
              title: t('main_flow:donate_purchase'),
              whiteBackBtn: true,
              rightBtn: this.renderHeaderButton(),
              backgroundStart: 'transparent',
              backgroundFinish: '#DD01AD',
              onPressGoBack: this.onPressGoBack,
            }}>
            {request && !is_refresh && <Preloader />}
            {donates_list && (
              <ScrollView
                contentContainerStyle={styles.content}
                keyboardShouldPersistTaps="handled"
                refreshControl={
                  <RefreshControl
                    refreshing={is_refresh}
                    onRefresh={() => {
                      this.setState({is_refresh: true});
                      this.props.getDonatesList();
                    }}
                  />
                }>
                {donates_list.length ? (
                  donates_list.map(item => this.renderOfferItem(item))
                ) : (
                  <View>
                    <Text style={styles.emptyListText}>
                      {t('main_flow:no_quizzes_available')}
                    </Text>
                  </View>
                )}
              </ScrollView>
            )}
            {/* <View style={styles.content}>
              {LIST.map(item => this.renderOfferItem(item))}
              <View style={styles.itemOffer}>
                <Text style={styles.offerTitle}>
                  {t('main_flow:other_shopping_packages')}
                </Text>
                <Button
                  contentContainerStyle={styles.button}
                  title={`${t('common:buttons.buy')}`}
                  type="red"
                  onPress={() => {}}
                />
              </View>
            </View> */}
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.quizzesReducer.is_request,
  is_modal_request: state.quizzesReducer.is_modal_request,
  donates_list: state.quizzesReducer.donates_list,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {getDonatesList, addQuizTipsLifes, toogleDialogModal},
    dispatch,
  );

export default withTranslation(['common', 'main_flow', 'quiz_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PurchaseDonateScreen),
);
