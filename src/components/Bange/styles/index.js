import {StyleSheet} from 'react-native';
import {COLOR_BLUE_2, COLOR_WHITE} from '../../../constants/Colors';
import variables, {
  scale,
  FONT_MEDIUM,
} from '../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  text: {
    color: COLOR_WHITE,
    fontSize: regular,
    fontFamily: FONT_MEDIUM,
  },
  container: {
    backgroundColor: COLOR_BLUE_2,
    borderRadius: scale(20),
    paddingHorizontal: scale(5),
    height: scale(20),
    minWidth: scale(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
