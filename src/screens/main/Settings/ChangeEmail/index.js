import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {navigate} from '../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {CommonActions} from '@react-navigation/native';
import {changeEmail, verifyEmail} from '../../../../actions/profileActions';
import {CHECK_BLACK_ICON} from '../../../../constants/Images';
import {Container, Input, Preloader} from '../../../../components';
import {validateEmail} from '../../../../utils/Helper';
import styles from './styles';

class ChangeEmailScreen extends Component {
  state = {
    form: {
      email: {
        value: '',
        isValid: null,
        error: '',
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {code_send, t} = this.props;

    if (prevProps.code_send !== code_send && code_send) {
      navigate(MainFlowKeys.ConfirmCode_main, {
        headerTitle: t('main_flow:change_email'),
        text: t('main_flow:change_email_code_text'),
        requestAction: this.props.verifyEmail,
        callBack: () => {
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {name: MainFlowKeys.Home},
                {name: MainFlowKeys.Settings},
              ],
            }),
          );
        },
      });
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    this.setState({
      form: {
        ...form,
        [name]: {
          ...form[name],
          value: val,
          isValid: validateEmail(val),
        },
      },
    });
  };

  checkFullValidate = async () => {
    const {
      form: {email},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            email: {
              ...email,
              isValid: email.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.email.isValid ? resolve(true) : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {email},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.changeEmail(email.value);
    } catch (err) {
      console.log('error validation');
    }
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this.handleSubmit}
        style={[styles.headerButton, styles.activeButton]}>
        <Image
          source={CHECK_BLACK_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {email},
    } = this.state;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:change_email'),
          rightBtn: this.renderHeaderButton(),
        }}>
        {is_request && <Preloader />}
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Input
              label={t('common:form.email')}
              value={email.value}
              required
              isValid={email.isValid}
              onBlur={() => this.validate(email.value, 'email')}
              onChangeText={val => this.validate(val, 'email')}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  code_send: state.profileReducer.code_send,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({changeEmail, verifyEmail}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChangeEmailScreen),
);
