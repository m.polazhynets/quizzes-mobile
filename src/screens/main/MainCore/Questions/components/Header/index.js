import React from 'react';
import PropTypes from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import {MainFlowKeys} from '../../../../../../navigator/Keys';
import {
  ARROW_BACK_WHITE_ICON,
  QUESTION_ICON,
  TIMER_ICON,
} from '../../../../../../constants/Images';
import styles from './styles';

function renderTimer(time) {
  const min = Math.floor(time / 60);
  const sec = time % 60;

  return `0${min}:${sec >= 10 ? sec : '0' + sec}`;
}

const Header = ({timer, defaultTime}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.header}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.menuButton}
        onPress={() => navigation.goBack()}>
        <Image
          source={ARROW_BACK_WHITE_ICON}
          style={styles.arrowIcon}
          resizeMode="contain"
        />
      </TouchableOpacity>
      {timer || defaultTime ? (
        <View style={styles.timerBlock}>
          <Image
            source={TIMER_ICON}
            resizeMode="contain"
            style={styles.timerIcon}
          />
          <Text style={styles.timerText}>
            {renderTimer(timer !== null ? timer : defaultTime)}
          </Text>
        </View>
      ) : (
        <View />
      )}
      <View style={styles.rightSide}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.questionButton}
          onPress={() => {
            navigation.navigate({
              name: MainFlowKeys.FAQs,
              key: 'tournament',
            });
          }}>
          <Image
            source={QUESTION_ICON}
            style={styles.questionIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

Header.propTypes = {
  onQuestionPress: PropTypes.func,
};

export default Header;
