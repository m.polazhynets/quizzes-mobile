import {createActions} from 'redux-actions';

import {TOURNAMENT} from '../constants/ActionTypes';

export const {
  getTournamentsList,
  getTournamentParticipantsList,
  joinToTournament,
  leaveTournament,
  getTournamentQuestion,
  cleareOldData,
  cleareAnswer,
  getCurrentTooltip,
  sendAnswer,
} = createActions({
  [TOURNAMENT.GET_TOURNAMENTS_LIST]: () => ({}),
  [TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST]: id => ({id}),
  [TOURNAMENT.JOIN_TO_TOURNAMENT]: id => ({id}),
  [TOURNAMENT.LEAVE_TOURNAMENT]: id => ({id}),
  [TOURNAMENT.GET_TOURNAMENT_QUESTION]: (id, callback) => ({id, callback}),
  [TOURNAMENT.CLEARE_OLD_DATA]: () => ({}),
  [TOURNAMENT.CLEARE_ANSWER]: () => ({}),
  [TOURNAMENT.GET_CURRENT_TOOLTIP]: (id, callback) => ({id, callback}),
  [TOURNAMENT.SEND_ANSWER]: (id, data, callback) => ({
    id,
    data,
    callback,
  }),
});
