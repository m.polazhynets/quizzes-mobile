export {default as Header} from './Header';
export {default as DetailInfo} from './DetailInfo';
export {default as TipsAndLifesModal} from './TipsAndLifesModal';
export {default as OfferPayModal} from './OfferPayModal';
