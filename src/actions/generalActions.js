import {createActions} from 'redux-actions';

import {GENERAL} from '../constants/ActionTypes';

export const {
  getPrivacyPolicy,
  sendFeedback,
  getFaqsList,
  getFaqsListTournament,
  getCountriesList,
  getCitiesList,
  cleareAutocomplete,
  getHomeRequests,
  toogleDialogModal,
} = createActions({
  [GENERAL.GET_PRIVACY_POLICY]: () => ({}),
  [GENERAL.SEND_FEEDBACK]: (name, subject, email, text) => ({
    name,
    subject,
    email,
    text,
  }),
  [GENERAL.GET_FAQS_LIST]: () => ({}),
  [GENERAL.GET_FAQS_LIST_TOURNAMENT]: () => ({}),
  [GENERAL.GET_COUNTRIES_LIST]: search => ({search}),
  [GENERAL.GET_CITIES_LIST]: (search, country) => ({search, country}),
  [GENERAL.CLEARE_AUTOCOMPLETE]: () => ({}),
  [GENERAL.GET_HOME_REQUESTS]: () => ({}),
  [GENERAL.TOOGLE_DIALOG_MODAL]: (show, type_key) => ({
    show,
    type_key,
  }),
});
