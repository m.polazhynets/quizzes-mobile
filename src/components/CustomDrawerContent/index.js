import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Animated from 'react-native-reanimated';
import RadialGradient from 'react-native-radial-gradient';
import DeviceInfo from 'react-native-device-info';
import {withTranslation} from 'react-i18next';
import {logOut} from '../../actions/authActions';
import {navigate, replace} from '../../navigator/RootNavigation';
import {MainFlowKeys, AppFlowKeys, AuthFlowKeys} from '../../navigator/Keys';
import {Bange} from '../../components';
import {deviceWidth} from '../../constants/StylesConstants';
import {
  CLOSE_WHITE_ICON,
  PROFILE_ICON,
  ARROW_WHITE_RIGHT_ICON,
  ARROW_GRAY_ICON,
  MENU_BRAIN_ICON,
  MENU_PRIZE_ICON,
  MENU_FEEDBACK_ICON,
  MENU_POLICY_ICON,
  MENU_QUESTION_ICON,
  MENU_TOOLS_ICON,
  MENU_LOG_OUT_ICON,
  MENU_CHATS_ICON,
  MENU_NOTIFICATION_ICON,
  BRAIN_IMAGE,
} from '../../constants/Images';
import {COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN} from '../../constants/Colors';
import styles from './styles';

const LIST = [
  {
    id: '0',
    source: MENU_BRAIN_ICON,
    label: 'free_game',
    link: 'QuizzesList',
  },
  {
    id: '1',
    source: MENU_PRIZE_ICON,
    label: 'tournament_game',
    link: 'TournamentsList',
  },
  {
    id: '3',
    source: MENU_QUESTION_ICON,
    label: 'faq',
    link: 'FAQs',
  },
  {
    id: '4',
    source: MENU_POLICY_ICON,
    label: 'privacy_policy',
    link: 'Policy',
  },
  {
    id: '5',
    source: MENU_CHATS_ICON,
    label: 'chats',
    link: 'Chats',
  },
  {
    id: '6',
    source: MENU_FEEDBACK_ICON,
    label: 'feedback',
    link: 'Feedback',
  },
  {
    id: '7',
    source: MENU_NOTIFICATION_ICON,
    label: 'notification',
    link: 'Notifications',
  },
  {
    id: '8',
    source: MENU_TOOLS_ICON,
    label: 'settings',
    link: 'Settings',
  },
];

class CustomDrawerContent extends Component {
  logOut = () => {
    this.props.logOut();
    replace(AppFlowKeys.Auth, {
      screen: AuthFlowKeys.ChooseAuth,
      params: {headerEnabled: true},
    });
  };

  renderItem = ({label, id, source, link}) => {
    const {tournaments_list, not_read_count, t} = this.props;
    const {state} = this.props.state ? this.props.state.routes[0] : {};
    const currentPage = state ? state.routes[state.index] : null;

    if (
      link === MainFlowKeys.TournamentsList &&
      (!tournaments_list || !tournaments_list.length)
    ) {
      return null;
    }

    if (currentPage) {
      if (
        currentPage.name === MainFlowKeys.QuestionQuizzes &&
        link === MainFlowKeys.QuizzesList
      ) {
        return null;
      }
      if (
        currentPage.name === MainFlowKeys.QuestionTournament &&
        link === MainFlowKeys.TournamentsList
      ) {
        return null;
      }
    }

    return (
      <TouchableOpacity
        key={id}
        activeOpacity={0.8}
        onPress={() => {
          if (link) {
            navigate(MainFlowKeys[link]);
          }
        }}
        style={styles.item}>
        <Image source={source} resizeMode={'contain'} style={styles.iconMenu} />
        <View style={styles.itemContent}>
          <Text style={styles.itemText}>{`${t(
            `main_flow:menu.${label}`,
          )}`}</Text>
          <View style={styles.menuArrow}>
            {link === MainFlowKeys.Chats && not_read_count ? (
              <Bange count={not_read_count} />
            ) : (
              <View />
            )}
            <Image
              source={ARROW_GRAY_ICON}
              resizeMode="contain"
              style={styles.arrowIcon}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {progress, navigation, profile, t, ...rest} = this.props;

    return (
      <Animated.View style={[styles.flex]} {...rest}>
        <RadialGradient
          style={styles.flex}
          colors={[COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN]}
          stops={[0, 1]}
          center={[0, 0]}
          radius={deviceWidth}>
          <View style={styles.header}>
            <View style={styles.topHeader}>
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.closeButton}
                onPress={() => navigation.closeDrawer()}>
                <Image
                  source={CLOSE_WHITE_ICON}
                  style={styles.closeIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            {profile && (
              <TouchableOpacity
                style={styles.profileBlock}
                activeOpacity={0.8}
                onPress={() => navigate(MainFlowKeys.Profile)}>
                <View style={styles.leftSide}>
                  <View style={styles.imageWrapper}>
                    {profile.photo ? (
                      <Image
                        source={{uri: profile.photo}}
                        resizeMode="cover"
                        style={styles.avatar}
                      />
                    ) : (
                      <Image
                        source={PROFILE_ICON}
                        resizeMode="contain"
                        style={styles.profileIcon}
                      />
                    )}
                  </View>
                  <View style={styles.profileData}>
                    {profile.first_name && (
                      <Text style={styles.name}>{profile.first_name}</Text>
                    )}
                    <Text style={styles.email}>{profile.email}</Text>
                  </View>
                </View>
                <Image
                  source={ARROW_WHITE_RIGHT_ICON}
                  resizeMode="contain"
                  style={styles.arrowIcon}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.content}>
            <Image
              source={BRAIN_IMAGE}
              resizeMode="cover"
              style={styles.background}
            />
            <View>{LIST.map(item => this.renderItem(item))}</View>
            <View style={styles.bottomContent}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={this.logOut}
                style={styles.item}>
                <Image
                  source={MENU_LOG_OUT_ICON}
                  resizeMode={'contain'}
                  style={styles.iconMenu}
                />
                <View style={[styles.itemContent, styles.noBorder]}>
                  <Text style={styles.itemText}>
                    {t('main_flow:menu.log_out')}
                  </Text>
                </View>
              </TouchableOpacity>
              <Text style={styles.versionText}>
                {`${t('main_flow:version')} ${DeviceInfo.getVersion()}`}
              </Text>
            </View>
          </View>
        </RadialGradient>
      </Animated.View>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profileReducer.profile,
  tournaments_list: state.tournamentsReducer.tournaments_list,
  not_read_count: state.chatReducer.not_read_count,
});

const mapDispatchToProps = dispatch => bindActionCreators({logOut}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CustomDrawerContent),
);
