import {StyleSheet} from 'react-native';
import variables, {
  v_scale,
  scale,
  FONT_BOLD,
} from '../../../../../../../constants/StylesConstants';
import {COLOR_WHITE} from '../../../../../../../constants/Colors';

const {large} = variables.fontSize;

export default StyleSheet.create({
  listWrapper: {
    paddingTop: v_scale(20),
  },
  vieoWrapper: {
    marginTop: v_scale(16),
    marginHorizontal: -scale(8),
    justifyContent: 'center',
    alignItems: 'center',
    height: v_scale(220),
  },
  videoStyles: {
    backgroundColor: 'red',
    height: v_scale(220),
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  imageWrapper: {
    marginTop: v_scale(16),
    borderRadius: scale(10),
    overflow: 'hidden',
  },
  titleQuestion: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    lineHeight: v_scale(30),
    color: COLOR_WHITE,
  },
  image: {
    height: v_scale(220),
    width: '100%',
  },
});
