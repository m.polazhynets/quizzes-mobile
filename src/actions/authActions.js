import {createActions} from 'redux-actions';

import {AUTH} from '../constants/ActionTypes';

export const {
  logIn,
  socialLogIn,
  getConfirmationCode,
  verifyConfirmationCode,
  logOut,
  resetPassword,
  changePassword,
  refreshToken,
  setFcmToken,
  removeFcmToken,
} = createActions({
  [AUTH.LOG_IN]: (email, password) => ({email, password}),
  [AUTH.SOCIAL_LOG_IN]: (access_token, provider) => ({access_token, provider}),
  [AUTH.GET_CONFIRMATION_CODE]: (email, reset) => ({email, reset}),
  [AUTH.VERIFY_CONFIRMATION_CODE]: (code, email, password) => ({
    code,
    email,
    password,
  }),
  [AUTH.LOG_OUT]: () => ({}),
  [AUTH.RESET_PASSWORD]: (email, password) => ({email, password}),
  [AUTH.CHANGE_PASSWORD]: (password_old, password_new) => ({
    password_old,
    password_new,
  }),
  [AUTH.REFRESH_TOKEN]: nextRequest => ({
    nextRequest,
  }),
  [AUTH.SET_FCM_TOKEN]: notification_token => ({
    notification_token,
  }),
  [AUTH.REMOVE_FCM_TOKEN]: enable_push => ({
    enable_push,
  }),
});
