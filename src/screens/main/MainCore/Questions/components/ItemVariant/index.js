import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {CLOSE_WHITE_ICON, CHECK_ICON} from '../../../../../../constants/Images';
import styles, {SUCCESS, FAILED, DEFAULT} from './styles';

const ItemVariant = ({
  text,
  iGuessed,
  isFailed,
  order_num,
  onPress,
  showTooltip,
}) => {
  return (
    <LinearGradient
      start={{x: 0.5, y: 0.5}}
      end={{x: 1, y: 0.5}}
      style={styles.item}
      colors={iGuessed ? SUCCESS : isFailed ? FAILED : DEFAULT}>
      <TouchableOpacity
        style={[styles.itemContent, showTooltip && styles.greenBorder]}
        activeOpacity={0.8}
        onPress={() => onPress(order_num)}>
        <Text style={[styles.text, (iGuessed || isFailed) && styles.textWhite]}>
          {text}
        </Text>
        {(iGuessed || isFailed) && (
          <Image
            source={iGuessed ? CHECK_ICON : CLOSE_WHITE_ICON}
            resizeMode="contain"
            style={iGuessed ? styles.checkIcon : styles.closeIcon}
          />
        )}
      </TouchableOpacity>
    </LinearGradient>
  );
};

ItemVariant.propTypes = {
  text: PropTypes.string,
  order_num: PropTypes.number,
  onPress: PropTypes.func,
  iGuessed: PropTypes.bool,
  isFailed: PropTypes.bool,
  showTooltip: PropTypes.bool,
};

export default ItemVariant;
