import {createActions} from 'redux-actions';

import {QUIZZES} from '../constants/ActionTypes';

export const {
  getQuizzesList,
  getQuestionsListQuizzes,
  setAnswersToStoreQuizzes,
  changeCurrentQuestionDataQuizzes,
  cleareAnswersQuizzes,
  sendResultQuiz,
  getDonatesList,
} = createActions({
  [QUIZZES.GET_QUIZZES_LIST]: () => ({}),
  [QUIZZES.GET_QUESTIONS_LIST_QUIZZES]: id => ({id}),
  [QUIZZES.SET_ANSWERS_TO_STORE_QUIZZES]: (id, answer) => ({id, answer}),
  [QUIZZES.CHANGE_CURRENT_QUESTION_DATA_QUIZZES]: data => ({...data}),
  [QUIZZES.CLEARE_ANSWERS_QUIZZES]: id => ({id}),
  [QUIZZES.SEND_RESULT_QUIZ]: (id, data, callback) => ({
    id,
    data,
    callback,
  }),
  [QUIZZES.GET_DONATES_LIST]: () => ({}),
});
