import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as Keychain from 'react-native-keychain';
import {logIn} from '../../../actions/authActions';
import {navigate, replace} from '../../../navigator/RootNavigation';
import {AuthFlowKeys, AppFlowKeys, MainFlowKeys} from '../../../navigator/Keys';
import {Button, Container, Input, ButtonLink} from '../../../components';
import {validateEmail, validatePassword} from '../../../utils/Helper';
import styles from './styles';

const options =
  Platform.OS === 'ios'
    ? {}
    : {
        accessControl: Keychain.ACCESS_CONTROL.APPLICATION_PASSWORD,
        rules: Keychain.SECURITY_RULES.NONE,
      };

class LoginScreen extends Component {
  state = {
    form: {
      email: {
        value: '',
        isValid: null,
        error: '',
      },
      password: {
        value: '',
        isValid: null,
        error: '',
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {access_token} = this.props;
    const {form} = this.state;

    if (prevProps.access_token !== access_token && access_token) {
      Keychain.getGenericPassword(options)
        .then(res => {
          if (res) {
            if (res.username === form.email.value) {
              replace(AppFlowKeys.Main, {
                screen: MainFlowKeys.Home,
              });
            } else {
              replace(AuthFlowKeys.PinCode, {
                email: form.email.value,
                createPin: true,
              });
            }
          } else {
            replace(AuthFlowKeys.PinCode, {
              email: form.email.value,
              createPin: true,
            });
          }
        })
        .catch(e => {
          replace(AppFlowKeys.Auth, {
            screen: AuthFlowKeys.PinCode,
            params: {email: form.email.value, createPin: true},
          });
        });
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'password':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val),
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateEmail(val),
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {email, password},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            email: {
              ...email,
              isValid: email.isValid === true,
            },
            password: {
              ...password,
              isValid: password.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.email.isValid && form.password.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {email, password},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.logIn(email.value, password.value);
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {email, password},
    } = this.state;

    return (
      <Container
        headerProps={{
          title: t('auth_flow:login'),
        }}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Input
              label={t('common:form.email')}
              value={email.value}
              required
              isValid={email.isValid}
              onBlur={() => this.validate(email.value, 'email')}
              onChangeText={val => this.validate(val, 'email')}
            />
            <Input
              label={t('common:form.password')}
              value={password.value}
              required
              secureTextEntry
              isValid={password.isValid}
              onBlur={() => this.validate(password.value, 'password')}
              onChangeText={val => this.validate(val, 'password')}
            />
            <ButtonLink
              contentContainerStyle={styles.link}
              title={t('common:buttons.forget_password')}
              onPress={() => navigate(AuthFlowKeys.ResetPassword)}
            />
          </View>
          <View style={styles.bottomContent}>
            <Button
              contentContainerStyle={styles.button}
              title={t('common:buttons.sign_in')}
              type="red"
              isLoading={is_request}
              onPress={this.handleSubmit}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  access_token: state.authReducer.access_token,
  is_request: state.authReducer.is_request,
});

const mapDispatchToProps = dispatch => bindActionCreators({logIn}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginScreen),
);
