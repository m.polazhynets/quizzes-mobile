import {StyleSheet} from 'react-native';
import {COLOR_DARK} from '../../../../../../../constants/Colors';
import variables, {
  v_scale,
  FONT_REGULAR,
} from '../../../../../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  label: {
    color: COLOR_DARK,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
  },
  switchBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: v_scale(15),
  },
});
