import {StyleSheet} from 'react-native';
import {
  COLOR_RED,
  COLOR_DARK,
  COLOR_WHITE,
  COLOR_GRAY_2,
  COLOR_VIOLET,
  COLOR_VIOLET_2,
} from '../../../constants/Colors';
import variables, {
  FONT_REGULAR,
  scale,
  v_scale,
} from '../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  modalContainer: {
    backgroundColor: COLOR_WHITE,
    padding: scale(24),
    borderRadius: scale(10),
  },
  container: {
    position: 'relative',
    paddingVertical: v_scale(10),
  },
  inputWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
    paddingHorizontal: 0,
    color: COLOR_DARK,
    letterSpacing: -0.41,
    borderColor: 'white',
  },
  inputError: {
    color: COLOR_RED,
  },
  inputContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    borderWidth: scale(1),
    borderColor: COLOR_GRAY_2,
    backgroundColor: COLOR_WHITE,
    height: v_scale(60),
    paddingHorizontal: scale(22),
    borderRadius: scale(10),
    //backgroundColor: 'red'
  },
  errorStyle: {
    borderColor: COLOR_RED,
  },
  successStyle: {
    borderColor: COLOR_VIOLET,
  },
  placeholder: {
    color: 'red',
  },
  noPadding: {
    paddingBottom: 0,
  },
  successLabelStyle: {
    color: COLOR_VIOLET_2,
  },
  errorLabelStyle: {
    color: COLOR_RED,
  },
  label: {
    position: 'absolute',
    left: scale(17),
    paddingHorizontal: scale(5),
    fontFamily: FONT_REGULAR,
  },
  errorMessageStyles: {
    marginTop: v_scale(5),
    fontSize: regular,
    fontFamily: FONT_REGULAR,
    color: COLOR_RED,
  },
  calendarIcon: {
    width: scale(19),
    height: scale(21),
  },
});
