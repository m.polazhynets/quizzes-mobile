import {StyleSheet} from 'react-native';
import {COLOR_WHITE} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  deviceHeight,
  FONT_MEDIUM,
} from '../../../../constants/StylesConstants';

const {extraLarge} = variables.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  content: {
    flex: 1,
    padding: scale(24),
    justifyContent: 'space-between',
  },
  image: {
    width: '80%',
    height: '80%',
    maxHeight: deviceHeight * 0.5,
  },
  imageWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  link: {
    marginBottom: v_scale(16),
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(230),
  },
  title: {
    fontFamily: FONT_MEDIUM,
    fontSize: extraLarge,
    color: COLOR_WHITE,
    textAlign: 'center',
    marginBottom: v_scale(10),
  },
});
