import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';
import _ from 'underscore';
import {QUIZZES, PROFILE} from '../constants/ActionTypes';

export default injectReducer(initialState.quizzesReducer, {
  [QUIZZES.GET_QUIZZES_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [QUIZZES.GET_QUIZZES_LIST_SUCCESS]: (state, {payload: {results}}) => ({
    ...state,
    is_request: false,
    quizzes_list: results,
  }),
  [QUIZZES.GET_QUIZZES_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [QUIZZES.GET_QUESTIONS_LIST_QUIZZES]: state => ({
    ...state,
    is_request: true,
  }),
  [QUIZZES.GET_QUESTIONS_LIST_QUIZZES_SUCCESS]: (
    state,
    {payload: {results, tooltips, life, complete_num}},
  ) => ({
    ...state,
    is_request: false,
    tooltips,
    life,
    complete_num,
    questions_list: results,
  }),
  [QUIZZES.GET_QUESTIONS_LIST_QUIZZES_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [QUIZZES.SET_ANSWERS_TO_STORE_QUIZZES]: (state, {payload: {id, answer}}) => {
    let _answers = {};

    if (state.answers) {
      _.each(state.answers, (item, key) => (_answers[key] = [...item]));
    }
    if (!_answers[id]) {
      _answers[id] = answer;
    } else {
      _answers[id] = [..._answers[id], {...answer}];
    }
    return {
      ...state,
      answers: _answers,
    };
  },

  [QUIZZES.CHANGE_CURRENT_QUESTION_DATA_QUIZZES]: (state, {payload}) => {
    return {
      ...state,
      currentQuestion: payload,
    };
  },

  [QUIZZES.CLEARE_ANSWERS_QUIZZES]: (state, {payload: {id}}) => {
    let answers = {};

    if (state.answers) {
      _.each(state.answers, (item, key) => {
        if (+key === +id) {
          answers[key] = [];
        } else {
          answers[key] = [...item];
        }
      });
    } else {
      answers[id] = [];
    }
    return {
      ...state,
      answers,
    };
  },

  [PROFILE.ADD_QUIZ_TIPS_LIFES]: state => ({
    ...state,
    is_modal_request: true,
  }),
  [PROFILE.ADD_QUIZ_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {count, target, method}},
  ) => {
    let life = state.life;
    let tooltips = state.tooltips;

    // if (method === 'advertising') {
    //   if (target === 'tooltip') {
    //     tooltips = tooltips + count;
    //   } else {
    //     life = life + count;
    //   }
    // }
    if (target === 'tooltip') {
      tooltips = tooltips + count;
    } else {
      life = life + count;
    }

    return {
      ...state,
      life,
      tooltips,
      is_modal_request: false,
    };
  },
  [PROFILE.ADD_QUIZ_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_modal_request: false,
  }),

  [PROFILE.REMOVE_QUIZ_TIPS_LIFES]: state => ({
    ...state,
    is_modal_request: true,
  }),
  [PROFILE.REMOVE_QUIZ_TIPS_LIFES_SUCCESS]: (
    state,
    {additionalPayload: {target}},
  ) => {
    let life = state.life;
    let tooltips = state.tooltips;

    if (target === 'tooltip') {
      tooltips = tooltips - 1;
    } else {
      life = life - 1;
    }

    return {
      ...state,
      life,
      tooltips,
      is_modal_request: false,
    };
  },
  [PROFILE.REMOVE_QUIZ_TIPS_LIFES_FAILURE]: state => ({
    ...state,
    is_modal_request: false,
  }),

  [QUIZZES.SEND_RESULT_QUIZ]: state => ({
    ...state,
    is_request: true,
    result_saved: null,
  }),
  [QUIZZES.SEND_RESULT_QUIZ_SUCCESS]: (state, {additionalPayload: {id}}) => {
    let answers = {};

    if (state.answers) {
      _.each(state.answers, (item, key) => {
        if (+key === +id) {
          answers[key] = [];
        } else {
          answers[key] = [...item];
        }
      });
    }

    return {
      ...state,
      is_request: false,
      result_saved: true,
      answers,
    };
  },
  [QUIZZES.SEND_RESULT_QUIZ_FAILURE]: state => ({
    ...state,
    is_request: false,
    result_saved: false,
  }),

  [QUIZZES.GET_DONATES_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [QUIZZES.GET_DONATES_LIST_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    donates_list: payload,
  }),
  [QUIZZES.GET_DONATES_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),
});
