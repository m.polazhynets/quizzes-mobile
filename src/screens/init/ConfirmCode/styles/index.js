import {StyleSheet} from 'react-native';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_MEDIUM,
} from '../../../../constants/StylesConstants';
import {COLOR_DARK, COLOR_GRAY_5} from '../../../../constants/Colors';

const {regular, extraLarge} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_DARK,
    lineHeight: v_scale(22),
  },
  hideInput: {
    opacity: 0,
  },
  code: {
    width: scale(40),
    height: v_scale(60),
    justifyContent: 'flex-end',
    borderBottomWidth: scale(1),
    borderColor: COLOR_GRAY_5,
    marginHorizontal: scale(5),
    paddingBottom: v_scale(7),
  },
  codeBlock: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: v_scale(100),
  },
  number: {
    textAlign: 'center',
    fontFamily: FONT_MEDIUM,
    fontSize: extraLarge,
    color: COLOR_DARK,
  },
});
