import {StyleSheet} from 'react-native';
import {scale, v_scale} from '../../../../../../../constants/StylesConstants';

export default StyleSheet.create({
  progress: {
    marginHorizontal: scale(23),
    width: '85%',
    height: scale(3),
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    borderRadius: scale(3),
    marginBottom: v_scale(20),
    overflow: 'hidden',
  },
  leftSide: {
    backgroundColor: 'red',
    width: 0.7,
    height: scale(3),
  },
});
