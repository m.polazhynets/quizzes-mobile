import {StyleSheet} from 'react-native';
import {COLOR_DARK} from '../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
} from '../../../constants/StylesConstants';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  itemText: {
    color: COLOR_DARK,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
  },
  icon: {
    width: scale(14),
    height: scale(14),
  },
  item: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    paddingVertical: scale(19),
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    textAlign: 'center',
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  textWrapper: {
    justifyContent: 'center',
  },
  rightContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkIcon: {
    height: v_scale(20),
    width: scale(27),
    position: 'absolute',
    right: 0,
  },
});
