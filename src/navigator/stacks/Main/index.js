import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {CustomDrawerContent} from '../../../components';
import {DRAWER_WIDTH} from '../../../components/CustomDrawerContent/styles';
import {
  HomeScreen,
  TournamentsListScreen,
  QuestionQuizzesScreen,
  QuestionTournamentScreen,
  PurchaseDonateScreen,
  PolicyScreen,
  FAQsScreen,
  FeedbackScreen,
  SettingsScreen,
  ChangePasswordScreen,
  ChangeLanguageScreen,
  ChangeEmailScreen,
  ProfileScreen,
  ProfileEditScreen,
  StandingsScreen,
  StatisticsScreen,
  UserScreen,
  AutocompleteScreen,
  ChatsScreen,
  ChatScreen,
  TransactionHistoryScreen,
  QuizzesListScreen,
  CongratulationScreen,
  ConfirmCodeScreen,
  TermsParticipationScreen,
  NotificationsScreen,
} from '../../Screens';
import {MainFlowKeys} from '../../Keys';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const stack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <Stack.Screen name={MainFlowKeys.Home} component={HomeScreen} />
      <Stack.Screen
        name={MainFlowKeys.QuestionTournament}
        component={QuestionTournamentScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.QuestionQuizzes}
        component={QuestionQuizzesScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.TournamentsList}
        component={TournamentsListScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.QuizzesList}
        component={QuizzesListScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.Congratulation}
        component={CongratulationScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.PurchaseDonate}
        component={PurchaseDonateScreen}
      />
      <Stack.Screen name={MainFlowKeys.Policy} component={PolicyScreen} />
      <Stack.Screen
        name={MainFlowKeys.ProfileEdit}
        component={ProfileEditScreen}
      />
      <Stack.Screen name={MainFlowKeys.FAQs} component={FAQsScreen} />
      <Stack.Screen name={MainFlowKeys.Feedback} component={FeedbackScreen} />
      <Stack.Screen name={MainFlowKeys.Settings} component={SettingsScreen} />
      <Stack.Screen
        name={MainFlowKeys.ChangePassword}
        component={ChangePasswordScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.ChangeLanguage}
        component={ChangeLanguageScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.ChangeEmail}
        component={ChangeEmailScreen}
      />
      <Stack.Screen name={MainFlowKeys.Profile} component={ProfileScreen} />
      <Stack.Screen name={MainFlowKeys.Standings} component={StandingsScreen} />
      <Stack.Screen
        name={MainFlowKeys.Statistics}
        component={StatisticsScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.Autocomplete}
        component={AutocompleteScreen}
      />
      <Stack.Screen name={MainFlowKeys.User} component={UserScreen} />
      <Stack.Screen name={MainFlowKeys.Chats} component={ChatsScreen} />
      <Stack.Screen name={MainFlowKeys.Chat} component={ChatScreen} />
      <Stack.Screen
        name={MainFlowKeys.TransactionHistory}
        component={TransactionHistoryScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.ConfirmCode_main}
        component={ConfirmCodeScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.TermsParticipation}
        component={TermsParticipationScreen}
      />
      <Stack.Screen
        name={MainFlowKeys.Notifications}
        component={NotificationsScreen}
      />
    </Stack.Navigator>
  );
};

const MainStack = () => {
  return (
    <Drawer.Navigator
      initialRouteName={MainFlowKeys.Home}
      drawerContent={props => <CustomDrawerContent {...props} />}
      drawerStyle={{width: DRAWER_WIDTH}}>
      <Drawer.Screen name={MainFlowKeys.Home} component={stack} />
    </Drawer.Navigator>
  );
};

export default MainStack;
