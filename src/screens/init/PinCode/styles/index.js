import {StyleSheet} from 'react-native';
import {COLOR_GRAY_2, COLOR_DARK} from '../../../../constants/Colors';
import variables, {
  scale,
  deviceHeight,
  v_scale,
  FONT_MEDIUM,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';

const {extraLarge, regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    height: '100%',
    maxHeight: deviceHeight * 0.5,
  },
  imageWrapper: {
    flex: 1,
  },
  bottomContent: {
    alignItems: 'center',
    paddingTop: v_scale(60),
  },
  buttonTextStyle: {
    color: COLOR_DARK,
    fontFamily: FONT_MEDIUM,
    fontSize: extraLarge,
  },
  deleteIcon: {
    width: scale(18),
    height: scale(18),
  },
  biometricIcon: {
    width: scale(40),
    height: scale(40),
  },
  inputViewEmptyStyle: {
    backgroundColor: COLOR_GRAY_2,
    width: scale(18),
    height: scale(18),
    marginHorizontal: scale(12),
  },
  inputViewFilledStyle: {
    width: scale(18),
    height: scale(18),
    marginHorizontal: scale(12),
  },
  inputAreaStyle: {
    marginBottom: v_scale(30),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_DARK,
  },
  titleBlock: {
    marginBottom: v_scale(20),
  },
  flex: {
    flex: 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
});
