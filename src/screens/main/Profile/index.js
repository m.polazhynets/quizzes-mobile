import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getProfile,
  addQuizTipsLifes,
  getDisbursement,
} from '../../../actions/profileActions';
import {EDIT_ICON, PROFILE_ICON} from '../../../constants/Images';
import {Header, DetailInfo, OfferPayModal} from './components';
import {Preloader, Admob} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from './components/Header/styles';
import {
  BIRTHDAY_ICON,
  LOCATION_ICON,
  EMAIL_ICON,
} from '../../../constants/Images';
import styles, {
  LEFT_AVATAR_START,
  LEFT_AVATAR_FINISH,
  TOP_AVATAR_START,
  TOP_AVATAR_FINISH,
  AVATAR_SIZE_START,
  AVATAR_SIZE_FINISH,
} from './styles';

const toolbarHeight = HEADER_MIN_HEIGHT;

class Profile extends Component {
  state = {
    scrollOffset: new Animated.Value(0),
    watchAdvertsGoal: null,
    disbursementShowModal: false,
    admob: null,
  };
  headerHeight = HEADER_MAX_HEIGHT;
  header = React.createRef();

  componentDidMount() {
    this.props.getProfile();
  }

  onScroll = e => {
    this.state.scrollOffset.setValue(e.nativeEvent.contentOffset.y);
  };

  _getLeft = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [LEFT_AVATAR_START, LEFT_AVATAR_FINISH],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getSize = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [AVATAR_SIZE_START, AVATAR_SIZE_FINISH],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getTop = () => {
    const {scrollOffset} = this.state;

    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight - toolbarHeight],
      outputRange: [TOP_AVATAR_START, TOP_AVATAR_FINISH],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  showVideo = goal => {
    const {t} = this.props;
    const {admob} = this.state;

    if (!admob) {
      return Toast.show(
        t('common:errors.adverts_initialization_error'),
        Toast.SHORT,
      );
    }

    admob.show();
    this.setState({watchAdvertsGoal: goal});
    console.log('show');
  };

  onFinishedWatching = () => {
    const {watchAdvertsGoal} = this.state;

    this.props.addQuizTipsLifes(
      watchAdvertsGoal === 'tooltip' ? 'tooltip' : 'life',
      'advertising',
      1,
    );
  };

  onCancelModal = () => {
    this.setState({disbursementShowModal: false});
  };

  onSendDisbursement = (num, text) => {
    const {t} = this.props;

    this.onCancelModal();
    this.props.getDisbursement(num, text, res => {
      if (res) {
        Alert.alert('', t('main_flow:order_sent'));
      }
    });
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => navigate(MainFlowKeys.ProfileEdit)}
        style={styles.headerButton}>
        <Image
          source={EDIT_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, profile, is_request} = this.props;
    const {disbursementShowModal} = this.state;
    const left = this._getLeft();
    const top = this._getTop();
    const size = this._getSize();

    const avatarStyle = {
      left,
      top,
      width: size,
      height: size,
    };

    if (!profile) {
      return null;
    }

    return (
      <View style={styles.wrapper}>
        {is_request && <Preloader />}
        <Admob
          onFinishedWatching={this.onFinishedWatching}
          onLoaded={admob => this.setState({admob})}
          onCanceled={() => this.setState({admob: null})}
        />
        <SafeAreaView style={styles.content}>
          <Header
            ref={this.header}
            title={t('main_flow:profile')}
            whiteBackBtn
            rightBtn={this.renderHeaderButton()}
            background={'violet'}
          />
          <Animated.View style={[styles.avatarWrapper, avatarStyle]}>
            <Image
              source={profile.photo ? {uri: profile.photo} : PROFILE_ICON}
              resizeMode={profile.photo ? 'cover' : 'contain'}
              style={styles.avatar}
            />
          </Animated.View>
          <KeyboardAwareScrollView
            contentContainerStyle={styles.scrollContentStyles}
            onScroll={e => {
              this.header.current.onScroll(e);
              this.onScroll(e);
            }}
            scrollEventThrottle={16}>
            <View style={styles.userInfoBlock}>
              {profile.nickname || profile.first_name || profile.last_name ? (
                <View style={styles.row}>
                  {profile.nickname && (
                    <Text style={styles.title}>{profile.nickname}</Text>
                  )}
                  {(profile.first_name || profile.last_name) && (
                    <Text style={styles.text}>{`${profile.last_name ||
                      ''} ${profile.first_name || ''} ${profile.middle_name ||
                      ''}`}</Text>
                  )}
                </View>
              ) : (
                <View />
              )}
              <View style={styles.item}>
                <Image
                  source={BIRTHDAY_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:birthday')}</Text>
                {profile.birthdate && (
                  <Text style={styles.text}>
                    {moment(profile.birthdate).format('DD.MM.YYYY')}
                  </Text>
                )}
              </View>
              <View style={styles.item}>
                <Image
                  source={EMAIL_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:email')}</Text>
                {profile.email && (
                  <Text style={styles.text}>{profile.email}</Text>
                )}
              </View>
              <View style={styles.item}>
                <Image
                  source={LOCATION_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:location')}</Text>
                {profile.country_name || profile.city_name ? (
                  <Text style={styles.text}>{`${profile.city_name}, ${
                    profile.country_name
                  }`}</Text>
                ) : (
                  <View />
                )}
              </View>
            </View>
            <DetailInfo
              isTournament
              shodDisbursementModal={() =>
                this.setState({disbursementShowModal: true})
              }
            />
            <DetailInfo showVideo={this.showVideo} />
            <OfferPayModal
              isVisible={disbursementShowModal}
              onCancel={this.onCancelModal}
              maxAmount={profile.wallet.amount}
              onSuccess={this.onSendDisbursement}
            />
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  profile: state.profileReducer.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getProfile, addQuizTipsLifes, getDisbursement}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Profile),
);
