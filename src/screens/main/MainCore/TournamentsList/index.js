import React, {Component} from 'react';
import {
  ImageBackground,
  View,
  Text,
  Platform,
  RefreshControl,
} from 'react-native';
import moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import Toast from 'react-native-simple-toast';
import RadialGradient from 'react-native-radial-gradient';
import {CommonActions} from '@react-navigation/native';
import {
  getTournamentsList,
  getTournamentQuestion,
  joinToTournament,
  leaveTournament,
  cleareOldData,
} from '../../../../actions/tournamentsActions';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {navigate} from '../../../../navigator/RootNavigation';
import {
  Item,
  ModalYear,
  ModalConfirm,
  ModalParticipants,
  ModalEditProfile,
} from './components';
import {Container, Preloader} from '../../../../components';
import {BACKGROUND_1_IMAGE} from '../../../../constants/Images';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import styles from './styles';

class TournamentsListScreen extends Component {
  state = {
    showModal: null,
    choosen_tournament: null,
    is_refresh: false,
  };

  componentDidMount() {
    this.props.getTournamentsList();
  }

  componentDidUpdate(prevProps) {
    const {is_request, join_to_tournament} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      if (this.state.is_refresh) {
        this.setState({is_refresh: false});
      }
    }
    if (
      prevProps.join_to_tournament !== join_to_tournament &&
      join_to_tournament
    ) {
      if (this.state.choosen_tournament.can_be_started) {
        this.props.cleareOldData();
        this.props.getTournamentQuestion(this.state.choosen_tournament.id);
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 2,
            routes: [
              {name: MainFlowKeys.Home, params: {disableStartRequest: true}},
              {name: MainFlowKeys.TournamentsList},
              {
                name: MainFlowKeys.QuestionTournament,
                params: {id: this.state.choosen_tournament.id},
              },
            ],
          }),
        );
      }
    }
  }

  onSuccess = () => {
    const {showModal} = this.state;

    this.setState({showModal: null}, () => {
      switch (showModal) {
        case 'year':
          setTimeout(
            () => {
              this.setState({showModal: 'confirmation'});
            },
            Platform.OS === 'android' ? 0 : 500,
          );
          break;
        case 'edit_profile':
          navigate(MainFlowKeys.ProfileEdit);
          break;
        case 'confirmation':
          this.props.joinToTournament(this.state.choosen_tournament.id);
          break;
        default:
          break;
      }
    });
  };

  onPressCard = (key, tournament) => {
    const {profile, t} = this.props;

    switch (key) {
      case 'participants':
        this.setState({
          showModal: key,
          choosen_tournament: tournament,
        });
        break;
      case 'leave':
        this.setState({
          choosen_tournament: tournament,
        });
        this.props.leaveTournament(tournament.id);
        break;
      case 'join':
        if (
          !profile.birthdate ||
          !profile.nickname ||
          !profile.first_name ||
          !profile.last_name
        ) {
          this.setState({showModal: 'edit_profile'});
        } else {
          if (moment().diff(moment(profile.birthdate), 'years') < 18) {
            return Toast.show(t('main_flow:over_18_years_old'), Toast.LONG);
          }
          this.setState({
            showModal: 'confirmation',
            choosen_tournament: tournament,
          });
        }
        break;
      case 'play':
        this.props.cleareOldData();
        this.props.getTournamentQuestion(tournament.id);
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 2,
            routes: [
              {name: MainFlowKeys.Home, params: {disableStartRequest: true}},
              {name: MainFlowKeys.TournamentsList},
              {
                name: MainFlowKeys.QuestionTournament,
                params: {id: tournament.id},
              },
            ],
          }),
        );
        break;
      default:
        console.log(key);
        break;
    }
  };

  render() {
    const {t, is_request, tournaments_list, profile} = this.props;
    const {showModal, choosen_tournament, is_refresh} = this.state;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={['#DD01AD', '#2200AB']}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.flex}>
          <Container
            enableStatusBar={false}
            transparentBackground={true}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.content}
            headerProps={{
              title: t('main_flow:all_tournaments'),
              whiteBackBtn: true,
              backgroundStart: 'transparent',
              backgroundFinish: '#DD01AD',
            }}
            childrenComponentProps={{
              refreshControl: (
                <RefreshControl
                  refreshing={is_refresh}
                  onRefresh={() => {
                    this.setState({is_refresh: true});
                    this.props.getTournamentsList();
                  }}
                />
              ),
            }}>
            {is_request && !is_refresh && <Preloader />}
            <ModalYear
              isVisible={showModal === 'year'}
              onCancel={() => this.setState({showModal: null})}
              onSuccess={this.onSuccess}
            />
            <ModalConfirm
              isVisible={showModal === 'confirmation'}
              onCancel={() => this.setState({showModal: null})}
              onSuccess={this.onSuccess}
              subject={choosen_tournament && choosen_tournament.title}
              price={choosen_tournament && choosen_tournament.initial_bid}
            />
            <ModalParticipants
              isVisible={showModal === 'participants'}
              onCancel={() => this.setState({showModal: null})}
              onSuccess={this.onSuccess}
              tournament_id={choosen_tournament && choosen_tournament.id}
            />
            <ModalEditProfile
              isVisible={showModal === 'edit_profile'}
              onCancel={() => this.setState({showModal: null})}
              onSuccess={this.onSuccess}
              profile={profile}
            />
            {tournaments_list &&
            !(is_request && !is_refresh) &&
            tournaments_list.length ? (
              tournaments_list.map(item => (
                <Item
                  key={item.id}
                  {...item}
                  onPress={key => this.onPressCard(key, item)}
                />
              ))
            ) : (
              <View>
                <Text style={styles.emptyListText}>
                  {t('main_flow:no_tournaments_available')}
                </Text>
              </View>
            )}
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.tournamentsReducer.is_request,
  tournaments_list: state.tournamentsReducer.tournaments_list,
  join_to_tournament: state.tournamentsReducer.join_to_tournament,
  profile: state.profileReducer.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTournamentsList,
      getTournamentQuestion,
      joinToTournament,
      leaveTournament,
      cleareOldData,
    },
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TournamentsListScreen),
);
