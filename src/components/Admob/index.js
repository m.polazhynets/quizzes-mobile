import React, {useEffect} from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import {
  RewardedAd,
  RewardedAdEventType,
  TestIds,
} from '@react-native-firebase/admob';

const adUnitId = __DEV__ ? TestIds.REWARDED : TestIds.REWARDED;

const AdmobComponent = ({
  onFinishedWatching,
  onCanceled = () => {},
  onLoaded = () => {},
}) => {
  useEffect(() => {
    const rewarded = RewardedAd.createForAdRequest(adUnitId, {
      requestNonPersonalizedAdsOnly: true,
      keywords: ['fashion'],
    });

    const eventListener = rewarded.onAdEvent((type, error, reward) => {
      console.log('type', type);
      if (type === RewardedAdEventType.LOADED) {
        onLoaded(rewarded);
      }

      if (type === RewardedAdEventType.EARNED_REWARD) {
        onFinishedWatching();
        rewarded.load();
      }

      if (type === 'closed') {
        onCanceled();
        rewarded.load();
      }

      if (error) {
        onCanceled();
        console.log(error);
      }
    });
    // Start loading the rewarded ad straight away
    rewarded.load();

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  },[]);

  return <View />;
};

AdmobComponent.propTypes = {
  onFinishedWatching: PropTypes.func.isRequired,
  onCanceled: PropTypes.func,
  onLoaded: PropTypes.func,
};

export default AdmobComponent;
