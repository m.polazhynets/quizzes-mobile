import {StyleSheet, I18nManager} from 'react-native';
import {
  COLOR_DARK,
  COLOR_GRAY_5,
  COLOR_RED_1,
} from '../../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_MEDIUM,
} from '../../../../../../constants/StylesConstants';

const {small, regular} = variables.fontSize;

export default StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(18),
    paddingRight: scale(24),
  },
  name: {
    fontFamily: FONT_MEDIUM,
    fontSize: regular,
    color: COLOR_DARK,
    lineHeight: v_scale(22),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_GRAY_5,
    lineHeight: v_scale(22),
  },
  imageWrapper: {
    width: scale(52),
    height: scale(52),
    backgroundColor: 'rgba(234, 234, 234, 0.5)',
    borderRadius: scale(30),
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(16),
  },
  fullAvatar: {
    width: '100%',
    height: '100%',
  },
  avatar: {
    width: scale(26),
    height: scale(26),
  },
  leftSide: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightSide: {
    flex: 1,
    alignItems: 'flex-end',
  },
  badge: {
    marginTop: v_scale(5),
  },
  swipeButton: {
    paddingVertical: scale(8),
    height: scale(60),
    justifyContent: 'center',
    alignItems: 'center',
  },
  background_red: {
    backgroundColor: COLOR_RED_1,
  },
  swipeIcon: {
    width: scale(22),
    height: scale(22),
  },
  rightContainer: {
    width: scale(60),
    flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
    alignItems: 'center',
  },
  emptyBlock: {
    height: v_scale(25),
  },
});
