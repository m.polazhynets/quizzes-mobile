import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import {COLOR_WHITE} from '../../../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
} from '../../../../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../../../../components/Header/styles';

const {mediumRegular} = variables.fontSize;

export default StyleSheet.create({
  header: {
    height: isIphoneX()
      ? HEADER_MIN_HEIGHT - getStatusBarHeight() - v_scale(14)
      : HEADER_MIN_HEIGHT - getStatusBarHeight(),
    paddingHorizontal: scale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  arrowIcon: {
    width: scale(30),
    height: v_scale(20),
  },
  menuButton: {
    paddingLeft: scale(14),
    height: HEADER_MIN_HEIGHT,
    justifyContent: 'center',
    width: scale(40),
  },
  rightSide: {
    flexDirection: 'row',
  },
  starIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  questionIcon: {
    height: v_scale(20),
    width: scale(20),
  },
  starButton: {
    height: HEADER_MIN_HEIGHT,
    justifyContent: 'center',
    paddingRight: scale(8),
    paddingLeft: scale(14),
  },
  questionButton: {
    height: HEADER_MIN_HEIGHT,
    justifyContent: 'center',
    paddingRight: scale(14),
    paddingLeft: scale(3),
  },
  timerBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: scale(8),
    paddingVertical: v_scale(3),
    borderWidth: scale(2),
    borderColor: COLOR_WHITE,
    borderRadius: scale(100),
    minWidth: scale(90),
  },
  timerIcon: {
    width: scale(20),
    height: v_scale(23),
    marginRight: scale(4),
  },
  timerText: {
    //fontFamily: FONT_MEDIUM,
    fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace',
    fontSize: mediumRegular,
    color: COLOR_WHITE,
  },
});
