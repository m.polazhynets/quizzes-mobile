import _ from 'underscore';
import moment from 'moment';
import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {CHAT} from '../constants/ActionTypes';

export default injectReducer(initialState.chatReducer, {
  [CHAT.GET_CHATS_LIST]: state => ({
    ...state,
    is_request: true,
    chat_id: null,
  }),
  [CHAT.GET_CHATS_LIST_SUCCESS]: (
    state,
    {payload: {total_pages, results}, additionalPayload: {page}},
  ) => {
    let chats_list = [];

    if (page === 1) {
      chats_list = results;
    } else if (state.chats_list) {
      chats_list = [...state.chats_list, ...results];
    } else {
      chats_list = results;
    }

    let not_read_count = chats_list.reduce((a, b) => a + b.not_read_count, 0);

    return {
      ...state,
      chats_list,
      is_request: false,
      not_read_count,
      chats_filter_params: {
        ...state.chats_filter_params,
        page,
        total_pages,
        last_page: page === total_pages,
      },
    };
  },
  [CHAT.GET_CHATS_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [CHAT.DELETE_CHAT]: state => ({
    ...state,
    is_request: true,
  }),
  [CHAT.DELETE_CHAT_SUCCESS]: (state, {additionalPayload: {id}}) => {
    let chats_list = [];
    _.each(state.chats_list, item => {
      if (item.id !== id) {
        chats_list.push(item);
      }
    });
    let not_read_count = chats_list.reduce((a, b) => a + b.not_read_count, 0);

    return {
      ...state,
      chats_list,
      not_read_count,
      is_request: false,
    };
  },
  [CHAT.DELETE_CHAT_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [CHAT.GET_HISTORY_CHAT]: state => ({
    ...state,
    is_request: true,
  }),
  [CHAT.GET_HISTORY_CHAT_SUCCESS]: (
    state,
    {payload: {total_pages, histories}, additionalPayload: {id, page}},
  ) => {
    let chats_list = [];

    _.each(state.chats_list, item => {
      if (item.id !== id) {
        chats_list.push(item);
      } else {
        chats_list.push({...item, not_read_count: 0});
      }
    });

    let not_read_count = chats_list.reduce((a, b) => a + b.not_read_count, 0);

    return {
      ...state,
      messages:
        page === 1
          ? histories
          : state.messages
          ? [...state.messages, ...histories]
          : histories,
      is_request: false,
      chat_id: id,
      not_read_count,
      chats_list,
      messages_filter_params: {
        ...state.messages_filter_params,
        page,
        total_pages,
        last_page: page === total_pages,
      },
    };
  },
  [CHAT.GET_HISTORY_CHAT_FAILURE]: (state, {additionalPayload: {id}}) => ({
    ...state,
    messages: [],
    chat_id: id,
    is_request: false,
  }),

  [CHAT.SEND_MESSAGE]: state => ({
    ...state,
    is_request: true,
  }),
  [CHAT.SEND_MESSAGE_SUCCESS]: (
    state,
    {additionalPayload: {user_id, message, chat_id}},
  ) => {
    let chats_list = [];

    _.each(state.chats_list, item => {
      if (item.id === chat_id) {
        chats_list.push({
          ...item,
          last_message: {
            id: Math.round(Math.random() * 1000),
            created: moment().format('YYYY-MM-DD HH:mm:ss'),
            text: message,
            owner: true,
          },
        });
      } else {
        chats_list.push(item);
      }
    });

    chats_list.sort(
      (a, b) =>
        moment(b.last_message.created).valueOf() -
        moment(a.last_message.created).valueOf(),
    );

    const messages = [
      {
        id: Math.round(Math.random() * 1000),
        created: moment().format('YYYY-MM-DD HH:mm:ss'),
        text: message,
        owner: true,
      },
      ...state.messages,
    ];

    return {
      ...state,
      messages,
      chats_list,
      is_request: false,
    };
  },
  [CHAT.SEND_MESSAGE_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [CHAT.GET_NEW_MESSAGE]: (
    state,
    {
      payload: {
        data: {chat_id, message},
      },
    },
  ) => {
    let chats_list = null;
    let messages = state.messages ? [...state.messages] : null;

    if (state.chats_list) {
      chats_list = [];
      _.each(state.chats_list, item => {
        if (+item.id === +chat_id) {
          chats_list.push({
            ...item,
            last_message: JSON.parse(message),
          });
        } else {
          chats_list.push(item);
        }
      });
      chats_list.sort(
        (a, b) =>
          moment(b.last_message.created).valueOf() -
          moment(a.last_message.created).valueOf(),
      );
    }

    if (+state.chat_id === +chat_id && messages) {
      messages.unshift(JSON.parse(message));
    }

    return {
      ...state,
      chats_list,
      messages,
    };
  },

  [CHAT.REMOVE_CURRENT_CHAT_STORE]: state => ({
    ...state,
    chat_id: null,
  }),
});
