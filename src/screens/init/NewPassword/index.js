import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from 'i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {CommonActions} from '@react-navigation/native';
import {AuthFlowKeys} from '../../../navigator/Keys';
import {resetPassword} from '../../../actions/authActions';
import {Button, Container, Input} from '../../../components';
import {validatePassword} from '../../../utils/Helper';
import styles from './styles';

class NewPasswordScreen extends Component {
  state = {
    email: this.props.route.params.email,
    form: {
      password: {
        value: '',
        isValid: null,
        error: '',
      },
      confirm_password: {
        value: '',
        isValid: null,
        error: i18n.t('common:errors.password_do_not_match'),
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {password_reset, navigation} = this.props;

    if (prevProps.password_reset !== password_reset && password_reset) {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            {name: AuthFlowKeys.ChooseAuth, params: {headerEnabled: true}},
            {name: AuthFlowKeys.Login},
          ],
        }),
      );
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'password':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val),
            },
            confirm_password: {
              ...form.confirm_password,
              isValid: form.confirm_password.value
                ? validatePassword(form.confirm_password.value, val)
                : null,
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val, form.password.value),
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {password, confirm_password},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            password: {
              ...password,
              isValid: password.isValid === true,
            },
            confirm_password: {
              ...confirm_password,
              isValid: confirm_password.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.password.isValid && form.confirm_password.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      email,
      form: {password},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.resetPassword(email, password.value);
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {password, confirm_password},
    } = this.state;

    return (
      <Container
        headerProps={{
          title: t('auth_flow:password_recovery'),
        }}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View style={styles.form}>
            <Input
              label={t('common:form.password')}
              value={password.value}
              required
              secureTextEntry
              isValid={password.isValid}
              onBlur={() => this.validate(password.value, 'password')}
              onChangeText={val => this.validate(val, 'password')}
            />
            <Input
              label={t('common:form.repeat_password')}
              value={confirm_password.value}
              required
              secureTextEntry
              isValid={confirm_password.isValid}
              onBlur={() =>
                this.validate(confirm_password.value, 'confirm_password')
              }
              onChangeText={val => this.validate(val, 'confirm_password')}
              errorMessage={
                confirm_password.value ? confirm_password.error : ''
              }
            />
          </View>
          <View style={styles.bottomContent}>
            <Button
              contentContainerStyle={styles.button}
              title={t('common:buttons.save')}
              type="red"
              isLoading={is_request}
              onPress={this.handleSubmit}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.authReducer.is_request,
  password_reset: state.authReducer.password_reset,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({resetPassword}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(NewPasswordScreen),
);
