import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {AUTH} from '../constants/ActionTypes';

export default injectReducer(initialState.authReducer, {
  [AUTH.LOG_OUT_SUCCESS]: () => ({
    ...initialState.authReducer,
  }),

  [AUTH.GET_CONFIRMATION_CODE]: state => ({
    ...state,
    code_send: null,
    is_request: true,
  }),
  [AUTH.GET_CONFIRMATION_CODE_SUCCESS]: state => ({
    ...state,
    code_send: true,
    is_request: false,
  }),
  [AUTH.GET_CONFIRMATION_CODE_FAILURE]: state => ({
    ...state,
    code_send: false,
    is_request: false,
  }),

  [AUTH.VERIFY_CONFIRMATION_CODE]: state => ({
    ...state,
    code_confirmed: null,
    is_request: true,
  }),
  [AUTH.VERIFY_CONFIRMATION_CODE_SUCCESS]: state => ({
    ...state,
    code_confirmed: true,
    is_request: false,
  }),
  [AUTH.VERIFY_CONFIRMATION_CODE_FAILURE]: state => ({
    ...state,
    code_confirmed: false,
    is_request: false,
  }),

  [AUTH.SING_UP]: state => ({
    ...state,
    is_request: true,
  }),
  [AUTH.SING_UP_SUCCESS]: (state, {payload: {access, refresh}}) => ({
    ...state,
    access_token: access,
    refresh_token: refresh,
    is_request: false,
  }),
  [AUTH.SING_UP_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [AUTH.LOG_IN]: state => ({
    ...state,
    is_request: true,
  }),
  [AUTH.LOG_IN_SUCCESS]: (state, {payload: {access, refresh}}) => ({
    ...state,
    access_token: access,
    refresh_token: refresh,
    is_request: false,
  }),
  [AUTH.LOG_IN_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [AUTH.SOCIAL_LOG_IN]: state => ({
    ...state,
    is_request: true,
  }),
  [AUTH.SOCIAL_LOG_IN_SUCCESS]: (state, {payload: {access, refresh}}) => ({
    ...state,
    access_token: access,
    refresh_token: refresh,
    is_request: false,
  }),
  [AUTH.SOCIAL_LOG_IN_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [AUTH.RESET_PASSWORD]: state => ({
    ...state,
    is_request: true,
    password_reset: null,
  }),
  [AUTH.RESET_PASSWORD_SUCCESS]: state => ({
    ...state,
    password_reset: true,
    is_request: false,
  }),
  [AUTH.RESET_PASSWORD_FAILURE]: state => ({
    ...state,
    password_reset: false,
    is_request: false,
  }),

  [AUTH.CHANGE_PASSWORD]: state => ({
    ...state,
    is_request: true,
    password_reset: null,
  }),
  [AUTH.CHANGE_PASSWORD_SUCCESS]: state => ({
    ...state,
    password_reset: true,
    is_request: false,
  }),
  [AUTH.CHANGE_PASSWORD_FAILURE]: state => ({
    ...state,
    password_reset: false,
    is_request: false,
  }),

  [AUTH.REFRESH_TOKEN]: state => ({
    ...state,
  }),
  [AUTH.REFRESH_TOKEN_SUCCESS]: (state, {payload: {access, refresh}}) => ({
    ...state,
    access_token: access,
    refresh_token: refresh,
  }),
  [AUTH.REFRESH_TOKEN_FAILURE]: state => ({
    ...state,
    access_token: null,
    refresh_token: null,
  }),

  [AUTH.SET_FCM_TOKEN_SUCCESS]: (
    state,
    {additionalPayload: {notification_token}},
  ) => ({
    ...state,
    fcm_token: notification_token,
  }),

  [AUTH.REMOVE_FCM_TOKEN_SUCCESS]: state => ({
    ...state,
    fcm_token: null,
  }),
});
