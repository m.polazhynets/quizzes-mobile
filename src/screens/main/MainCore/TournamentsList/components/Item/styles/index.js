import {StyleSheet} from 'react-native';
import {
  COLOR_DARK,
  COLOR_WHITE,
  COLOR_GRAY_0,
  COLOR_GRAY_5,
} from '../../../../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../../../../constants/StylesConstants';

const {mediumRegular, regular, small} = variables.fontSize;

export default StyleSheet.create({
  button: {
    width: scale(160),
    marginTop: v_scale(8),
  },
  itemContent: {
    //flex: 1,
    backgroundColor: COLOR_WHITE,
    paddingVertical: scale(20),
    paddingHorizontal: scale(24),
    borderRadius: scale(10),
    borderWidth: scale(2),
    borderColor: COLOR_GRAY_0,
    marginBottom: v_scale(20),
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: mediumRegular,
    color: COLOR_DARK,
    marginBottom: v_scale(5),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_DARK,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: v_scale(10),
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: scale(22),
    height: scale(22),
    marginRight: scale(10),
  },
  link: {
    width: 'auto',
  },
  linkText: {
    textAlign: 'left',
  },
  label: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_GRAY_5,
  },
  textBold: {
    fontFamily: FONT_BOLD,
    fontSize: mediumRegular,
    color: COLOR_DARK,
    marginTop: v_scale(3),
  },
  noMargin: {
    marginVertical: 0,
  },
  regular: {
    fontSize: regular,
  },
  alignStart: {
    alignItems: 'flex-start',
  },
});
