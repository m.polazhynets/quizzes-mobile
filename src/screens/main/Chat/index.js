import React, {Component} from 'react';
import {View, TouchableOpacity, Image, BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _ from 'underscore';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import 'moment/locale/ru';
import {GiftedChat, Bubble, Time} from 'react-native-gifted-chat';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getHistoryChat,
  sendMessage,
  removeCurrentChatStore,
  makeChatRead,
} from '../../../actions/chatActions';
import {Preloader} from '../../../components';
import Header from './components/Header';
import {SEND_ACTIVE_ICON, SEND_ICON} from '../../../constants/Images';
import styles from './styles';

class Chat extends Component {
  state = {
    user: this.props.route.params.user,
    chat_id: this.props.route.params.id,
    text: '',
  };

  componentDidMount() {
    this.getData(1);
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.willGoBack,
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  willGoBack = () => {
    this.props.removeCurrentChatStore();
    this.props.makeChatRead(this.state.chat_id);
  };

  renderMessages = list => {
    const {profile} = this.props;
    const {user} = this.state;
    let result = [];

    _.each(list, item => {
      result.push({
        _id: item.id,
        text: item.text,
        createdAt: item.created,
        user: {
          _id: item.owner ? profile.id : user.user_id,
          name: item.owner ? profile.nickname : user.nickname,
          avatar: item.owner ? profile.photo : user.photo,
        },
      });
    });

    return result;
  };

  getData = page => {
    const {filter_params, is_request} = this.props;
    const {chat_id} = this.state;

    if (is_request) {
      return;
    }

    this.props.getHistoryChat(chat_id, page || filter_params.page + 1);
  };

  onSend = text => {
    const {user, chat_id} = this.state;

    this.props.sendMessage(user.user_id, text, chat_id, res => {
      if (res) {
        this.setState({text: ''});
      }
    });
  };

  renderTime = ({currentMessage, timeFormat}) => {
    return (
      <Time
        currentMessage={currentMessage}
        timeFormat={timeFormat}
        timeTextStyle={{
          right: styles.timeText,
          left: styles.timeText,
        }}
      />
    );
  };

  renderBubble = props => {
    return (
      <Bubble
        {...props}
        textStyle={{
          left: styles.leftBubblesText,
          right: styles.rightBubblesText,
        }}
        wrapperStyle={{
          left: styles.leftBubbles,
          right: styles.rightBubbles,
        }}
      />
    );
  };

  renderSend = props => {
    return (
      <TouchableOpacity
        style={styles.sendButton}
        onPress={() => {
          if (props.text.length) {
            this.onSend(props.text);
          }
        }}
        activeOpacity={0.5}>
        <Image
          source={props.text.length ? SEND_ACTIVE_ICON : SEND_ICON}
          resizeMode="contain"
          style={styles.sendIcon}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {
      t,
      is_request,
      stored_chat_id,
      profile,
      messages,
      filter_params,
    } = this.props;
    const {text, user, chat_id} = this.state;

    return (
      <View style={styles.wrapper}>
        <SafeAreaView style={styles.content}>
          <Header data={user} onPressBack={this.willGoBack} />
          {is_request && <Preloader />}
          <KeyboardAwareScrollView
            contentContainerStyle={styles.flexGrow}
            scrollEnabled={false}
            keyboardShouldPersistTaps="handle">
            {chat_id === stored_chat_id && (
              <GiftedChat
                listViewProps={{
                  style: styles.scrollContentStyles,
                }}
                textInputStyle={styles.textInputStyle}
                messages={this.renderMessages(messages)}
                user={{
                  _id: profile.id,
                }}
                text={text}
                onInputTextChanged={massage => this.setState({text: massage})}
                renderAvatar={null}
                placeholder={t('main_flow:write_message')}
                alwaysShowSend
                multiline
                scrollToBottomOffset={0}
                timeFormat="HH:mm"
                locale="ru"
                renderSend={this.renderSend}
                renderBubble={this.renderBubble}
                renderTime={this.renderTime}
                onLoadEarlier={this.getData}
                loadEarlier={!filter_params.last_page}
                label={t('common:texts.load_earlier_messages')}
                infiniteScroll
              />
            )}
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.chatReducer.is_request,
  profile: state.profileReducer.profile,
  messages: state.chatReducer.messages,
  stored_chat_id: state.chatReducer.chat_id,
  filter_params: state.chatReducer.messages_filter_params,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {getHistoryChat, sendMessage, removeCurrentChatStore, makeChatRead},
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Chat),
);
