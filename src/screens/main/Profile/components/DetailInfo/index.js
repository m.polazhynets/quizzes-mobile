import React, {Component} from 'react';
import {View, Image, Text, Alert} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from '../../../../../utils/i18n';
import {addTournamentTipsLifes} from '../../../../../actions/profileActions';
import {ButtonLink, ListItem} from '../../../../../components';
import {TipsAndLifesModal} from '../index';
import {navigate} from '../../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../../navigator/Keys';
import {
  DOCUMENT_IMAGE,
  LAMP_YELLOW_ICON,
  LAMP_BACKGROUND_IMAGE,
  HEART_ICON,
  HEART_BACKGROUND_IMAGE,
} from '../../../../../constants/Images';
import styles from './styles';

class DatailBlock extends Component {
  state = {
    showModal: null,
    dialogData: null,
  };
  listTournament = [
    {
      key: '0',
      link: 'Statistics',
      title: i18n.t('main_flow:tournament_statistics'),
    },
    {
      key: '1',
      link: 'Standings',
      title: i18n.t('main_flow:standings'),
    },
    {
      key: '2',
      link: 'FAQs',
      title: i18n.t('main_flow:faq_tournament'),
    },
  ];
  listQuiz = [
    {
      key: '0',
      link: 'Statistics',
      title: i18n.t('main_flow:quiz_statistics'),
    },
  ];

  showPay = (type, price) => {
    Alert.alert(
      'Оплата',
      `Ви уверени что хотите купить ${
        type === 'tooltip' ? '1 подсказку' : '1 жизнь'
      } за ${price} Р`,
      [
        {
          text: 'Нет',
          style: 'cancel',
        },
        {
          text: 'Да',
          onPress: () => this.props.addTournamentTipsLifes(type, price),
        },
      ],
      {cancelable: false},
    );
  };

  onGetMore = type => {
    const {t, isTournament, profile, showVideo} = this.props;

    this.setState({
      showModal: 'dialog',
      dialogData: {
        title: t(`main_flow:${type === 'tooltip' ? 'get_hint' : 'get_life'}`),
        buttons: [
          {
            title: t('common:buttons.buy'),
            onPress: () => {
              if (isTournament) {
                this.showPay(
                  type,
                  type === 'tooltip'
                    ? profile.tournament_tooltip_cost
                    : profile.tournament_life_cost,
                );
              } else {
                navigate(MainFlowKeys.PurchaseDonate);
              }
              this.closeDialogModal();
            },
            type: 'red',
          },
          !isTournament && {
            title: t('main_flow:watch_video'),
            onPress: () => {
              showVideo(type);
              this.closeDialogModal();
            },
            type: 'yellow',
          },
        ],
        links: [
          {
            title: t('common:texts.cancel'),
            onPress: () => {
              this.closeDialogModal();
            },
          },
        ],
      },
    });
  };

  closeDialogModal = () => {
    this.setState({showModal: null, dialogData: null});
  };

  render() {
    const {t, isTournament, profile, shodDisbursementModal} = this.props;
    const {showModal, dialogData} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.topBlock}>
          <View style={styles.line} />
          <Text style={styles.smallTitle}>
            {isTournament ? t('main_flow:tournament') : t('main_flow:quiz')}
          </Text>
          <View style={styles.row}>
            <Text style={styles.text}>
              {isTournament
                ? t('main_flow:overall_tournament_rating')
                : t('main_flow:overall_quiz_rating')}
            </Text>
            <Text
              style={[
                styles.title,
                isTournament ? styles.colorYellow : styles.colorGreen,
              ]}>
              {(isTournament ? profile.points : profile.free_quiz_points) || 0}
            </Text>
          </View>
        </View>
        {isTournament && (
          <React.Fragment>
            <View style={styles.personalAccountBlock}>
              <Image
                source={DOCUMENT_IMAGE}
                resizeMode={'contain'}
                style={styles.accountBackground}
              />
              <View style={styles.partAccount}>
                <Text style={styles.smallTitle}>
                  {t('main_flow:personal_account')}
                </Text>
                <Text style={[styles.title, styles.noMargin]}>{`${
                  profile.wallet.amount
                } P`}</Text>
              </View>
              <View style={styles.partAccount}>
                <ButtonLink
                  contentContainerStyle={styles.alignEnd}
                  title={t('common:buttons.replenish')}
                  onPress={() => {}}
                />
                <ButtonLink
                  contentContainerStyle={styles.alignEnd}
                  title={t('common:buttons.order_payment')}
                  onPress={shodDisbursementModal}
                />
              </View>
            </View>
            <View style={styles.transactionBlock}>
              <ButtonLink
                contentContainerStyle={styles.alignStart}
                title={t('common:buttons.transaction_history')}
                onPress={() => navigate(MainFlowKeys.TransactionHistory)}
              />
            </View>
          </React.Fragment>
        )}
        <View style={styles.gridBlock}>
          <View style={styles.part}>
            <View style={styles.grid}>
              <Image
                source={LAMP_YELLOW_ICON}
                resizeMode="contain"
                style={styles.gridIcon}
              />
              <Image
                source={LAMP_BACKGROUND_IMAGE}
                resizeMode={'contain'}
                style={styles.gridBackground}
              />
              <Text style={styles.gridText}>
                {t('main_flow:number_of_hints')}
              </Text>
              <Text style={[styles.title, styles.noMargin]}>
                {isTournament
                  ? profile.tournament_tooltips
                  : profile.free_tooltips}
              </Text>
            </View>
            <ButtonLink
              contentContainerStyle={[styles.alignStart, styles.paddingLeft]}
              title={t('common:buttons.get_hint')}
              onPress={() => this.onGetMore('tooltip')}
            />
          </View>
          <View style={styles.part}>
            <View style={styles.grid}>
              <Image
                source={HEART_ICON}
                resizeMode="contain"
                style={styles.gridIcon}
              />
              <Image
                source={HEART_BACKGROUND_IMAGE}
                resizeMode={'contain'}
                style={styles.gridBackground}
              />
              <Text style={styles.gridText}>
                {t('main_flow:number_of_lives')}
              </Text>
              <Text style={[styles.title, styles.noMargin]}>
                {isTournament ? profile.tournament_life : profile.free_life}
              </Text>
            </View>
            <ButtonLink
              contentContainerStyle={[styles.alignStart, styles.paddingLeft]}
              title={t('common:buttons.get_life')}
              onPress={() => this.onGetMore('life')}
            />
          </View>
        </View>
        <View style={styles.list}>
          {(isTournament ? this.listTournament : this.listQuiz).map(item => (
            <ListItem
              {...item}
              noBorder
              isArrow
              contentContainerStyle={styles.listItem}
              onPress={() => {
                if (item.link === 'FAQs') {
                  return navigate({
                    name: MainFlowKeys.FAQs,
                    key: 'tournament',
                  });
                }
                if (item.link === 'Statistics') {
                  return navigate({
                    name: MainFlowKeys.Statistics,
                    params: {
                      isTournament,
                    },
                  });
                }
                if (item.link) {
                  navigate(MainFlowKeys[item.link]);
                }
              }}
            />
          ))}
        </View>
        <TipsAndLifesModal
          isVisible={showModal === 'dialog'}
          data={dialogData}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profileReducer.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({addTournamentTipsLifes}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(DatailBlock),
);
