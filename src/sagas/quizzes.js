/**
 * @module Sagas/Quizzes
 * @desc Quizzes
 */

import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {QUIZZES, ERRORS} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getQuizzesList() {
  try {
    const response = yield call(request, 'quizzes/', {
      method: 'GET',
    });
    yield put({
      type: QUIZZES.GET_QUIZZES_LIST_SUCCESS,
      payload: response,
    });
    return response;
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: QUIZZES.GET_QUIZZES_LIST,
          payload: {},
          params: null,
        },
      });
    } else {
      yield put({
        type: QUIZZES.GET_QUIZZES_LIST_FAILURE,
        payload: err.response,
      });
      return null;
    }
  }
}

export function* getQuestionsList({payload: {id}}) {
  try {
    const response = yield call(request, `quizzes/${id}/questions/`, {
      method: 'GET',
    });
    yield put({
      type: QUIZZES.GET_QUESTIONS_LIST_QUIZZES_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: QUIZZES.GET_QUESTIONS_QUIZZES_LIST,
          payload: {id},
          params: null,
        },
      });
    } else {
      yield put({
        type: QUIZZES.GET_QUESTIONS_QUIZZES_LIST_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* sendResultQuiz({payload: {id, data, callback}}) {
  try {
    const response = yield call(request, `quizzes/${id}/save_result/`, {
      method: 'POST',
      payload: data,
    });
    yield put({
      type: QUIZZES.SEND_RESULT_QUIZ_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
    callback();
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: QUIZZES.SEND_RESULT_QUIZ,
          payload: {id, data, callback},
          params: null,
        },
      });
    } else {
      yield put({
        type: QUIZZES.SEND_RESULT_QUIZ_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* getDonatesList() {
  try {
    const response = yield call(request, 'price_packages/', {
      method: 'GET',
    });
    yield put({
      type: QUIZZES.GET_DONATES_LIST_SUCCESS,
      payload: response,
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: QUIZZES.GET_DONATES_LIST,
        },
      });
    } else {
      yield put({
        type: QUIZZES.GET_DONATES_LIST_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

/**
 * Quizzes Sagas
 */
export default function* root() {
  yield all([takeLatest(QUIZZES.GET_QUIZZES_LIST, getQuizzesList)]);
  yield all([takeLatest(QUIZZES.GET_QUESTIONS_LIST_QUIZZES, getQuestionsList)]);
  yield all([takeLatest(QUIZZES.SEND_RESULT_QUIZ, sendResultQuiz)]);
  yield all([takeLatest(QUIZZES.GET_DONATES_LIST, getDonatesList)]);
}
