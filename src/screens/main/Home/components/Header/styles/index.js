import {StyleSheet} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';
import {scale, v_scale} from '../../../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../../../components/Header/styles';

export default StyleSheet.create({
  header: {
    height: isIphoneX()
      ? HEADER_MIN_HEIGHT - getStatusBarHeight() - v_scale(14)
      : HEADER_MIN_HEIGHT - getStatusBarHeight(),
    paddingHorizontal: scale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  menuIcon: {
    height: v_scale(10),
    width: scale(28),
  },
  menuButton: {
    paddingLeft: scale(14),
    height: HEADER_MIN_HEIGHT,
    justifyContent: 'center',
    width: scale(50),
  },
});
