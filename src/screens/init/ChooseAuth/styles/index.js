import {StyleSheet} from 'react-native';
import {
  scale,
  deviceHeight,
  v_scale,
} from '../../../../constants/StylesConstants';

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    height: '100%',
    maxHeight: deviceHeight * 0.5,
  },
  imageWrapper: {
    flex: 1,
  },
  link: {
    marginTop: v_scale(16),
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(270),
  },
  facebookButton: {
    width: scale(270),
    height: v_scale(36),
  },
  googleButton: {
    width: scale(280),
    marginVertical: v_scale(15),
  },
  flex: {
    flex: 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
});
