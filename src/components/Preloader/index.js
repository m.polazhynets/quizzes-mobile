import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {COLOR_ROSE_DARKEN} from '../../constants/Colors';
import styles from './styles';

export default ({containerStyles = {}}) => {
  return (
    <View style={[styles.container, containerStyles]}>
      <ActivityIndicator size="large" color={COLOR_ROSE_DARKEN} />
    </View>
  );
};
