import {StyleSheet} from 'react-native';
import {v_scale, scale} from '../../../../../../../constants/StylesConstants';

export default StyleSheet.create({
  vieoWrapper: {
    marginTop: v_scale(16),
    marginHorizontal: -scale(8),
    justifyContent: 'center',
    alignItems: 'center',
    height: v_scale(220),
  },
});
