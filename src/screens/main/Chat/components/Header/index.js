import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image, View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {goBack} from '../../../../../navigator/RootNavigation';
import {
  ARROW_BACK_WHITE_ICON,
  PROFILE_ICON,
} from '../../../../../constants/Images';
import {
  COLOR_VIOLET_1,
  COLOR_VIOLET_1_DARKEN,
} from '../../../../../constants/Colors';
import styles from './styles';

const VIOLET = [COLOR_VIOLET_1, COLOR_VIOLET_1_DARKEN];

export default class Header extends Component {
  render() {
    const {data, onPressBack = () => {}} = this.props;

    return (
      <View style={styles.headerWrapper}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 1, y: 0}}
          colors={VIOLET}
          style={styles.headerContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.backButton}
            onPress={() => {
              goBack();
              onPressBack();
            }}>
            <Image
              source={ARROW_BACK_WHITE_ICON}
              style={styles.arrowIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.headerContent}>
            <View style={styles.imageWrapper}>
              <Image
                source={data.photo ? {uri: data.photo} : PROFILE_ICON}
                resizeMode={data.photo ? 'cover' : 'contain'}
                style={data.photo ? styles.fullAvatar : styles.avatar}
              />
            </View>
            <Text style={styles.nicname}>{data.nickname}</Text>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

Header.propTypes = {
  headerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  position: PropTypes.string,
  rating: PropTypes.string,
};
