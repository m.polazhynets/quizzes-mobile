import {StyleSheet, Platform} from 'react-native';
import {isIphoneX, getStatusBarHeight} from 'react-native-iphone-x-helper';
import {
  COLOR_GRAY_5,
  COLOR_GRAY_2,
  COLOR_DARK,
} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
  FONT_BOLD,
} from '../../../../constants/StylesConstants';
import {HEADER_MIN_HEIGHT} from '../../../../components/Header/styles';

const {regular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: scale(10),
  },
  text: {
    color: COLOR_GRAY_5,
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    lineHeight: v_scale(22),
    marginBottom: v_scale(10),
  },
  headerIcon: {
    height: v_scale(20),
    width: scale(27),
  },
  headerButton: {
    position: 'absolute',
    top:
      Platform.OS === 'ios' && isIphoneX()
        ? getStatusBarHeight() + v_scale(14)
        : getStatusBarHeight(),
    right: scale(10),
    height: HEADER_MIN_HEIGHT,
    paddingTop: scale(15),
    paddingRight: scale(14),
    paddingLeft: scale(3),
    opacity: 0.2,
  },
  activeButton: {
    opacity: 1,
  },
  avatarWrapper: {
    backgroundColor: COLOR_GRAY_2,
    width: scale(134),
    height: scale(134),
    borderRadius: scale(134),
    justifyContent: 'center',
    alignContent: 'center',
    overflow: 'hidden',
    marginBottom: v_scale(10),
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  imageContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(260),
  },
  form: {
    paddingTop: v_scale(16),
  },
  labelBlock: {
    paddingTop: v_scale(20),
    paddingBottom: v_scale(14),
  },
  label: {
    fontFamily: FONT_BOLD,
    fontSize: regular,
    color: COLOR_DARK,
  },
});
