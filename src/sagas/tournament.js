/**
 * @module Sagas/Tournament
 * @desc Tournament
 */

import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {TOURNAMENT, ERRORS, GENERAL} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getTournamentsList() {
  try {
    const response = yield call(request, 'tournaments/', {
      method: 'GET',
    });
    yield put({
      type: TOURNAMENT.GET_TOURNAMENTS_LIST_SUCCESS,
      payload: response,
    });
    return response;
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.GET_TOURNAMENTS_LIST,
          payload: {},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.GET_TOURNAMENTS_LIST_FAILURE,
        payload: err.response,
      });
      return null;
    }
  }
}

export function* getTournamentParticipantsList({payload: {id}}) {
  try {
    const response = yield call(request, `tournaments/${id}/participants/`, {
      method: 'GET',
    });
    yield put({
      type: TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST,
          payload: {id},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

export function* joinToTournament({payload: {id}}) {
  try {
    const response = yield call(request, `tournaments/${id}/join/`, {
      method: 'POST',
      payload: {},
    });
    yield put({
      type: TOURNAMENT.JOIN_TO_TOURNAMENT_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.JOIN_TO_TOURNAMENT,
          payload: {id},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.JOIN_TO_TOURNAMENT_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* leaveTournament({payload: {id}}) {
  try {
    const response = yield call(request, `tournaments/${id}/disconnect/`, {
      method: 'POST',
      payload: {},
    });
    yield put({
      type: TOURNAMENT.LEAVE_TOURNAMENT_SUCCESS,
      payload: response,
      additionalPayload: {id},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.LEAVE_TOURNAMENT,
          payload: {id},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.LEAVE_TOURNAMENT_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* getTournamentQuestion({payload: {id, callback}}) {
  try {
    const response = yield call(request, `tournaments/${id}/question/`, {
      method: 'GET',
    });
    yield put({
      type: TOURNAMENT.GET_TOURNAMENT_QUESTION_SUCCESS,
      payload: response,
    });
    if (callback) {
      callback(true);
    }
  } catch (err) {
    console.log(err);
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.GET_TOURNAMENT_QUESTION,
          payload: {id, callback},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.GET_TOURNAMENT_QUESTION_FAILURE,
        payload: err.response,
      });
      if (callback) {
        callback(false);
      }
      if (
        err.response.data &&
        err.response.data.errors &&
        err.response.data.errors.life
      ) {
        yield put({
          type: GENERAL.TOOGLE_DIALOG_MODAL,
          payload: {show: 'main', type_key: null},
        });
      }
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
      }
    }
  }
}

export function* getCurrentTooltip({payload: {id, callback = () => {}}}) {
  try {
    const response = yield call(request, `tournaments/${id}/get_tooltip/`, {
      method: 'POST',
      payload: {},
    });
    yield put({
      type: TOURNAMENT.GET_CURRENT_TOOLTIP_SUCCESS,
      payload: response,
    });
    callback(true);
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.GET_CURRENT_TOOLTIP,
          payload: {id, callback},
          params: null,
        },
      });
    } else {
      if (
        err.response.data &&
        err.response.data.errors &&
        err.response.data.errors.time_out
      ) {
        yield put({
          type: GENERAL.TOOGLE_DIALOG_MODAL,
          payload: {show: 'main', type_key: 'time_out'},
        });
      }
      yield put({
        type: TOURNAMENT.GET_CURRENT_TOOLTIP_FAILURE,
        payload: err.response,
      });
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
        callback(false);
      }
    }
  }
}

export function* sendAnswer({payload: {id, data, callback}}) {
  try {
    const response = yield call(request, `tournaments/${id}/question/`, {
      method: 'POST',
      payload: data,
    });
    yield put({
      type: TOURNAMENT.SEND_ANSWER_SUCCESS,
      payload: response,
    });
    callback(response.question ? 'next' : 'finish');
  } catch (err) {
    console.log(err);
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: TOURNAMENT.SEND_ANSWER,
          payload: {id, data, callback},
          params: null,
        },
      });
    } else {
      yield put({
        type: TOURNAMENT.SEND_ANSWER_FAILURE,
        payload: err.response,
      });
      if (
        err.response.data &&
        err.response.data.errors &&
        err.response.data.errors.life
      ) {
        yield put({
          type: GENERAL.TOOGLE_DIALOG_MODAL,
          payload: {show: 'children', type_key: null},
        });
      }
      if (err.response.data) {
        yield put({
          type: ERRORS.SET_ERROR,
          payload: err.response.data,
        });
        callback(false);
      }
    }
  }
}

/**
 * Tournaments Sagas
 */
export default function* root() {
  yield all([takeLatest(TOURNAMENT.GET_TOURNAMENTS_LIST, getTournamentsList)]);
  yield all([
    takeLatest(
      TOURNAMENT.GET_TOURNAMENT_PARTICIPANTS_LIST,
      getTournamentParticipantsList,
    ),
  ]);
  yield all([takeLatest(TOURNAMENT.JOIN_TO_TOURNAMENT, joinToTournament)]);
  yield all([takeLatest(TOURNAMENT.LEAVE_TOURNAMENT, leaveTournament)]);
  yield all([
    takeLatest(TOURNAMENT.GET_TOURNAMENT_QUESTION, getTournamentQuestion),
  ]);
  yield all([takeLatest(TOURNAMENT.GET_CURRENT_TOOLTIP, getCurrentTooltip)]);
  yield all([takeLatest(TOURNAMENT.SEND_ANSWER, sendAnswer)]);
}
