import React from 'react';
import Modal from 'react-native-modal';
import {Text, ImageBackground} from 'react-native';
import i18n from 'i18next';
import {Button, ButtonLink} from '../../../../../../components';
import {BACKGROUND_2_IMAGE} from '../../../../../../constants/Images';
import styles from './styles';

export default ({isVisible, onSuccess, onCancel, subject, price}) => {
  return (
    <Modal isVisible={isVisible} useNativeDriver>
      <ImageBackground source={BACKGROUND_2_IMAGE} style={styles.modalContent}>
        <Text style={styles.title}>
          {i18n.t('main_flow:confirmation_of_participation')}
        </Text>
        <Text style={styles.text}>{i18n.t('main_flow:tournament')}:</Text>
        <Text style={[styles.text, styles.bold]}>{subject}</Text>
        <Text style={styles.text}>
          {i18n.t('main_flow:cost_of_participation')}:
        </Text>
        <Text style={[styles.text, styles.bold]}>{price} P</Text>
        <Button
          contentContainerStyle={styles.button}
          title={i18n.t('common:buttons.pay')}
          type="yellow"
          onPress={onSuccess}
        />
        <ButtonLink title={i18n.t('common:texts.cancel')} onPress={onCancel} />
      </ImageBackground>
    </Modal>
  );
};
