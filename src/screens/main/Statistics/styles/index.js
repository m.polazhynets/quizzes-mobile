import {StyleSheet} from 'react-native';
import {COLOR_GRAY_0, COLOR_DARK} from '../../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';

const {small, large} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: v_scale(10),
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(18),
    borderBottomWidth: scale(1),
    borderBottomColor: COLOR_GRAY_0,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  label: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    color: COLOR_DARK,
  },
  value: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    color: COLOR_DARK,
  },
});
