import React, {Component} from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getRatingList} from '../../../actions/profileActions';
import {Preloader} from '../../../components';
import Header from './components/Header';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {PROFILE_ICON} from '../../../constants/Images';
import {COLOR_ROSE_DARKEN} from '../../../constants/Colors';
import styles from './styles';

class StandingsScreen extends Component {
  header = React.createRef();
  state = {
    refreshing: false,
    firstLoading: false,
  };

  componentDidMount() {
    this.getData(false, 1);
  }

  componentDidUpdate(prevProps) {
    const {is_request} = this.props;

    if (prevProps.is_request !== is_request && !is_request) {
      this.setState({refreshing: false, firstLoading: false});
    }
  }

  getData = (refreshing, page) => {
    const {rating_filter_params, is_request} = this.props;

    if (is_request) {
      return;
    }

    this.setState({refreshing, firstLoading: !refreshing}, () => {
      this.props.getRatingList(page || rating_filter_params.page + 1);
    });
  };

  renderItem = ({item, index}) => {
    const {rating_list} = this.props;

    return (
      <View
        style={[
          styles.item,
          rating_list.length - 1 === index && styles.noBorder,
        ]}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.leftSide}
          onPress={() => navigate(MainFlowKeys.User, {id: item.id})}>
          <Text style={[styles.headerTitle, styles.black]}>
            {item.position}
          </Text>
          <View style={styles.itemAvatarWrapper}>
            <Image
              style={item.photo ? styles.itemAvatarLarge : styles.itemAvatar}
              source={item.photo ? {uri: item.photo} : PROFILE_ICON}
              resizeMode={item.photo ? 'cover' : 'contain'}
            />
          </View>
          <Text style={styles.tableText}>{item.nickname}</Text>
        </TouchableOpacity>
        <View style={styles.rightSide}>
          <Text style={styles.tableText}>{item.points}</Text>
          {/* <TouchableOpacity onPress={() => {}} style={styles.moreButton}>
            <Image
              source={MORE_ICON}
              style={styles.moreIcon}
              resizeMode="contain"
            />
          </TouchableOpacity> */}
        </View>
      </View>
    );
  };

  renderListHeaderComponent = () => {
    const {t} = this.props;

    return (
      <React.Fragment>
        <Text style={styles.title}>{t('main_flow:tournament_grid')}</Text>
        <View style={styles.headerTable}>
          <View style={styles.leftSide}>
            <Text style={styles.headerTitle}>{t('main_flow:users')}</Text>
          </View>
          <View style={styles.rightSide}>
            <Text style={styles.headerTitle}>{t('main_flow:rating')}</Text>
          </View>
        </View>
      </React.Fragment>
    );
  };

  renderFooter = () => {
    if (
      !this.props.is_request ||
      !this.props.rating_list ||
      !this.props.rating_list.length
    ) {
      return null;
    }

    if (this.state.firstLoading || this.state.refreshing) {
      return null;
    }

    return (
      <View>
        <ActivityIndicator animating size="large" color={COLOR_ROSE_DARKEN} />
      </View>
    );
  };

  renderEmptyComponent = () => {
    const {t} = this.props;

    return <Text style={styles.emptyText}>{t('common:texts.list_empty')}</Text>;
  };

  render() {
    const {rating_my_profile, rating_list, rating_filter_params} = this.props;
    const {refreshing, firstLoading} = this.state;

    return (
      <View style={styles.wrapper}>
        <SafeAreaView style={styles.content}>
          <Header ref={this.header} data={rating_my_profile} />
          {firstLoading && <Preloader />}
          <View style={styles.emptyBlock} />
          <FlatList
            style={styles.zIndex}
            contentContainerStyle={styles.scrollContentStyles}
            ListHeaderComponent={this.renderListHeaderComponent}
            ListEmptyComponent={this.renderEmptyComponent}
            ListFooterComponent={this.renderFooter}
            keyExtractor={item => `${item.id}`}
            data={rating_list}
            renderItem={this.renderItem}
            refreshing={refreshing}
            onRefresh={() => this.getData(true, 1)}
            onEndReached={() => {
              if (!rating_filter_params.last_page) {
                this.getData(false);
              }
            }}
            onScroll={e => this.header.current.onScroll(e)}
            scrollEventThrottle={0.6}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  rating_my_profile: state.profileReducer.rating_my_profile,
  rating_list: state.profileReducer.rating_list,
  rating_filter_params: state.profileReducer.rating_filter_params,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getRatingList}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(StandingsScreen),
);
