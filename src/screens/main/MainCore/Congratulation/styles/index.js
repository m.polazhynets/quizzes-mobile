import {StyleSheet} from 'react-native';
import variable, {
  scale,
  v_scale,
  FONT_MEDIUM,
} from '../../../../../constants/StylesConstants';
import {COLOR_WHITE} from '../../../../../constants/Colors';

const {extraLarge} = variable.fontSize;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scrollContentStyles: {
    padding: scale(24),
    paddingTop: v_scale(10),
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
  content: {
    paddingHorizontal: scale(24),
    paddingVertical: v_scale(45),
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  topContent: {
    alignItems: 'center',
  },
  button: {
    width: scale(260),
  },
  title: {
    fontFamily: FONT_MEDIUM,
    fontSize: extraLarge,
    color: COLOR_WHITE,
    textAlign: 'center',
    marginTop: v_scale(40),
  },
  image: {
    marginTop: v_scale(80),
    width: scale(230),
    height: scale(230),
  },
});
