import React, {Component} from 'react';
import {ScrollView, Text} from 'react-native';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import {Container} from '../../../../components';
import styles from './styles';

class TermsParticipationScreen extends Component {
  state = {
    content: this.props.route.params ? this.props.route.params.content : '',
  };

  render() {
    const {t} = this.props;
    const {content} = this.state;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:terms_of_participation'),
        }}>
        <ScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <Text style={styles.text}>{content}</Text>
        </ScrollView>
      </Container>
    );
  }
}

export default withTranslation(['common', 'main_flow'])(
  connect(
    null,
    null,
  )(TermsParticipationScreen),
);
