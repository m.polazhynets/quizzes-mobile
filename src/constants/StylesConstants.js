import {Dimensions, Platform} from 'react-native';
const {width: screenWidth, height: screenHeight} = Dimensions.get('window');
const localWidth = screenWidth >= screenHeight ? screenHeight : screenWidth;
const localHeight = screenWidth < screenHeight ? screenHeight : screenWidth;
const widthCoef = (localWidth > 375 ? 375 : localWidth) / 375;
const heightCoef = (localHeight > 812 ? 812 : localHeight) / 812;

export const scale = size => widthCoef * size;
export const v_scale = size => heightCoef * size;
export const deviceWidth = screenWidth;
export const deviceHeight = screenHeight;

const variables = {
  fontSize: {
    smaller: scale(10),
    small: scale(12),

    regular: scale(14),
    mainRegular: scale(16),
    mediumRegular: scale(18),

    large: scale(20),
    extraLarge: scale(30),
  },
  shadow: {
    shadowColor: '#333',
    shadowOffset: {width: 0, height: 10},
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: Platform.OS === 'ios' ? 5 : 3,
  },
};

export const FONT_BOLD = 'Montserrat-Bold';
export const FONT_MEDIUM = 'Montserrat-Medium';
export const FONT_REGULAR = 'Montserrat-Regular';
export const FONT_LIGHT = 'Montserrat-Light';

export default variables;
