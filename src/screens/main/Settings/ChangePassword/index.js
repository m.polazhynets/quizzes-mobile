import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from 'i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {changePassword} from '../../../../actions/authActions';
import {toggleCustomToast} from '../../../../actions/settingsActions';
import {Container, Input, Preloader} from '../../../../components';
import {CHECK_BLACK_ICON} from '../../../../constants/Images';
import {validatePassword} from '../../../../utils/Helper';
import styles from './styles';

class ChangePasswordScreen extends Component {
  state = {
    form: {
      password_old: {
        value: '',
        isValid: null,
        error: '',
      },
      password_new: {
        value: '',
        isValid: null,
        error: '',
      },
      confirm_password: {
        value: '',
        isValid: null,
        error: i18n.t('common:errors.password_do_not_match'),
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {password_reset} = this.props;

    if (prevProps.password_reset !== password_reset && password_reset) {
      this.props.toggleCustomToast(true);
      this.clearForm();
    }
  }

  clearForm = () => {
    this.setState({
      form: {
        password_old: {
          value: '',
          isValid: null,
          error: '',
        },
        password_new: {
          value: '',
          isValid: null,
          error: '',
        },
        confirm_password: {
          value: '',
          isValid: null,
          error: i18n.t('common:errors.password_do_not_match'),
        },
      },
    });
  };

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'password_new':
        this.setState({
          form: {
            ...form,
            password_new: {
              ...form.password_new,
              value: val,
              isValid:
                validatePassword(val) &&
                !validatePassword(val, form.password_old.value),
            },
            confirm_password: {
              ...form.confirm_password,
              isValid: form.confirm_password.value
                ? validatePassword(form.confirm_password.value, val)
                : null,
            },
          },
        });
        break;
      case 'confirm_password':
        this.setState({
          form: {
            ...form,
            confirm_password: {
              ...form.confirm_password,
              value: val,
              isValid:
                validatePassword(val, form.password_new.value) &&
                !validatePassword(val, form.password_old.value),
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val),
            },
            password_new: {
              ...form.password_new,
              isValid: form.password_new.value
                ? validatePassword(
                    form.confirm_password.value,
                    form.password_new.value,
                  ) && !validatePassword(val, form.password_new.value)
                : form.password_new.isValid,
            },
            confirm_password: {
              ...form.confirm_password,
              isValid: form.confirm_password.value
                ? validatePassword(
                    form.confirm_password.value,
                    form.password_new.value,
                  ) && !validatePassword(val, form.confirm_password.value)
                : form.confirm_password.isValid,
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {password_old, password_new, confirm_password},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            password_old: {
              ...password_old,
              isValid: password_old.isValid === true,
            },
            password_new: {
              ...password_new,
              isValid: password_new.isValid === true,
            },
            confirm_password: {
              ...confirm_password,
              isValid: confirm_password.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.password_old.isValid &&
          form.password_new.isValid &&
          form.confirm_password.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {password_old, password_new},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.changePassword(password_old.value, password_new.value);
    } catch (err) {
      console.log('error validation');
    }
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this.handleSubmit}
        style={[styles.headerButton, styles.activeButton]}>
        <Image
          source={CHECK_BLACK_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {password_old, password_new, confirm_password},
    } = this.state;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:change_password'),
          rightBtn: this.renderHeaderButton(),
        }}>
        {is_request && <Preloader />}
        <KeyboardAwareScrollView
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="handled">
          <View>
            <Input
              label={t('common:form.old_password')}
              value={password_old.value}
              required
              secureTextEntry
              isValid={password_old.isValid}
              onBlur={() => this.validate(password_old.value, 'password_old')}
              onChangeText={val => this.validate(val, 'password_old')}
            />
            <Input
              label={t('common:form.new_password')}
              value={password_new.value}
              required
              secureTextEntry
              isValid={password_new.isValid}
              onBlur={() => this.validate(password_new.value, 'password_new')}
              onChangeText={val => this.validate(val, 'password_new')}
            />
            <Input
              label={t('common:form.repeat_new_password')}
              value={confirm_password.value}
              required
              secureTextEntry
              isValid={confirm_password.isValid}
              onBlur={() =>
                this.validate(confirm_password.value, 'confirm_password')
              }
              onChangeText={val => this.validate(val, 'confirm_password')}
              errorMessage={
                confirm_password.value && !password_old.value
                  ? confirm_password.error
                  : ''
              }
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.authReducer.is_request,
  password_reset: state.authReducer.password_reset,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({changePassword, toggleCustomToast}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChangePasswordScreen),
);
