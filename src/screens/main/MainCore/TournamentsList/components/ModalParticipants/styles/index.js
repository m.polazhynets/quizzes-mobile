import {StyleSheet} from 'react-native';
import variables, {
  v_scale,
  scale,
  FONT_BOLD,
  FONT_REGULAR,
  deviceHeight,
} from '../../../../../../../constants/StylesConstants';
import {
  COLOR_WHITE,
  COLOR_GRAY_0,
  COLOR_DARK,
  COLOR_BLACK,
} from '../../../../../../../constants/Colors';

const {large, regular, small} = variables.fontSize;

export default StyleSheet.create({
  modalContent: {
    backgroundColor: COLOR_WHITE,
    borderWidth: scale(2),
    borderColor: COLOR_GRAY_0,
    borderRadius: scale(10),
    justifyContent: 'center',
    paddingVertical: scale(25),
    paddingBottom: v_scale(30),
    maxHeight: deviceHeight * 0.8,
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    textAlign: 'center',
    color: COLOR_DARK,
    marginBottom: v_scale(8),
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingVertical: v_scale(10),
    alignItems: 'center',
    borderBottomWidth: scale(1),
    borderColor: COLOR_GRAY_0,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  leftSide: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemAvatarWrapper: {
    width: scale(36),
    height: scale(36),
    backgroundColor: COLOR_GRAY_0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(18),
    marginLeft: scale(10),
    marginRight: scale(12),
    overflow: 'hidden',
  },
  itemAvatar: {
    width: scale(16),
    height: scale(16),
  },
  photo: {
    width: '100%',
    height: '100%',
  },
  itemText: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    color: COLOR_DARK,
  },
  itemTitle: {
    fontFamily: FONT_BOLD,
    fontSize: small,
    color: COLOR_DARK,
  },
  list: {
    width: '100%',
    paddingHorizontal: scale(25),
    paddingTop: v_scale(10),
  },
  avatarIcon: {
    width: scale(16),
    height: scale(16),
  },
  emptyListText: {
    fontSize: regular,
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    textAlign: 'center',
  },
});
