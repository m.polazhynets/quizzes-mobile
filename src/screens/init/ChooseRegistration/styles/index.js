import {StyleSheet} from 'react-native';
import {
  COLOR_GRAY_2,
  COLOR_DARK,
  COLOR_YELLOW_2,
} from '../../../../constants/Colors';
import variables, {
  scale,
  deviceHeight,
  v_scale,
  FONT_REGULAR,
} from '../../../../constants/StylesConstants';

const {mainRegular} = variables.fontSize;

export default StyleSheet.create({
  content: {
    flexGrow: 1,
    padding: scale(24),
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    height: '100%',
    maxHeight: deviceHeight * 0.5,
  },
  imageWrapper: {
    flex: 1,
  },
  link: {
    marginTop: v_scale(16),
  },
  bottomContent: {
    alignItems: 'center',
  },
  button: {
    borderWidth: scale(1),
    borderColor: COLOR_GRAY_2,
    borderRadius: scale(10),
    height: scale(60),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: v_scale(20),
    backgroundColor: COLOR_YELLOW_2,
  },
  buttonText: {
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    color: COLOR_DARK,
  },
  fb_icon: {
    width: scale(15),
    height: scale(32),
    marginRight: scale(16),
  },
  google_icon: {
    width: scale(46),
    height: scale(28),
    marginRight: scale(16),
  },
  flex: {
    flex: 1,
  },
  transparentStyle: {
    backgroundColor: 'transparent',
  },
});
