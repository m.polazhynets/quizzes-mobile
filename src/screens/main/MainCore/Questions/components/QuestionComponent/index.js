import React, {Component} from 'react';
import {View, Text, Image, Alert} from 'react-native';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {toogleDialogModal} from '../../../../../../actions/generalActions';
import {getTournamentQuestion} from '../../../../../../actions/tournamentsActions';
import {addTournamentTipsLifes} from '../../../../../../actions/profileActions';
import {
  ItemVariant,
  DialogModal,
  AudioComponent,
  VideoComponent,
} from '../index';

import styles from './styles';

class QuestionComponent extends Component {
  state = {
    answer: null,
    playMediaFiles: true,
  };

  async componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const {data} = this.props;

    if (prevProps.data !== data && data) {
      if (prevProps.data.id !== data.id) {
        this.setState({answer: null}, () => this.init());
      }
    }
  }

  init = async () => {
    const {data, startTimer} = this.props;
    if (data.question_type === 'text' || data.question_type === 'image') {
      setTimeout(() => {
        startTimer(data.time);
      }, 100);
    }
  };

  setAnswer = order_num => {
    const {onAnswered} = this.props;
    const {answer} = this.state;

    if (answer) {
      return null;
    }

    this.setState({answer: order_num, playMediaFiles: false});
    onAnswered(order_num);
  };

  showModal = () => {
    this.props.togglePause(true);
    this.props.toogleDialogModal('children');
  };

  closeDialogModal = () => {
    this.props.toogleDialogModal(null);
    this.props.togglePause(false);
  };

  showPay = (type, price, callback = () => {}) => {
    Alert.alert(
      'Оплата',
      `Ви уверени что хотите купить ${
        type === 'tooltip' ? '1 подсказку' : '1 жизнь'
      } за ${price} Р`,
      [
        {
          text: 'Нет',
          style: 'cancel',
          onPress: () => callback(false),
        },
        {
          text: 'Да',
          onPress: () =>
            this.props.addTournamentTipsLifes(type, price, callback),
        },
      ],
      {cancelable: false},
    );
  };

  // *** DialogModal functions
  onBuyPress = type => {
    const {tournament_data, tournament_id} = this.props;

    this.showPay('life', tournament_data.life_cost, res => {
      if (res) {
        this.props.getTournamentQuestion(tournament_id, resp => {
          if (resp) {
            this.closeDialogModal();
          }
        });
      }
    });
  };

  // ***

  render() {
    const {answer, playMediaFiles} = this.state;
    const {
      data,
      showTooltip,
      startTimer,
      tooglePause,
      show_dialog_modal,
      life,
      tooltips,
      current_tooltip,
      truest_answer,
      onExitPress,
    } = this.props;

    return (
      <View style={styles.content}>
        <Text style={styles.titleQuestion}>{data.text}</Text>
        {data.link && (
          <VideoComponent
            link={data.link}
            play={playMediaFiles}
            startTimer={() => startTimer(data.time)}
          />
        )}
        {data.audio && (
          <AudioComponent
            link={data.audio}
            play={playMediaFiles}
            startTimer={() => startTimer(data.time)}
          />
        )}
        {data.image && (
          <View style={styles.imageWrapper}>
            <Image
              source={{uri: data.image}}
              style={styles.image}
              resizeMode="contain"
            />
          </View>
        )}
        <View style={styles.listWrapper}>
          {data.answers.map(item => (
            <ItemVariant
              {...item}
              key={item.order_num}
              onPress={this.setAnswer}
              iGuessed={answer === item.order_num && truest_answer}
              isFailed={answer === item.order_num && truest_answer === false}
              showTooltip={
                showTooltip &&
                current_tooltip &&
                item.order_num === current_tooltip
              }
            />
          ))}
        </View>
        <DialogModal
          isVisible={show_dialog_modal === 'children'}
          type_key="tournament"
          onBuyPress={this.onBuyPress}
          onExitPress={onExitPress}
          life={life}
          tooltips={tooltips}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  truest_answer: state.tournamentsReducer.answer,
  tournament_data: state.tournamentsReducer.tournament_data,
  current_tooltip: state.tournamentsReducer.current_tooltip,
  life: state.tournamentsReducer.life,
  tooltips: state.tournamentsReducer.tooltips,
  show_dialog_modal: state.generalReducer.show_dialog_modal,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toogleDialogModal,
      addTournamentTipsLifes,
      getTournamentQuestion,
    },
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(QuestionComponent),
);
