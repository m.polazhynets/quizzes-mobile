import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import i18n from 'i18next';
import moment from 'moment';
import _ from 'underscore';
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';
import {getCountriesList, getCitiesList} from '../../../actions/generalActions';
import {toggleCustomToast} from '../../../actions/settingsActions';
import {updateProfile} from '../../../actions/profileActions';
import {
  Container,
  ButtonLink,
  Input,
  DateTimePicker,
  Preloader,
} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {CHECK_BLACK_ICON, PROFILE_ICON} from '../../../constants/Images';
import styles from './styles';

function renderForm(profile) {
  let form = {};

  _.each(profile, (item, key) => {
    if (key === 'country_name' || key === 'city_name') {
      const _key = key === 'country_name' ? 'country' : 'city';
      form[_key] = {
        value:
          form[_key] && form[_key].value
            ? {id: form[_key].value.id, name: item}
            : {name: item},
        isValid: item ? true : null,
        error: '',
      };
    } else if (key === 'country' || key === 'city') {
      form[key] = {
        value:
          form[key] && form[key].value
            ? {name: form[key].value.name, id: item}
            : {id: item},
        isValid: item ? true : null,
        error: '',
      };
    } else {
      form[key] = {
        value: item,
        isValid: item ? true : null,
        error: '',
      };
    }
  });
  return form;
}

class ProfileEditScreen extends Component {
  state = {
    form: renderForm(this.props.profile),
  };
  options = {
    title: i18n.t('common:texts.choose_photo'),
    cancelButtonTitle: i18n.t('common:texts.cancel'),
    takePhotoButtonTitle: i18n.t('common:texts.take_photo'),
    chooseFromLibraryButtonTitle: i18n.t('common:texts.choose_from_gallery'),
    noData: true,
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  componentDidUpdate(prevProps) {
    const {is_updated, navigation} = this.props;

    if (prevProps.is_updated !== is_updated && is_updated) {
      this.props.toggleCustomToast(true);
      navigation.goBack();
    }
  }

  openImagePickerCheck = () => {
    if (Platform.OS === 'ios') {
      this.openImagePicker();
    } else {
      this.requestCameraPermission();
    }
  };

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      ]);
      if (
        granted[PermissionsAndroid.PERMISSIONS.CAMERA] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        this.openImagePicker();
      } else {
        Toast.show('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  openImagePicker = () => {
    ImagePicker.showImagePicker(this.options, async response => {
      let rotation = 0;
      const {originalRotation} = response;
      //Determining rotation param
      if (originalRotation === 90) {
        rotation = 90;
      } else if (originalRotation === 180) {
        //For a few images rotation is 180
        rotation = -180;
      } else if (originalRotation === 270) {
        //When taking images with the front camera (selfie), the rotation is 270.
        rotation = -90;
      }
      if (response.uri) {
        // resize image
        const resizedImageUri = await ImageResizer.createResizedImage(
          `${response.uri}`,
          600,
          600,
          'JPEG',
          50,
          rotation,
        );
        const filePath =
          Platform.OS === 'android' && resizedImageUri.uri.replace
            ? resizedImageUri.uri.replace('file:/data', '/data')
            : resizedImageUri.uri;
        const photoData = await RNFS.readFile(filePath, 'base64');
        this.validate(photoData, 'photo');
      }
    });
  };

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'birthdate':
      case 'city':
      case 'photo':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: val ? true : false,
            },
          },
        });
        break;
      case 'country':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: val ? true : false,
            },
            city: {
              ...form.city,
              value: null,
              isValid: form.city.value ? false : null,
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: val && val.length > 2 ? true : false,
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {nickname, last_name, first_name, birthdate, country, city},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            ...this.state.form,
            nickname: {
              ...nickname,
              isValid: nickname.isValid === true,
            },
            last_name: {
              ...last_name,
              isValid: last_name.isValid === true,
            },
            first_name: {
              ...first_name,
              isValid: first_name.isValid === true,
            },
            birthdate: {
              ...birthdate,
              isValid: birthdate.isValid === true,
            },
            country: {
              ...country,
              isValid: country.isValid === true,
            },
            city: {
              ...city,
              isValid: city.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.nickname.isValid &&
          form.last_name.isValid &&
          form.first_name.isValid &&
          form.birthdate.isValid &&
          form.country.isValid &&
          form.city.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {
        photo,
        nickname,
        last_name,
        first_name,
        middle_name,
        birthdate,
        country,
        city,
      },
    } = this.state;
    const {profile, navigation} = this.props;

    try {
      await this.checkFullValidate();
      let payload = {};
      const data = {
        photo: photo.value,
        nickname: nickname.value,
        last_name: last_name.value,
        first_name: first_name.value,
        middle_name: middle_name.value,
        birthdate: moment(birthdate.value).format('YYYY-MM-DD'),
        country: country.value.id,
        city: city.value.id,
      };

      _.each(data, (element, index) => {
        if (profile[index] !== element && element) {
          payload[index] = element;
        }
      });
      if (payload.photo) {
        payload.photo_name = 'my_photo.jpeg';
      }

      if (Object.entries(payload).length === 0) {
        navigation.goBack();
      } else {
        this.props.updateProfile(payload);
      }
    } catch (err) {
      console.log('error validation');
    }
  };

  renderHeaderButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this.handleSubmit}
        style={[styles.headerButton, styles.activeButton]}>
        <Image
          source={CHECK_BLACK_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, is_request} = this.props;
    const {
      form: {
        photo,
        nickname,
        last_name,
        first_name,
        middle_name,
        birthdate,
        country,
        city,
      },
    } = this.state;

    return (
      <Container
        headerProps={{
          title: t('main_flow:edit_profile'),
          rightBtn: this.renderHeaderButton(),
        }}>
        {is_request && <Preloader />}
        <View style={styles.content}>
          <View style={styles.imageContent}>
            <View style={styles.avatarWrapper}>
              <Image
                source={
                  photo.value
                    ? {
                        uri: /^(https?:\/\/)/.test(photo.value)
                          ? photo.value
                          : `data:image/jpeg;base64,${photo.value}`,
                      }
                    : PROFILE_ICON
                }
                resizeMode="cover"
                style={styles.avatar}
              />
            </View>
            <ButtonLink
              title={t('common:buttons.change_photo')}
              onPress={this.openImagePickerCheck}
            />
          </View>
          <View style={styles.form}>
            <Input
              label={t('common:form.nicname')}
              value={nickname.value}
              required
              isValid={nickname.isValid}
              onBlur={() => this.validate(nickname.value, 'nickname')}
              onChangeText={val => this.validate(val, 'nickname')}
            />
            <View style={styles.labelBlock}>
              <Text style={styles.label}>{t('main_flow:FIO')}</Text>
            </View>
            <Input
              label={t('common:form.last_name')}
              value={last_name.value}
              required
              isValid={last_name.isValid}
              onBlur={() => this.validate(last_name.value, 'last_name')}
              onChangeText={val => this.validate(val, 'last_name')}
            />
            <Input
              label={t('common:form.first_name')}
              value={first_name.value}
              required
              isValid={first_name.isValid}
              onBlur={() => this.validate(first_name.value, 'first_name')}
              onChangeText={val => this.validate(val, 'first_name')}
            />
            <Input
              label={t('common:form.middle_name')}
              value={middle_name.value}
              onBlur={() => this.validate(middle_name.value, 'middle_name')}
              onChangeText={val => this.validate(val, 'middle_name')}
            />
            <View style={styles.labelBlock}>
              <Text style={styles.label}>{t('main_flow:birthday')}</Text>
            </View>
            <DateTimePicker
              label={t('common:form.set_date')}
              value={birthdate.value}
              required
              isValid={birthdate.isValid}
              maximumDate={new Date()}
              onBlur={() => this.validate(birthdate.value, 'birthdate')}
              onChange={(e, val) => this.validate(val, 'birthdate')}
            />
            <View style={styles.labelBlock}>
              <Text style={styles.label}>{t('main_flow:location')}</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigate(MainFlowKeys.Autocomplete, {
                  headerTitle: t('main_flow:choose_country'),
                  action: this.props.getCountriesList,
                  onSelect: val => this.validate(val, 'country'),
                });
              }}
              activeOpacity={0.8}>
              <Input
                label={t('common:form.country')}
                editable={false}
                value={country.value ? country.value.name : null}
                required
                isValid={country.isValid}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigate(MainFlowKeys.Autocomplete, {
                  headerTitle: t('main_flow:choose_city'),
                  action: this.props.getCitiesList,
                  requestParams:
                    country.value && country.value.id ? [country.value.id] : [],
                  onSelect: val => this.validate(val, 'city'),
                });
              }}
              activeOpacity={0.8}>
              <Input
                label={t('common:form.city')}
                editable={false}
                value={city.value ? city.value.name : null}
                required
                isValid={city.isValid}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  profile: state.profileReducer.profile,
  is_updated: state.profileReducer.is_updated,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {getCountriesList, getCitiesList, updateProfile, toggleCustomToast},
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ProfileEditScreen),
);
