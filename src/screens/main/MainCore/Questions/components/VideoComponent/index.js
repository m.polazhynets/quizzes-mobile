import React, {Component} from 'react';
import {View} from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';
import {v_scale} from '../../../../../../constants/StylesConstants';
import styles from './styles';

export default class VideoComponent extends Component {
  render() {
    const {link, startTimer, play = true, tooglePlay = () => {}} = this.props;
    if (!link) {
      return null;
    }
    const videoId = link.substring(link.indexOf('v=') + 2, link.length);

    return (
      <View style={styles.vieoWrapper}>
        <YoutubePlayer
          play={play}
          height={v_scale(220)}
          width={'100%'}
          videoId={videoId}
          onChangeState={event => {
            if (event === 'paused' || event === 'ended') {
              startTimer();
            }
            if (event === 'playing') {
              tooglePlay(true);
            }
          }}
          onError={e => console.log(e)}
          onPlaybackQualityChange={q => console.log(q)}
          volume={50}
          playbackRate={1}
        />
      </View>
    );
  }
}
