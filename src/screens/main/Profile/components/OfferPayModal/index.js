import React, {Component} from 'react';
import {View, Text} from 'react-native';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import Modal from 'react-native-modal';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {withTranslation} from 'react-i18next';
import {Button, ButtonLink, Input} from '../../../../../components';
import {validateText} from '../../../../../utils/Helper';
import styles from './styles';

function validateMaxAmount(val, max, percent) {
  let res = false;
  if (val) {
    res = +val <= max - (max * percent) / 100;
  }

  return res;
}

class OfferPay extends Component {
  state = {
    form: {
      amount: {
        value: '',
        isValid: null,
        error: i18n.t('common:errors.incorrect_value'),
      },
      text: {
        value: '',
        isValid: null,
        error: i18n.t('common:errors.incorrect_value'),
      },
    },
  };

  componentDidUpdate(prevProps) {
    const {isVisible} = this.props;

    if (prevProps.isVisible !== isVisible) {
      this.setState({
        form: {
          amount: {
            value: '',
            isValid: null,
            error: i18n.t('common:errors.incorrect_value'),
          },
          text: {
            value: '',
            isValid: null,
            error: i18n.t('common:errors.incorrect_value'),
          },
        },
      });
    }
  }

  validate = (val, name) => {
    const {form} = this.state;

    switch (name) {
      case 'text':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateText(val, 10),
            },
          },
        });
        break;
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateMaxAmount(val, this.props.maxAmount, 10),
            },
          },
        });
        break;
    }
  };

  checkFullValidate = async () => {
    const {
      form: {amount, text},
    } = this.state;

    return new Promise((resolve, reject) => {
      this.setState(
        {
          form: {
            amount: {
              ...amount,
              isValid: amount.isValid === true,
            },
            text: {
              ...text,
              isValid: text.isValid === true,
            },
          },
        },
        () => {
          const {form} = this.state;

          form.amount.isValid && form.text.isValid
            ? resolve(true)
            : reject(false);
        },
      );
    });
  };

  handleSubmit = async () => {
    const {
      form: {amount, text},
    } = this.state;

    try {
      await this.checkFullValidate();

      this.props.onSuccess(amount.value, text.value);
    } catch (err) {
      console.log('error validation');
    }
  };

  render() {
    const {t, isVisible, onCancel} = this.props;
    const {
      form: {amount, text},
    } = this.state;

    return (
      <Modal isVisible={isVisible} useNativeDriver>
        <View style={styles.modalContent}>
          <Text style={styles.title}>{t('main_flow:order_payment')}</Text>
          <Text style={styles.text}>{t('main_flow:disbursement_text')}</Text>
          <KeyboardAwareScrollView
            contentContainerStyle={styles.form}
            keyboardShouldPersistTaps="handled">
            <Input
              label={t('common:form.amount')}
              value={amount.value}
              required
              isValid={amount.isValid}
              keyboardType="numeric"
              onBlur={() => this.validate(amount.value, 'amount')}
              onChangeText={val => this.validate(val, 'amount')}
              errorMessage={amount.error}
            />
            <Input
              label={t('common:form.getting_description')}
              value={text.value}
              required
              isValid={text.isValid}
              onBlur={() => this.validate(text.value, 'text')}
              onChangeText={val => this.validate(val, 'text')}
              errorMessage={amount.error}
            />
          </KeyboardAwareScrollView>
          <View style={styles.buttonsContent}>
            <Button
              type={'red'}
              contentContainerStyle={styles.button}
              title={t('common:buttons.order')}
              onPress={this.handleSubmit}
              margin
            />
            <ButtonLink
              contentContainerStyle={styles.link}
              title={t('common:texts.cancel')}
              onPress={onCancel}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

export default withTranslation(['common', 'main_flow'])(OfferPay);

OfferPay.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
  maxAmount: PropTypes.number.isRequired,
};
