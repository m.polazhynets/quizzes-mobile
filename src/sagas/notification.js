/**
 * @module Sagas/Notification
 * @desc Notification
 */

import {all, takeLatest, call, put} from 'redux-saga/effects';
import {request} from '../utils/Client';
import {NOTICATIONS} from '../constants/ActionTypes';
import {refreshToken} from './auth';

export function* getNotificationList({payload: {page}}) {
  try {
    const response = yield call(request, 'profile/notifications/', {
      method: 'GET',
      params: {page},
    });
    yield put({
      type: NOTICATIONS.GET_NOTIFICATION_LIST_SUCCESS,
      payload: response,
      additionalPayload: {page},
    });
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield refreshToken({
        payload: {
          type: NOTICATIONS.GET_NOTIFICATION_LIST,
          payload: {page},
        },
      });
    } else {
      yield put({
        type: NOTICATIONS.GET_NOTIFICATION_LIST_FAILURE,
        payload: err.response,
      });
    }
  }
}

/**
 * Notification Sagas
 */
export default function* root() {
  yield all([
    takeLatest(NOTICATIONS.GET_NOTIFICATION_LIST, getNotificationList),
  ]);
}
