import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image, Animated} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {RectButton} from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {hideNotification} from '../../actions/notificationActions';
import {APP_LOGO_ICON} from '../../constants/Images';
import {navigate} from '../../navigator/RootNavigation';
import {MainFlowKeys} from '../../navigator/Keys';

import styles from './styles';

class NotificationToast extends Component {
  componentDidUpdate(prevProps) {
    const {message} = this.props;

    if (prevProps.message !== message && message) {
      setTimeout(() => {
        this.props.hideNotification();
      }, 3000);
    }
  }

  renderSwipeActions = (progress, dragX) => {
    const trans = dragX.interpolate({
      inputRange: [0, 50, 100, 101],
      outputRange: [-20, 0, 0, 1],
    });
    return (
      <RectButton onPress={() => {}}>
        <Animated.Text
          style={[
            styles.actionText,
            {
              transform: [{translateX: trans}],
            },
          ]}>
          Archive
        </Animated.Text>
      </RectButton>
    );
  };

  render() {
    const {message} = this.props;

    if (!message) {
      return null;
    }

    return (
      <View style={styles.wrapper}>
        <Swipeable
          renderLeftActions={this.renderSwipeActions}
          renderRightActions={this.renderSwipeActions}
          onSwipeableWillOpen={this.props.hideNotification}>
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              navigate(MainFlowKeys.Notifications);
              this.props.hideNotification();
            }}
            activeOpacity={0.9}>
            <Image
              style={styles.icon}
              resizeMode="contain"
              source={APP_LOGO_ICON}
            />
            <View style={styles.content}>
              <Text style={styles.title} numberOfLines={1}>
                {message.title}
              </Text>
              <Text style={styles.text}>{message.body}</Text>
            </View>
          </TouchableOpacity>
        </Swipeable>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  message: state.notificationReducer.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({hideNotification}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationToast);
