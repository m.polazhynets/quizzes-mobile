import {StyleSheet} from 'react-native';
import {v_scale, scale} from '../../../../../../../constants/StylesConstants';

export default StyleSheet.create({
  audioWrapper: {
    marginTop: v_scale(16),
    height: v_scale(140),
    backgroundColor: '#4600AB',
    borderRadius: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  controlIcon: {
    width: scale(26),
    height: scale(26),
  },
  buttonControl: {
    width: scale(64),
    height: scale(64),
    borderRadius: scale(32),
    overflow: 'hidden',
  },
  buttonControlContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
