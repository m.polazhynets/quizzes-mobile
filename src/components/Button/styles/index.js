import {StyleSheet} from 'react-native';
import {
  COLOR_WHITE,
  COLOR_BLUE,
  COLOR_GRAY_3,
  COLOR_BLACK,
} from '../../../constants/Colors';
import variables, {
  scale,
  v_scale,
  FONT_BOLD,
} from '../../../constants/StylesConstants';

const {mainRegular} = variables.fontSize;

export default StyleSheet.create({
  buttonText: {
    textAlign: 'center',
    color: COLOR_WHITE,
    fontSize: mainRegular,
    fontFamily: FONT_BOLD,
  },
  disabledbuttonText: {
    color: COLOR_GRAY_3,
  },
  button: {
    position: 'relative',
    height: v_scale(60),
    backgroundColor: COLOR_WHITE,
    width: '100%',
    borderRadius: scale(100),
  },
  buttonContent: {
    borderRadius: scale(100),
    borderWidth: scale(2),
    borderColor: 'rgba(255, 255, 255, 0.15)',
    height: '100%',
    justifyContent: 'center',
  },
  buttomMargin: {
    marginBottom: v_scale(18),
  },
  button_yellow: {
    backgroundColor: COLOR_WHITE,
    borderColor: COLOR_BLUE,
  },
  buttonText_red: {
    color: COLOR_WHITE,
  },
  buttonText_yellow: {
    color: COLOR_BLACK,
  },
  buttonText_white: {
    color: COLOR_BLUE,
  },
  loaderWrap: {
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    top: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  hideText: {
    opacity: 0,
  },
  icon: {
    width: scale(30),
    height: v_scale(18),
    marginLeft: scale(12),
  },
  textWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
