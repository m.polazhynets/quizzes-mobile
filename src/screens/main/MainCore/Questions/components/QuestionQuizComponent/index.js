import React, {Component} from 'react';
import {View, Text, Image, Vibration} from 'react-native';
import Toast from 'react-native-simple-toast';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {changeCurrentQuestionDataQuizzes} from '../../../../../../actions/quizzesActions';
import {toogleDialogModal} from '../../../../../../actions/generalActions';
import {
  addQuizTipsLifes,
  removeQuizTipsLifes,
} from '../../../../../../actions/profileActions';
import {Admob} from '../../../../../../components';
import {navigate} from '../../../../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../../../../navigator/Keys';
import {
  ItemVariant,
  DialogModal,
  AudioComponent,
  VideoComponent,
} from '../index';

import styles from './styles';

class QuestionQuizComponent extends Component {
  state = {
    answer: null,
    playMediaFiles: true,
    watchAdvertsGoal: null,
    admob: null,
  };

  async componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const {data} = this.props;

    if (prevProps.data !== data && data) {
      if (prevProps.data.id !== data.id) {
        this.setState({answer: null}, () => this.init());
      }
    }
  }

  init = async () => {
    const {data, startTimer} = this.props;
    if (data.question_type === 'text' || data.question_type === 'image') {
      setTimeout(() => {
        startTimer(data.time);
      }, 0);
    }

    this.props.changeCurrentQuestionDataQuizzes({
      id: data.id,
      mistakes: 0,
      tooltips: 0,
      correct: null,
    });
  };

  setAnswer = order_num => {
    const {data, currentQuestion, onSuccessAnswered, life, t} = this.props;
    const {answer, admob} = this.state;

    if (answer) {
      return null;
    }

    const right = order_num === data.right_answer_num;

    this.props.changeCurrentQuestionDataQuizzes({
      ...currentQuestion,
      mistakes: right ? currentQuestion.mistakes : currentQuestion.mistakes + 1,
      correct: right,
    });

    this.setState({answer: order_num, playMediaFiles: false});
    if (!right) {
      Vibration.vibrate();
      if (currentQuestion.mistakes >= 1) {
        if (life > 0) {
          this.props.removeQuizTipsLifes('life');
        }
        this.props.togglePause(true);
        this.showModal();
      } else {
        setTimeout(() => {
          this.setState({answer: null});
          if (!admob) {
            return;
          }

          this.props.togglePause(true);
          admob.show();
        }, 500);
      }
    } else {
      onSuccessAnswered();
    }
  };

  showModal = () => {
    this.props.togglePause(true);
    this.props.toogleDialogModal('children');
  };

  closeDialogModal = () => {
    this.props.toogleDialogModal(null);
    this.props.togglePause(false);
  };

  tooglePlayMedia = val => {
    this.setState({playMediaFiles: val});
  };

  onFinishedWatching = () => {
    const {watchAdvertsGoal} = this.state;

    if (!watchAdvertsGoal) {
      this.props.togglePause(false);
      return;
    }

    this.props.addQuizTipsLifes(watchAdvertsGoal, 'advertising', 1);
  };

  // *** DialogModal functions
  onExitPress = () => {
    this.props.saveAnswers(false);
    this.closeDialogModal();
  };

  onTryOnePress = () => {
    this.setState({answer: null});
    this.closeDialogModal();
  };

  onBuyPress = () => {
    const {show_dialog_modal} = this.props;

    navigate(MainFlowKeys.PurchaseDonate, {
      dialogParams: {dialog_type_key: null, show_dialog_modal},
    });
    this.props.togglePause(true);
    this.setState({answer: null});
    this.closeDialogModal();
  };

  onWatchVideo = key => {
    this.setState({answer: null});
    this.setState({watchAdvertsGoal: key});
    this.admob?.current.init();
  };

  onUseTooltips = () => {
    this.setState({answer: null});
    this.props.onUseTooltips();
  };
  // ***

  render() {
    const {answer, playMediaFiles} = this.state;
    const {
      data,
      showTooltip,
      startTimer,
      tooltips,
      life,
      show_dialog_modal,
    } = this.props;

    return (
      <View style={styles.content}>
        <Text style={styles.titleQuestion}>{data.text}</Text>
        {data.link && (
          <VideoComponent
            link={data.link}
            play={playMediaFiles}
            tooglePlay={this.tooglePlayMedia}
            startTimer={() => startTimer(data.time)}
          />
        )}
        {data.audio && (
          <AudioComponent
            link={data.audio}
            play={playMediaFiles}
            tooglePlay={this.tooglePlayMedia}
            startTimer={() => startTimer(data.time)}
          />
        )}
        {data.image && (
          <View style={styles.imageWrapper}>
            <Image
              source={{uri: data.image}}
              style={styles.image}
              resizeMode="contain"
            />
          </View>
        )}
        <View style={styles.listWrapper}>
          {data.answers.map(item => (
            <ItemVariant
              {...item}
              key={item.order_num}
              onPress={this.setAnswer}
              iGuessed={
                answer === item.order_num && answer === data.right_answer_num
              }
              isFailed={
                answer === item.order_num && answer !== data.right_answer_num
              }
              showTooltip={
                showTooltip && item.order_num === data.right_answer_num
              }
            />
          ))}
        </View>
        <DialogModal
          isVisible={show_dialog_modal === 'children'}
          type_key="quiz"
          title_type_key="wrong"
          onBuyPress={this.onBuyPress}
          onTryOnePress={this.onTryOnePress}
          onExitPress={this.onExitPress}
          onWatchVideo={this.onWatchVideo}
          onUseTooltips={this.onUseTooltips}
          life={life}
          tooltips={tooltips}
        />
        <Admob
          onFinishedWatching={this.onFinishedWatching}
          onLoaded={admob => this.setState({admob})}
          onCanceled={() => {
            if (!show_dialog_modal) {
              this.props.togglePause(false);
            }
            this.setState({admob: null});
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentQuestion: state.quizzesReducer.currentQuestion,
  life: state.quizzesReducer.life,
  tooltips: state.quizzesReducer.tooltips,
  show_dialog_modal: state.generalReducer.show_dialog_modal,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeCurrentQuestionDataQuizzes,
      addQuizTipsLifes,
      removeQuizTipsLifes,
      toogleDialogModal,
    },
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(QuestionQuizComponent),
);
