import React, {Component} from 'react';
import {ImageBackground, View, Text, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _ from 'underscore';
import {withTranslation} from 'react-i18next';
import {CommonActions} from '@react-navigation/native';
import RadialGradient from 'react-native-radial-gradient';
import {
  setAnswersToStoreQuizzes,
  getQuestionsListQuizzes,
} from '../../../../actions/quizzesActions';
import {MainFlowKeys} from '../../../../navigator/Keys';
import {replace} from '../../../../navigator/RootNavigation';
import {Container, Button} from '../../../../components';
import {
  BACKGROUND_1_IMAGE,
  CUP_VIOLET_IMAGE,
  CUP_BLUE_IMAGE,
} from '../../../../constants/Images';
import {COLOR_BLUE_3_DARKEN, COLOR_BLUE_3} from '../../../../constants/Colors';
import {deviceWidth, deviceHeight} from '../../../../constants/StylesConstants';
import styles from './styles';

const VIOLET = ['#DD01AD', '#2200AB'];
const BLUE = [COLOR_BLUE_3_DARKEN, COLOR_BLUE_3];

class CongratulationScreen extends Component {
  state = {
    isTournament: this.props.route.params.isTournament,
    current_id: this.props.route.params.id,
  };

  getNextStep = (isTournament, answers, quizzes_list) => {
    const {current_id} = this.state;
    if (isTournament) {
      return 'back';
    }
    const last_key =
      current_id || Object.keys(answers)[Object.keys(answers).length - 1];
    const current_step = _.find(quizzes_list, item => +item.id === +last_key);

    if (current_step) {
      const next_step = _.find(
        quizzes_list,
        item => item.order_num === current_step.order_num + 1,
      );
      if (next_step && next_step.available) {
        return next_step;
      } else {
        return 'home';
      }
    } else {
      return 'home';
    }
  };

  goNext = () => {
    const {isTournament} = this.state;
    const {quizzes_list, answers} = this.props;

    const next_step = this.getNextStep(isTournament, answers, quizzes_list);
    console.log(next_step);
    if (next_step === 'back') {
      return this.props.navigation.goBack();
    }
    if (next_step === 'home') {
      return this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: MainFlowKeys.Home,
              params: {disableStartRequest: true},
            },
          ],
        }),
      );
    }

    this.props.setAnswersToStoreQuizzes(next_step.id, []);
    this.props.getQuestionsListQuizzes(next_step.id);
    replace(MainFlowKeys.QuestionQuizzes, {id: next_step.id});
  };

  render() {
    const {t, quizzes_list, answers} = this.props;
    const {isTournament} = this.state;

    return (
      <RadialGradient
        style={styles.wrapper}
        colors={isTournament ? VIOLET : BLUE}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_1_IMAGE} style={styles.flex}>
          <Container
            enableStatusBar={false}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}
            scrollEnabled={false}
            headerEnabled={false}>
            <View style={styles.content}>
              <View style={styles.topContent}>
                <Text style={styles.title}>
                  {t('main_flow:answered_an_all_questions')}
                </Text>
                <Image
                  source={isTournament ? CUP_VIOLET_IMAGE : CUP_BLUE_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <Button
                contentContainerStyle={styles.button}
                title={
                  typeof this.getNextStep(
                    isTournament,
                    answers,
                    quizzes_list,
                  ) !== 'string'
                    ? t('common:buttons.lets_move')
                    : t('common:buttons.on_home')
                }
                onPress={this.goNext}
                type="red"
                isArrow={
                  typeof this.getNextStep(
                    isTournament,
                    answers,
                    quizzes_list,
                  ) !== 'string'
                }
              />
            </View>
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({
  quizzes_list: state.quizzesReducer.quizzes_list,
  answers: state.quizzesReducer.answers,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {setAnswersToStoreQuizzes, getQuestionsListQuizzes},
    dispatch,
  );

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CongratulationScreen),
);
