import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import RadialGradient from 'react-native-radial-gradient';
import {Container} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {AuthFlowKeys} from '../../../navigator/Keys';
import {CONCEPT_IMAGE, FB_ICON, GOOGLE_ICON} from '../../../constants/Images';
import {COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN} from '../../../constants/Colors';
import {BACKGROUND_3_IMAGE} from '../../../constants/Images';
import {deviceWidth, deviceHeight} from '../../../constants/StylesConstants';
import styles from './styles';

const BACKGROUND = [COLOR_YELLOW_2, COLOR_YELLOW_2_DARKEN];

class ChooseRegistrationScreen extends Component {
  render() {
    const {t} = this.props;

    return (
      <RadialGradient
        style={styles.flex}
        colors={BACKGROUND}
        stops={[0, 1]}
        center={[deviceWidth, deviceHeight * 0.2]}
        radius={deviceHeight * 1.0375}>
        <ImageBackground source={BACKGROUND_3_IMAGE} style={styles.flex}>
          <Container
            headerProps={{
              title: t('auth_flow:registration'),
              backgroundStart: 'transparent',
              backgroundFinish: 'transparent',
            }}
            contentContainerStyle={styles.transparentStyle}
            scrollStyles={styles.transparentStyle}>
            <View style={styles.content}>
              <View style={styles.imageWrapper}>
                <Image
                  source={CONCEPT_IMAGE}
                  style={styles.image}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.bottomContent}>
                {/* <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {}}
                  style={styles.button}>
                  <Image
                    source={FB_ICON}
                    style={styles.fb_icon}
                    resizeMode="contain"
                  />
                  <Text style={styles.buttonText}>
                    {t('common:buttons.via_facebook')}
                  </Text>
                </TouchableOpacity> */}
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {}}
                  style={styles.button}>
                  <Image
                    source={GOOGLE_ICON}
                    style={styles.google_icon}
                    resizeMode="contain"
                  />
                  <Text style={styles.buttonText}>
                    {t('common:buttons.via_google')}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => navigate(AuthFlowKeys.Registration)}
                  style={styles.button}>
                  <Text style={styles.buttonText}>
                    {t('common:buttons.email_password')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Container>
        </ImageBackground>
      </RadialGradient>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default withTranslation(['common', 'auth_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ChooseRegistrationScreen),
);
