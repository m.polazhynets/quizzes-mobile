import React, {Component} from 'react';
import {StatusBar} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SplashScreen from 'react-native-splash-screen';
import {withTranslation} from 'react-i18next';
import ReactNativeBiometrics from 'react-native-biometrics';
import {logOut} from '../../actions/authActions';
import {setBiometricType} from '../../actions/settingsActions';
import {COLOR_WHITE_DARKEN} from '../../constants/Colors';
import {AppFlowKeys, AuthFlowKeys} from '../../navigator/Keys';

class StartScreen extends Component {
  componentDidMount() {
    const {quick_log_in, i18n, lang_code} = this.props;
    i18n.changeLanguage(lang_code);
    if (!quick_log_in) {
      this.props.logOut();
    }
    ReactNativeBiometrics.isSensorAvailable()
      .then(biometryType => {
        this.props.setBiometricType(biometryType);
        this.goToNextStep();
      })
      .catch(() => {
        this.goToNextStep();
      });
  }

  goToNextStep = () => {
    const {access_token, profile, navigation, lang_code} = this.props;

    setTimeout(() => {
      if (access_token && profile) {
        navigation.replace(AppFlowKeys.Auth, {
          screen: AuthFlowKeys.PinCode,
          params: {email: profile.user_social_id || profile.email},
        });
      } else {
        navigation.replace(AppFlowKeys.Auth, {
          screen: lang_code
            ? AuthFlowKeys.ChooseAuth
            : AuthFlowKeys.ChooseLanguage,
          params: {
            headerEnabled: true,
          },
        });
      }
      SplashScreen.hide();
    }, 0);
  };

  render() {
    return (
      <StatusBar barStyle="dark-content" backgroundColor={COLOR_WHITE_DARKEN} />
    );
  }
}

const mapStateToProps = state => ({
  access_token: state.authReducer.access_token,
  profile: state.profileReducer.profile,
  lang_code: state.settingsReducer.lang_code,
  quick_log_in: state.settingsReducer.quick_log_in,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({setBiometricType, logOut}, dispatch);

export default withTranslation(['common'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(StartScreen),
);
