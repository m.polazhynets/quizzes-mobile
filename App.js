/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import {I18nextProvider} from 'react-i18next';
import i18n from './src/utils/i18n';
import * as Sentry from '@sentry/react-native';
import admob, {MaxAdContentRating} from '@react-native-firebase/admob';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './src/navigator/RootNavigation';
import {store, persistor} from './src/store/configureStore';
import RootStackScreen from './src/navigator/Root';
import {ErrorModal, CustomToast, NotificationToast} from './src/components';

Sentry.init({
  dsn:
    'https://17771b32db4d4212bac2291877179fbe@o404936.ingest.sentry.io/5269758',
});

admob()
  .setRequestConfiguration({
    // Update all future requests suitable for parental guidance
    maxAdContentRating: MaxAdContentRating.PG,

    // Indicates that you want your content treated as child-directed for purposes of COPPA.
    tagForChildDirectedTreatment: true,

    // Indicates that you want the ad request to be handled in a
    // manner suitable for users under the age of consent.
    tagForUnderAgeOfConsent: true,
  })
  .then(() => {
    console.log('Request config successfully set!');
  })
  .catch(e => {
    console.log(e);
  });

console.reportErrorsAsExceptions = false;
console.disableYellowBox = true;

export default () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <I18nextProvider i18n={i18n}>
          <SafeAreaProvider>
            <NavigationContainer ref={navigationRef}>
              <ErrorModal />
              <CustomToast />
              <RootStackScreen />
              <NotificationToast />
            </NavigationContainer>
          </SafeAreaProvider>
        </I18nextProvider>
      </PersistGate>
    </Provider>
  );
};
