import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import {GENERAL} from '../constants/ActionTypes';

export default injectReducer(initialState.generalReducer, {
  [GENERAL.SET_APP_LOADED_PARAM]: (state, {payload}) => ({
    ...state,
    app_is_full_loaded: payload,
  }),

  [GENERAL.GET_PRIVACY_POLICY]: state => ({
    ...state,
    is_request: true,
  }),
  [GENERAL.GET_PRIVACY_POLICY_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    privacy_policy: payload,
  }),
  [GENERAL.GET_PRIVACY_POLICY_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [GENERAL.SEND_FEEDBACK]: state => ({
    ...state,
    is_request: true,
    is_sent_feedback: null,
  }),
  [GENERAL.SEND_FEEDBACK_SUCCESS]: state => ({
    ...state,
    is_request: false,
    is_sent_feedback: true,
  }),
  [GENERAL.SEND_FEEDBACK_FAILURE]: state => ({
    ...state,
    is_request: false,
    is_sent_feedback: false,
  }),

  [GENERAL.GET_FAQS_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [GENERAL.GET_FAQS_LIST_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    faqs: payload,
  }),
  [GENERAL.GET_FAQS_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [GENERAL.GET_FAQS_LIST_TOURNAMENT]: state => ({
    ...state,
    is_request: true,
  }),
  [GENERAL.GET_FAQS_LIST_TOURNAMENT_SUCCESS]: (state, {payload}) => ({
    ...state,
    is_request: false,
    faqs_tournament: payload,
  }),
  [GENERAL.GET_FAQS_LIST_TOURNAMENT_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [GENERAL.GET_COUNTRIES_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [GENERAL.GET_COUNTRIES_LIST_SUCCESS]: (state, {payload: {results}}) => ({
    ...state,
    is_request: false,
    autocomplete_data: results,
  }),
  [GENERAL.GET_COUNTRIES_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [GENERAL.GET_CITIES_LIST]: state => ({
    ...state,
    is_request: true,
  }),
  [GENERAL.GET_CITIES_LIST_SUCCESS]: (state, {payload: {results}}) => ({
    ...state,
    is_request: false,
    autocomplete_data: results,
  }),
  [GENERAL.GET_CITIES_LIST_FAILURE]: state => ({
    ...state,
    is_request: false,
  }),

  [GENERAL.CLEARE_AUTOCOMPLETE]: state => ({
    ...state,
    autocomplete_data: null,
  }),

  [GENERAL.TOOGLE_DIALOG_MODAL]: (
    state,
    {payload: {show, type_key = null}},
  ) => ({
    ...state,
    show_dialog_modal: show,
    dialog_type_key: type_key,
  }),
});
