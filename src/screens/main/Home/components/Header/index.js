import React from 'react';
import PropTypes from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity, View, Image} from 'react-native';
import {DRAWER_ICON} from '../../../../../constants/Images';
import styles from './styles';

const Header = ({}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.header}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.menuButton}
        onPress={() => navigation.toggleDrawer()}>
        <Image
          source={DRAWER_ICON}
          style={styles.menuIcon}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};

Header.propTypes = {
  onQuestionPress: PropTypes.func,
};

export default Header;
