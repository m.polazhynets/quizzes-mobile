import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import moment from 'moment';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getUserProfile} from '../../../actions/profileActions';
import Header from './components/Header';
import {Preloader} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {
  BIRTHDAY_ICON,
  EMAIL_ICON,
  LOCATION_ICON,
  STAR_VIOLET_ICON,
  MESSAGE_ICON,
} from '../../../constants/Images';
import styles from './styles';

class User extends Component {
  header = React.createRef();
  state = {
    id: this.props.route.params.id,
  };

  componentDidMount() {
    this.props.getUserProfile(this.state.id);
  }

  renderHeaderButton = () => {
    const {user_profile} = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() =>
          navigate(MainFlowKeys.Chat, {
            user: {
              user_id: user_profile.id,
              photo: user_profile.photo,
              nickname: user_profile.nickname,
            },
            id: null,
          })
        }
        style={styles.headerButton}>
        <Image
          source={MESSAGE_ICON}
          style={styles.headerIcon}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {t, is_request, user_profile} = this.props;

    return (
      <View style={styles.wrapper}>
        <SafeAreaView style={styles.content}>
          <Header
            ref={this.header}
            rightBtn={this.renderHeaderButton()}
            data={user_profile}
          />
          {is_request && <Preloader />}
          {user_profile && (
            <ScrollView
              contentContainerStyle={styles.scrollContentStyles}
              onScroll={e => this.header.current.onScroll(e)}
              scrollEventThrottle={16}>
              <View style={styles.item}>
                <Image
                  source={STAR_VIOLET_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>
                  {t('main_flow:tournament_rating')}
                </Text>
                <View style={styles.row}>
                  <View style={styles.part}>
                    <Text style={styles.yellowText}>{user_profile.points}</Text>
                    <Text style={styles.text}>{t('main_flow:rating')}</Text>
                  </View>
                  <View style={styles.part}>
                    <Text style={styles.yellowText}>
                      {user_profile.position}
                    </Text>
                    <Text style={styles.text}>{t('main_flow:position')}</Text>
                  </View>
                </View>
                {/* <ButtonLink
                  contentContainerStyle={styles.button}
                  title={t('common:buttons.view_in_tournament_grid')}
                  onPress={() => navigate(MainFlowKeys.Statistics)}
                /> */}
              </View>
              <View style={styles.item}>
                <Image
                  source={BIRTHDAY_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:birthday')}</Text>
                <Text style={styles.text}>
                  {moment(user_profile.birthdate).format('DD.MM.YYYY')}
                </Text>
              </View>
              <View style={styles.item}>
                <Image
                  source={EMAIL_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:email')}</Text>
                <Text style={styles.text}>{user_profile.email}</Text>
              </View>
              <View style={styles.item}>
                <Image
                  source={LOCATION_ICON}
                  style={styles.itemIcon}
                  resizeMode={'contain'}
                />
                <Text style={styles.smallTitle}>{t('main_flow:location')}</Text>
                <Text style={styles.text}>
                  {`${user_profile.city_name}${
                    user_profile.city_name ? ', ' : ''
                  }${user_profile.country_name}`}
                </Text>
              </View>
            </ScrollView>
          )}
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.profileReducer.is_request,
  user_profile: state.profileReducer.user_profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getUserProfile}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(User),
);
