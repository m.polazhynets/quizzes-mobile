import React, {Component} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion';
import {
  getFaqsList,
  getFaqsListTournament,
} from '../../../actions/generalActions';
import {Container, Button, Preloader} from '../../../components';
import {navigate} from '../../../navigator/RootNavigation';
import {MainFlowKeys} from '../../../navigator/Keys';
import {ARROW_DARK_RIGHT_ICON} from '../../../constants/Images';
import styles from './styles';

class FAQsScreen extends Component {
  state = {
    activeSections: [],
    type: this.props.route.key,
  };

  componentDidMount() {
    const {type} = this.state;

    if (type === 'tournament') {
      this.props.getFaqsListTournament();
    } else {
      this.props.getFaqsList();
    }
  }

  _updateSections = activeSections => {
    this.setState({activeSections});
  };

  _renderHeader = (section, index, isActive) => {
    const activeStyles = {transform: [{rotate: '90deg'}], left: 3};
    return (
      <Animatable.View style={styles.headerList}>
        <Text style={styles.title}>{section.question_text}</Text>
        <Animatable.Image
          duration={300}
          transition="rotate"
          style={[styles.icon, isActive && activeStyles]}
          resizeMode={'contain'}
          source={ARROW_DARK_RIGHT_ICON}
        />
      </Animatable.View>
    );
  };

  _renderContent = section => {
    return (
      <View style={styles.contentItem}>
        <Text style={styles.text}>{section.question_answer}</Text>
      </View>
    );
  };

  render() {
    const {t, is_request, list, list_tornament} = this.props;
    const {activeSections, type} = this.state;

    const data = type === 'tournament' ? list_tornament : list;

    return (
      <Container
        enableBrainBackground
        headerProps={{
          title: t('main_flow:faq'),
        }}>
        {is_request && <Preloader />}
        {data && (
          <ScrollView
            contentContainerStyle={styles.content}
            keyboardShouldPersistTaps="handled">
            <Accordion
              sections={data}
              touchableComponent={TouchableOpacity}
              touchableProps={{activeOpacity: 0.8}}
              activeSections={activeSections}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              renderFooter={null}
              onChange={this._updateSections}
            />
          </ScrollView>
        )}
        <View style={styles.bottomContent}>
          <Button
            contentContainerStyle={styles.button}
            title={t('common:buttons.ask_your_question')}
            type="red"
            onPress={() => navigate(MainFlowKeys.Feedback)}
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.generalReducer.is_request,
  list: state.generalReducer.faqs,
  list_tornament: state.generalReducer.faqs_tournament,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getFaqsList, getFaqsListTournament}, dispatch);

export default withTranslation(['common', 'main_flow'])(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(FAQsScreen),
);
