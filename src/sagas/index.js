import {all, fork} from 'redux-saga/effects';
import auth from './auth';
import profile from './profile';
import general from './general';
import tournament from './tournament';
import quizzes from './quizzes';
import notification from './notification';
import chat from './chat';

/**
 * rootSaga
 */
export default function* root() {
  yield all([
    fork(auth),
    fork(profile),
    fork(general),
    fork(tournament),
    fork(quizzes),
    fork(notification),
    fork(chat),
  ]);
}
