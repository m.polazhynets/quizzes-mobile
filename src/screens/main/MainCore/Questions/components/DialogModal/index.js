import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Modal from 'react-native-modal';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {Button, ButtonLink, Preloader} from '../../../../../../components';
import styles from './styles';

class DialogModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.renderData(this.props),
    };
  }

  componentDidUpdate(prevProps) {
    const {isVisible} = this.props;

    if (prevProps !== this.props && isVisible) {
      this.setState({
        data: this.renderData(this.props),
      });
    }
  }

  renderData = ({
    t,
    life,
    type_key,
    title_type_key,
    tooltips,
    onBuyPress,
    onExitPress,
    onWatchVideo,
    onTryOnePress,
    onUseTooltips,
    onCancel,
    onNextPress,
  }) => {
    return {
      titles: {
        wrong: t('quiz_flow:are_mistaken'),
        no_time: t('quiz_flow:no_answer_question_during_time'),
        tooltip: tooltips
          ? `${t('quiz_flow:have_tips_available')} ${tooltips}`
          : `${t('quiz_flow:have_no_tips_available')}`,
      },
      texts: [
        life
          ? `${t('quiz_flow:have_lives_available')} ${life}`
          : `${t('quiz_flow:have_no_lives_available')}`,
        tooltips
          ? `${t('quiz_flow:have_tips_available')} ${tooltips}`
          : `${t('quiz_flow:have_no_tips_available')}`,
      ],
      buttons: [
        onBuyPress && {
          title: t('quiz_flow:buy'),
          onPress: () => onBuyPress(type_key),
          type: 'red',
        },
        onWatchVideo &&
          title_type_key !== 'tooltip' &&
          !life && {
            title: t('quiz_flow:watch_video_for_one_life'),
            onPress: () => onWatchVideo('life'),
            type: 'yellow',
          },
        onWatchVideo &&
          (life || title_type_key === 'tooltip') &&
          !tooltips && {
            title: t('quiz_flow:watch_video_for_one_tooltips'),
            onPress: () => onWatchVideo('tooltip'),
            type: 'yellow',
          },
        onUseTooltips &&
          (life || title_type_key === 'tooltip') &&
          tooltips && {
            title: t('quiz_flow:use_tooltip'),
            onPress: () => onUseTooltips(),
            type: 'yellow',
          },
      ],
      links: [
        life &&
          onTryOnePress && {
            title: t('quiz_flow:try_one_more_time'),
            onPress: () => onTryOnePress(),
          },
        !life &&
          title_type_key !== 'tooltip' &&
          type_key === 'tournament' &&
          onExitPress && {
            title: t('quiz_flow:finish_tournament'),
            onPress: () => onExitPress(),
          },
        !life &&
          title_type_key !== 'tooltip' &&
          type_key === 'quiz' &&
          onExitPress && {
            title: t('quiz_flow:finish_quiz'),
            onPress: () => onExitPress(),
          },
        onCancel && {
          title: t('common:texts.cancel'),
          onPress: () => onCancel(),
        },
        onNextPress && {
          title: t('common:buttons:go_next_question'),
          onPress: () => onNextPress(),
        },
      ],
    };
  };

  render() {
    const {
      isVisible,
      title_type_key,
      is_manual_request,
      is_request,
      is_request_tournament,
      modalData,
      onNextPress,
    } = this.props;
    const {data} = this.state;

    const request = is_request || is_request_tournament || is_manual_request;

    return (
      <Modal isVisible={isVisible} useNativeDriver>
        {request && <Preloader />}
        <View style={styles.modalContent}>
          {data.titles[title_type_key] && (
            <Text style={styles.title}>{data.titles[title_type_key]}</Text>
          )}
          {modalData && modalData.life && (
            <Text style={styles.title}>{modalData.life.title}</Text>
          )}
          {modalData && modalData.time_out && (
            <Text style={styles.title}>{modalData.time_out.title}</Text>
          )}
          {title_type_key !== 'tooltip' &&
            !onNextPress &&
            data.texts.map((item, index) => {
              if (!item) {
                return null;
              }
              return (
                <Text style={styles.text} key={index}>
                  {item}
                </Text>
              );
            })}
          {modalData && modalData.life && (
            <Text style={styles.text}>{modalData.life.text}</Text>
          )}
          {modalData && modalData.time_out && (
            <Text style={styles.text}>{modalData.time_out.text}</Text>
          )}
          <View style={styles.buttonsContent}>
            {data.buttons.map((item, index) => {
              if (!item) {
                return null;
              }
              return (
                <Button
                  key={index}
                  contentContainerStyle={styles.button}
                  margin
                  {...item}
                />
              );
            })}
            {data.links.map((item, index) => {
              if (!item) {
                return null;
              }
              return (
                <ButtonLink
                  key={index}
                  contentContainerStyle={styles.link}
                  {...item}
                />
              );
            })}
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  is_request: state.quizzesReducer.is_modal_request,
  is_request_tournament: state.tournamentsReducer.is_modal_request,
  modalData: state.tournamentsReducer.modalData,
});

export default withTranslation(['common', 'main_flow', 'quiz_flow'])(
  connect(
    mapStateToProps,
    null,
  )(DialogModal),
);
