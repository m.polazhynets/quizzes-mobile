import React, {Component} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import DateTimePicker from '@react-native-community/datetimepicker';
import Modal from 'react-native-modal';
import i18n from 'i18next';
import {
  View,
  TouchableOpacity,
  Text,
  Platform,
  Animated,
  Image,
} from 'react-native';
import {Button} from '../index';
import {deviceHeight, deviceWidth} from '../../constants/StylesConstants';
import {CALENDAR_ICON} from '../../constants/Images';
import {
  TOP_START,
  TOP_FINISH,
  COLOR_START,
  COLOR_FINISH,
  BACKGROUND_COLOR_START,
  BACKGROUND_COLOR_FINISH,
  SIZE_START,
  SIZE_FINISH,
} from '../Input/styles';
import styles from './styles';

export default class InputDateTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
      isFocused: props.value ? true : false,
      date: props.value ? new Date(props.value) : new Date(),
    };
    this.animatedIsFocused = new Animated.Value(props.value ? 1 : 0);
  }

  componentDidUpdate() {
    const {isFocused} = this.state;

    Animated.timing(this.animatedIsFocused, {
      toValue: isFocused ? 1 : 0,
      duration: 100,
    }).start();
  }

  handleFocus = () => this.setState({isFocused: true, focus: true});
  handleBlur = () => {
    const {onBlur, value} = this.props;

    if (onBlur) {
      onBlur();
    }

    this.setState({isFocused: value ? true : false, focus: false});
  };

  renderDatePicker = () => {
    const {focus, date} = this.state;
    const {onChange = () => {}, value, mode, maximumDate} = this.props;

    if (Platform.OS === 'ios') {
      return (
        <Modal
          isVisible={focus}
          supportedOrientations={['portrait']}
          deviceHeight={deviceHeight * 1.2}
          deviceWidth={deviceWidth * 3}
          useNativeDriver
          onBackdropPress={this.handleBlur}
          onBackButtonPress={this.handleBlur}
          backdropColor="rgba(5, 28, 63, 0.8)">
          <View style={styles.modalContainer}>
            <DateTimePicker
              value={date}
              mode={mode}
              maximumDate={maximumDate}
              is24Hour={true}
              locale={'ru-RU'}
              display="default"
              onChange={(event, date) => this.setState({date})}
            />
            <Button
              title={i18n.t('common:buttons.choose')}
              onPress={() => {
                onChange(null, date);
                this.setState({focus: false});
              }}
              type="red"
            />
          </View>
        </Modal>
      );
    }

    return (
      <DateTimePicker
        value={value ? new Date(value) : new Date()}
        mode={mode}
        is24Hour={true}
        display="default"
        maximumDate={maximumDate}
        onChange={(event, date) => {
          this.setState({focus: false, isFocused: date ? true : false}, () => {
            onChange(event, date);
          });
        }}
      />
    );
  };

  render() {
    const {
      label,
      value,
      mode,
      required,
      isValid,
      containerStyles,
      inputStyles,
    } = this.props;
    const {focus} = this.state;

    const labelStyle = {
      color: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [COLOR_START, COLOR_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      top: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [TOP_START, TOP_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      fontSize: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [SIZE_START, SIZE_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
      backgroundColor: this.animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [BACKGROUND_COLOR_START, BACKGROUND_COLOR_FINISH],
        extrapolate: 'clamp',
        useNativeDriver: true,
      }),
    };

    return (
      <View
        style={[
          styles.container,
          required && isValid === false && styles.noPadding,
        ]}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.handleFocus}
          style={[
            styles.inputContainerStyle,
            containerStyles,
            required && isValid === false && styles.errorStyle,
            required && isValid && styles.successStyle,
          ]}>
          {label && (
            <Animated.Text
              numberOfLines={1}
              style={[
                styles.label,
                labelStyle,
                isValid && styles.successLabelStyle,
                required && isValid === false && styles.errorLabelStyle,
              ]}>
              {label}
            </Animated.Text>
          )}
          <View style={styles.inputWrapper}>
            <Text
              style={[
                styles.input,
                required && isValid === false && styles.inputError,
                inputStyles,
              ]}
              numberOfLines={1}>
              {value
                ? moment(value).format(mode === 'time' ? 'hh:mm' : 'DD.MM.YYYY')
                : ''}
            </Text>
            <Image
              source={CALENDAR_ICON}
              style={styles.calendarIcon}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
        {focus && this.renderDatePicker()}
      </View>
    );
  }
}

InputDateTimePicker.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.date]),
  label: PropTypes.string,
  mode: PropTypes.string,
  required: PropTypes.bool,
  isValid: PropTypes.bool,
  containerStyles: PropTypes.object,
  onBlur: PropTypes.func,
  inputStyles: PropTypes.object,
};
